#!/bin/env pipecl
#
# FILEVL -- Evaluate sky images to reject exposures from the sky stack.
#
# This routine maybe entered with multiple flags.

int	nmin = 10
int	status = 1
bool	update = yes
bool	show = no
int	figno = 1
bool	cksftflag = yes
struct	*fd

bool	sameref
int	n, ndet, ngroup, ncol
real	quality = -1.
string	s4, html, refskymap, stk, obm, fig, fign
real	hsigma, lsigma
real	mean = 1.
real	stddev = INDEF
real	stddev2 = INDEF

# Tasks and packages.
nfextern
ace
#task	$mkgraphic = "$!mkgraphic $(1) $(2) world png 1 'i1>0'"

# Files and directories.
html = dataset.sname // "_evl.html"
fig = dataset.sname // "_fig"
stk = dataset.sname // "_skymap"
obm = dataset.sname // "_skymap_obm"

# Set list of available image basename if not done elsewhere.
# Only these will be used in the rest of the pipeline.
if (access ("fil_ims.tmp") == NO) {
    match ("[0-9].fits", "*.omi", print-, >> "filevl_ims.tmp"
    list = "filevl_ims.tmp"
    while (fscan (list, s1) != EOF) {
	s2 = substr (s1, strldx("/",s1)+1, strldx(".",s1)-1)
	print (s2, >> "fil_ims.tmp")
    }
    list = ""
}
;
if (access ("fil_ims.tmp") == NO)
    plexit NODATA
;

# Check blackboard.
s1 = envget ("OSF_FLAG")
if (s1 == "N") {
    delete ("DO??T.tmp"); plexit NODATA
}
;

# Check if template is desired.
if (access ("DOEVL.tmp") == NO) {
    delete ("DO??T.tmp"); plexit NODATA
}
;

# Check for sky data.
match ("OMI?*_sky", "*.omi", print-, > "filevl_in.tmp")
count ("filevl_in.tmp") | scan (i)
if (i == 0) {
    delete ("DO??T.tmp"); plexit NODATA
}
;

# Reset data.
if (access("filupd_sftflag.tmp")) {
    list = "filupd_sftflag.tmp"
    while (fscan (list, s1, line) != EOF) {
        if (line != "INDEF")
	    hedit (s1, "SFTFLAG", line)
	;
    }
    list = ""; delete ("filupd_sftflag.tmp")
}
;
if (access("filupd_frgflag.tmp")) {
    list = "filupd_frgflag.tmp"
    while (fscan (list, s1, line) != EOF) {
        if (line != "INDEF")
	    hedit (s1, "FRGFLAG", line)
	;
    }
    list = ""; delete ("filupd_frgflag.tmp")
}
;
if (access("filupd_pgrflag.tmp")) {
    list = "filupd_pgrflag.tmp"
    while (fscan (list, s1, line) != EOF) {
        if (line != "INDEF")
	    hedit (s1, "PGRFLAG", line)
	;
    }
    list = ""; delete ("filupd_pgrflag.tmp")
}
;

# Get sky images.
imcopy ("@filevl_in.tmp", ".", verbose-)
files ("*_sky.fits", > "filevl_in.tmp")
concat ("filevl_in.tmp", >> dataset.fdelete)

# Set montage width.
count ("filevl_in.tmp") | scan (n)
ncol = max (10, min (25, int (sqrt(n))))

# Start HTML file.
print (html, >> dataset.fdelete)
printf ('<HTML><HEAD><TITLE>Skies for %s</TITLE></HEAD></BODY>\n',
    dataset.sname, > html)
printf ('<CENTER><H2>Sky Analysis for %s</H2></CENTER>\n',
    dataset.sname, >> html)
printf ('\n<H4>Sky Map Analysis</H4>\n\n', >> html)

# Record list of sky files.
printf ('<P>Figure %d: List of all sky files.\n', figno, >> html)
printf ('<PRE>\n', >> html)
if (cksftflag == NO)
    hedit ("@filevl_in.tmp", "SFTFLAG", "Y")
;
hselect ("@filevl_in.tmp", "$I,SFTFLAG", yes) |
    translit ("STDIN", '"', delete+, >> html)
printf ('</PRE>\n', >> html)

# Make montage of all sky files.
montage ("filevl_in.tmp", fig, "All sky files.", ncol=ncol, html=html)

# Extract exposures which have not been previously rejected and make montage.
select ("SFTFLAG", "filevl_in.tmp", "filevl_good.tmp", "dev$null",
    cksftflag=cksftflag)
if (select.nselect > 0)
    montage ("filevl_good.tmp", fig,
        "Sky files after rejection by prior criteria.", ncol=ncol, html=html)
else
    printf ('<P>All skies rejected by prior criteria: exposure time, \
	source density, and maximum source size.\n', >> html)

# Finish if the exposures have been specifically selected.
if (select.nforced > 0) {
    printf ("Sky flat exposures explicitly selected.\n")
    printf ('<P>Sky flat exposures explicitly selected.\n', >> html)
    plexit COMPLETED
}
;

# Check if there are enough exposures for the rule.
if (select.nselect < nmin) {
    printf ("Not enough for making sky flat.\n")
    printf ('<P>Not enough for making sky flat.\n', >> html)
    delete ("DO??T.tmp"); plexit NODATA
}
;

# Apply a reference sky map if one is available.
if (access (stk//".fits")==YES)
    refskymap = stk // ".fits"
else {
    head ("filevl_in.tmp", nl=1) | scan (s1)
    getcal ("refskymap", image=s1, filter="!filter", mjd="!mjd-obs",
        quality=">=0", match="!noccbin", path+)
    if (getcal.nfound > 0)
	refskymap = getcal.value
    else
	refskymap = ""
}

if (refskymap != "") {
    printf ("Using reference sky map (%s)\n", refskymap)
    fd = "filevl_in.tmp"
    while (fscan (fd, s1) != EOF) {
	s2 = "orig_" // s1
        imrename (s1, s2)
	print (s2, >> "filevl_orig.tmp")
	print (s2, >> dataset.fdelete)
	imexpr ("a==-1000?a:(a/b)", s1, s2, refskymap, verbose-)
    }
    fd = ""

    refskymap = substr (refskymap, strldx("/",refskymap)+1,
        strstr(".fits",refskymap)-1)
    montage ("filevl_good.tmp", fig,
        "Flat field by reference "//refskymap//".", ncol=ncol, html=html)

    # Check if it is from the same dataset. This assumes a specific
    # naming convention.
    i = stridx ("-", refskymap)
    s1 = substr(refskymap, 1, strldx("_", substr(refskymap,1,i)))
    s2 = substr(stk, 1, strldx("_", substr(stk,1,i)))
    sameref = (s1 == s2)
    if (sameref) {
        s1 = substr (refskymap, i, 999)
        s2 = substr (stk, i, 999)
	sameref = (s1 == s2)
    }
    ;
} else
    sameref = NO

#plexit HALT

# Do sigma clipping.
count ("filevl_in.tmp") | scan (n)
if (n > 5) {

    # Image standard deviation.
    imstat ("@filevl_good.tmp", fields="image,stddev", format-,
	lower=-999, > "filevl_stat.tmp")
    sigclip ("filevl_stat.tmp", "filevl_good.tmp", "filevl_rej.tmp",
	label="Standard deviation", lclip=6, hclip=3, lsigma=6, hsigma=3,
	html=html)
    stddev2 = sigclip.mean
    delete ("filevl_stat.tmp")

    # Sky mean.
    hselect ("@filevl_in.tmp", "$I,$SKYMEAN", yes, > "filevl_stat.tmp")
    sigclip ("filevl_stat.tmp", "filevl_good.tmp", "filevl_rej.tmp",
	label="Sky mean", lclip=3, hclip=3, lsigma=4, hsigma=10, html=html)
    delete ("filevl_stat.tmp")

    # Magnitude zeropoint.
    hselect ("@filevl_in.tmp", "$I,$MAGZERO", yes, > "filevl_stat.tmp")
    sigclip ("filevl_stat.tmp", "filevl_good.tmp", "filevl_rej.tmp",
	label="Magnitude zero point", lclip=3, hclip=3, lsigma=4, hsigma=8,
	html=html)
    delete ("filevl_stat.tmp")

    # Make montage of remaining skies.
    if (access("filevl_rej.tmp") && access("filevl_good.tmp")) {
	montage ("filevl_good.tmp", fig, "Sky files after sigma clipping.",
	    ncol=ncol, html=html)
    }
    ;
}
;

# Check number rule.
if (access("filevl_good.tmp"))
    count ("filevl_good.tmp") | scan (n)
else
    n = 0
## if (n < 5) {
##     printf ("Not enough remaining for making sky flat\n")
##     printf ('<P>Not enough remaining for making sky flat\n', >> html)
## 
##     # Flag rejected skys.
##     if (access("filevl_rej.tmp")) {
## 	touch ("filevl.tmp,filevlobm.tmp")
## 	list = "filevl_rej.tmp"
## 	while (fscan (list, s1, line) != EOF) {
## 	    s2 = substr (s1, 1, strstr("_sky",s1)-1)
## 	    print (line, >> "filevl.tmp")
## 	    match (s2//"_00", "filevl_list.tmp", >> "filevl.tmp")
## 	    match (s2//"_sky", "filevl_list.tmp", >> "filevl.tmp")
## 	    match (s2//"-ccd[0-9]*.fits", "filevl_list.tmp", >> "filevl.tmp")
## 	    print (line, >> "filevlobm.tmp")
## 	    match (s2//"-ccd[0-9]*_obm.pl", "filevl_list.tmp", >> "filevlobm.tmp")
## 	}
## 	list = ""
## 
## 	list = "filevlobm.tmp"
## 	while (fscan (list, s1, line) != EOF) {
## 	    if (s1 == "#")
## 		printf ("%s %s\n", s1, line, >> "filevl.tmp")
## 	    else
## 		print (s1//"[pl]", >> "filevl.tmp")
## 	}
## 	list = ""
## 
## 	list = "filevl.tmp"; s2 = ""
## 	while (fscan (list, s1, s3) != EOF) {
## 	    if (s1 == "#") {
## 	        s2 = s3
## 		next
## 	    }
## 	    ;
## 	    if (s2 == "Standard") {
## 		hedit (s1, "SFTFLAG",
## 		    "((SFTFLAG=='Y')?'N standard deviation':SFTFLAG)",
## 		    show=show, verify-, update=update)
## 		hedit (s1, "FRGFLAG",
## 		    "((FRGFLAG=='Y')?'N standard deviation':FRGFLAG)",
## 		    show=show, verify-, update=update)
## 		hedit (s1, "PGRFLAG",
## 		    "((PGRFLAG=='Y')?'N standard deviation':PGRFLAG)",
## 		    show=show, verify-, update=update)
## 	    } else if (s2 == "Sky") {
## 		hedit (s1, "SFTFLAG",
## 		    "((SFTFLAG=='Y')?'N sky mean':SFTFLAG)",
## 		    show=show, verify-, update=update)
## 		hedit (s1, "FRGFLAG",
## 		    "((FRGFLAG=='Y')?'N sky mean':FRGFLAG)",
## 		    show=show, verify-, update=update)
## 	    } else if (s2 == "Magnitude") {
## 		hedit (s1, "SFTFLAG",
## 		    "((SFTFLAG=='Y')?'N magnitude zeropoint':SFTFLAG)",
## 		    show=show, verify-, update=update)
## 		hedit (s1, "FRGFLAG",
## 		    "((FRGFLAG=='Y')?'N magnitude zeropoint':FRGFLAG)",
## 		    show=show, verify-, update=update)
## 	    }
## 	    ;
## 	}
## 	list = ""
## 
## 	delete ("filevl_rej.tmp,filevl.tmp,filevlobm.tmp")
##     }
##     ;
## 
##     imdelete ("@filevl_in.tmp,filevl*.fits")
##     delete ("filevl*.tmp")
##     logout (status)
## }
## ;

## Remove sky gradients.  We need to make masks to exclude missing data.
#list = "filevl_good.tmp"
#while (fscan(list,s1) != EOF) {
#    s2 = substr (s1, strldx("/",s1)+1, strldx(".",s1)-1)
#    imexpr ("a==-1000?1 : 0", s2//"bpm.pl", s1, verbose-)
#    mimsurfit (s1, s2, 2, 2, xmed=1, ymed=1, mask=s2//"bpm.pl")
#    hedit (s2, "BPM", s2//"bpm.pl", add+, show=no, verify-)
#    print (s2//"bpm.pl", >> dataset.fdelete)
#    print (s2//"bpm.pl", >> ".fildone_delete.tmp")
#}
#list = ""
#
## Make montage.
#montage ("filevl_good.tmp", fig, "Sky files after removing planar gradient.",
#    ncol=ncol, html=html)

# Make sky stack and subtract from each sky map.
select ("SFTFLAG", "filevl_good.tmp", "dev$null", "filevlscales.tmp",
    cksftflag=cksftflag)
if (access(stk//".fits"))
    delete (stk//".fits")
;
scmb ("filevl_good.tmp", stk, stk//"_bpm", "filevlscales.tmp", "filevlstk",
    ngroup=INDEF, imcmb="IMCMBMEF", masktype="goodvalue", > "dev$null")
print (stk//"_bpm.pl", >> dataset.fdelete)
print (stk//"fits", >> ".fildone_delete.tmp")
mimstat (stk, imask="!BPM", fields="mean,stddev", format-) | scan (mean,stddev)
joinlines ("filevl_good.tmp", "filevlscales.tmp", > "filevl2.tmp")
rename ("filevl2.tmp", "filevlscales.tmp")

list = "filevlscales.tmp"
while (fscan(list,s1,x)!=EOF) {
    s2 = "ss_" // s1
    imexpr ("c==0?(a-b/d)+e:a", s2, s1, stk, stk//"_bpm", x, mean, verbose-)
    print (s2, >> dataset.fdelete)
    print (s2, >> "filevl1c.tmp")
    imstat (s2, fields="image,stddev", format-, lower=-999, >> "filevl_stat.tmp")
}
list = ""

imrename (stk, stk//"tmp")
imexpr ("c==0?a/b:a", stk, stk//"tmp", mean, stk//"_bpm", verbose-)
imdelete (stk//"tmp")
stddev /= mean; mean = 1

figno += 1; fign = fig + figno
touch (dataset.fprotect)
mkgraphic (stk, fign, "world", "png", 1, "'i1>0'")
printf ('<P>Figure %d: Sky stack.\n', figno, >> html)
printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> html)
pathname (fign//".png", >> "filevl_list.tmp")

# Reject deviant exposures.
if (n > 5) {

    # Image RMS.
    imstat ("@filevl1c.tmp", fields="image,stddev", format-,
	lower=-999, >> "filevl_stat.tmp")
    sigclip ("filevl_stat.tmp", "filevl_good.tmp", "filevl_rej1.tmp",
	label="RMS", lclip=2, hclip=2, lsigma=3, hsigma=3, html=html)
    stddev2 = sigclip.mean
    delete ("filevl_stat.tmp")

    # If exposures are rejected restack and make figures.
    if (access("filevl_rej1.tmp")) {
	concat ("filevl_rej1.tmp", >> "filevl_rej.tmp")
	imdelete ("@filevl1c.tmp,"//stk//","//stk//"_bpm")
	delete ("filevl1c.tmp,filevl_rej1.tmp,filevlscales.tmp")
	select ("SFTFLAG", "filevl_good.tmp", "dev$null", "filevlscales.tmp",
	    cksftflag=cksftflag)
	scmb ("filevl_good.tmp", stk, stk//"_bpm", "filevlscales.tmp",
	    "filevlstk", ngroup=INDEF, imcmb="IMCMBMEF",
	    masktype="goodvalue", > "dev$null")
	mimstat (stk, imask="!BPM", fields="mean,stddev", format-) |
	    scan (mean, stddev)
	joinlines ("filevl_good.tmp", "filevlscales.tmp", > "filevl2.tmp")
	rename ("filevl2.tmp", "filevlscales.tmp")

	list = "filevlscales.tmp"
	while (fscan(list,s1,x)!=EOF) {
	    s2 = "ss_" // s1
	    imexpr ("c==0?(a-b/d)+e:a", s2, s1, stk, stk//"_bpm",
	        x, mean, verbose-)
	    print (s2, >> "filevl1c.tmp")
	}
	list = ""

	montage ("filevl_good.tmp", fig, "Sky files after rms rejection.",
	    ncol=ncol, html=html)

	imrename (stk, stk//"tmp")
	imexpr ("c==0?a/b:a", stk, stk//"tmp", mean, stk//"_bpm", verbose-)
	imdelete (stk//"tmp")
	stddev /= mean; mean = 1

	figno += 1; fign = fig + figno
	touch (dataset.fprotect)
	mkgraphic (stk, fign, "world", "png", 1, "'i1>0'")
	printf ('<P>Figure %d: Sky stack after rms rejection.\n',
	    figno, >> html)
	printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> html)
	pathname (fign//".png", >> "filevl_list.tmp")
    }
    ;
}
;

# Make montage.
montage ("filevl1c.tmp", fig, 'Sky files after "sky subtraction".', ltrim=3,
    ncol=ncol, html=html)

# Detect sources.
if (n >= 5) { 
    if (isindef(stddev2))
        s3 = ""
    else if (refskymap == "")
	s3 = str (max (stddev2/2, 0.01))
    else
	s3 = str (max (stddev2/2, 0.01))
    touch ("filevl_rej1.tmp,filevl_rej2.tmp,filevl_good1.tmp,filevl_good2.tmp")
    for (hsigma=8; hsigma<=15; hsigma+=1) {
	if (refskymap == "")
	    lsigma = 10.
	else
	    lsigma = hsigma
	delete ("filevl_rej[12]*.tmp,filevl_good[12]*.tmp")
	count ("filevl1c.tmp") | scan (n)
	list = "filevl1c.tmp"; i = 0; j = 0
	while (fscan (list,s2) != EOF) {
	    s1 = substr (s2, 4, 999)
	    aceall (s2//"[3:38,3:38]", masks="!BPM", skyotype="subsky",
		skies="", sigmas=s3, objmasks="filevlobm.pl", catalogs="",
		skyimage="", exps="", gains="", omtype="all", extnames="",
		catdefs="", catfilter="", logfiles="", verbose=2,
		order="", nmaxrec=INDEF, gwtsig=INDEF, gwtnsig=INDEF,
		fitstep=1, fitblk1d=1, fithclip=2., fitlclip=3.,
		fitxorder=1, fityorder=1, fitxterms="half", blkstep=0,
		blksize=0, blknsubblks=2, updatesky=no, bpdetect="100",
		bpflag="1-5", convolve="bilinear 3 3", hsigma=hsigma,
		lsigma=lsigma, hdetect=yes, ldetect=yes, neighbors="8",
		minpix=9, sigavg=4., sigmax=4., bpval=INDEF,
		splitmax=INDEF, splitstep=0., splitthresh=5.,
		sminpix=8, ssigavg=10., ssigmax=5., ngrow=0, agrow=0.,
		magzero="INDEF") |
		match ("objects detected") | scan (ndet)
	    if (ndet > 0) {
		printf ("%s %s\n", s1, "# Source detection",
		    >> "filevl_rej1.tmp")
		printf ("%s %s\n", s2, "# Source detection",
		    >> "filevl_rej2.tmp")
		j += 1
	    } else {
		print (s1, >> "filevl_good1.tmp")
		print (s2, >> "filevl_good2.tmp")
	    }
	    imdelete ("filevlobm.pl")
	}
	list = ""

	if (n-j >= max(5,n/2))
	    break
	;
    }

    # Output results.
    if (access("filevl_rej2.tmp") && access("filevl_good2.tmp")) {
	montage ("filevl_good2.tmp", fig, "Sky files after source rejection.",
	    ltrim=3, ncol=ncol, html=html)
	delete ("filevl_rej2.tmp,filevl_good2.tmp")
    } else if (access("filevl_good2.tmp")) {
	printf ('<P> No residual "sources" detected.\n', >> html)
	delete ("filevl_good2.tmp")
    } else {
	printf ('<P> Residual "sources" detected in all exposures.\n',
	    >> html)
	delete ("filevl_rej2.tmp")
    }

    # Check how many were rejected.
    #if (n < 8 && n - j < 2) {
    if (n < 0 && n - j < 2) {
	printf ('<P> <B>Not enough remaining exposures.</B>\n', >> html)
	concat ("filevl_rej1.tmp", >> "filevl_rej.tmp")
	delete ("filevl_rej1.tmp,filevl_good*.tmp")
    #} else if (n - j < max (4, n/3)) {
    } else if (n - j < max (4, n/4)) {
	printf ('<P> <B>Not enough remaining exposures.  \
	    Source rejection ignored.</B>\n', >> html)
    } else {
	if (access("filevl_good1.tmp")) {
	    rename ("filevl_good1.tmp", "filevl_good.tmp")
	    if (access ("filevl_rej1.tmp"))
		concat ("filevl_rej1.tmp", >> "filevl_rej.tmp")
	    ;
	} else {
	    if (access ("filevl_rej1.tmp"))
		concat ("filevl_rej1.tmp", >> "filevl_rej.tmp")
	    ;
	    delete ("filevl_good.tmp,filevl_rej1.tmp")
	}
	;
    }

    # If exposures are rejected restack.
    imdelete ("@filevl1c.tmp")
    if (access("filevl_rej1.tmp")) {
	imdelete (stk//","//stk//"_bpm")
	select ("SFTFLAG", "filevl_good.tmp", "dev$null", "filevlscales.tmp",
	    cksftflag=cksftflag)
	scmb ("filevl_good.tmp", stk, stk//"_bpm", "filevlscales.tmp",
	    "filevlstk", ngroup=INDEF, imcmb="IMCMBMEF",
	    masktype="goodvalue", > "dev$null")
	mimstat (stk, imask="!BPM", fields="mean,stddev", format-) |
	    scan (mean, stddev)
	imrename (stk, stk//"tmp")
	imexpr ("c==0?a/b:a", stk, stk//"tmp", mean, stk//"_bpm", verbose-)
	imdelete (stk//"tmp")
	stddev /= mean; mean = 1

	figno += 1; fign = fig + figno
	touch (dataset.fprotect)
	mkgraphic (stk, fign, "world", "png", 1, "'i1>0'")
	printf ('<P>Figure %d: Sky stack.\n', figno, >> html)
	printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> html)
	pathname (fign//".png", >> "filevl_list.tmp")
    }
    ;
}
;

# If there is a good list left evaluate it further.
if (access("filevl_good.tmp")) {
    # Look for residual source light in stack.
    if (isindef(stddev))
        s3 = ""
    else if (refskymap == "")
	s3 = str (max (stddev/3, 0.002))
    else
	s3 = str (max (stddev/2, 0.002))
    if (refskymap == "") {
        hsigma = 8.
        lsigma = 12.
    } else {
        hsigma = 8.
        lsigma = 12.
    }
    aceall (stk//"[3:38,3:38]", masks="!BPM", skyotype="subsky", skies=mean,
	sigmas=s3, objmasks=obm, catalogs="", skyimage="", exps="",
	gains="", omtype="all", extnames="", catdefs="", catfilter="",
	logfiles="", verbose=2, order="", nmaxrec=INDEF, gwtsig=INDEF,
	gwtnsig=INDEF, fitstep=1, fitblk1d=1, fithclip=2., fitlclip=3.,
	fitxorder=1, fityorder=1, fitxterms="half", blkstep=0, blksize=0,
	blknsubblks=2, updatesky=no, bpdetect="100", bpflag="1-5",
	convolve="bilinear 3 3", hsigma=hsigma, lsigma=lsigma, hdetect=yes,
	ldetect=yes, neighbors="8", minpix=9, sigavg=4., sigmax=4.,
	bpval=INDEF, splitmax=INDEF, splitstep=0., splitthresh=5.,
	sminpix=8, ssigavg=10., ssigmax=5., ngrow=0, agrow=0.,
	magzero="INDEF") |
	match ("objects detected") | scan (ndet)
    if (ndet > 0 && !sameref) {
	printf ('<P><B>Residual source light (%d) detected in sky stack. \
	    Sky flat creation will be skipped.</B>\n', ndet, >> html)
    }
    ;

    # If a reference sky map was used make a stack from the original data.
    if (refskymap != "") {
	imdelete ("@filevl_in.tmp,"//stk//"_bpm")
	imrename ("@filevl_orig.tmp", "@filevl_in.tmp")
	#imrename (stk, stk//"1")
	imdelete (stk)
	delete ("filevlscales.tmp")
	select ("SFTFLAG", "filevl_good.tmp", "dev$null", "filevlscales.tmp",
	    cksftflag=cksftflag)
	scmb ("filevl_good.tmp", stk, stk//"_bpm", "filevlscales.tmp",
	    "filevlstk", ngroup=INDEF, imcmb="IMCMBMEF",
	    masktype="goodvalue", > "dev$null")

	figno += 1; fign = fig + figno
	touch (dataset.fprotect)
	mkgraphic (stk, fign, "world", "png", 1, "'i1>0'")
	printf ('<P>Figure %d: Final sky stack.\n', figno, >> html)
	printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> html)
	pathname (fign//".png", >> "filevl_list.tmp")
    }
    ;

    # Reject all remaining exposures if extended source light is detected.
    if (ndet > 0 && !sameref) {
	list = "filevl_good.tmp"
	while (fscan(list,s1) != EOF)
	    printf ("%s %s\n", s1, "# Source detection", >> "filevl_rej.tmp")
	list = ""; delete ("filevl_good.tmp")
	quality = -1
    } else
	quality = 0
} else
    quality = -1

delete ("index.html")
copy (html, "index.html")

# Finish up.
if (sky_review)
    plexit WAITING
else if (quality == -1) {
    #delete ("DO??T.tmp"); plexit NODATA
    plexit WAITING
    #plexit COMPLETED
} else
    plexit COMPLETED
