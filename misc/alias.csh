# Valdes.
alias ssh	"ssh -Y"
alias ed	vi
alias dir	ls
alias del	rm
alias type	cat
alias k		"kill -9"
alias show	printenv
alias back      'cd -; pwd'
alias pd        'pushd'
alias bc	"bc -q"
alias wc	"wc -l"
alias lrt	"ls -rt \!:* | tail"
alias lrtl	"ls -rtl \!:* | tail"
alias dirs	"dirs -v"
alias pltop	"top -d 10 -u $USER"

alias iqsdb	psql

alias arstat    "arstatus \!:* | grep -v ' "'0$'"'"
alias ckds      "cat $NHPPS_DATAAPP/*_TOP/*/topgroup_o.tmp"
alias sm	"tb $NHPPS_SYS_NAME Monitor;simplemon -l 'top|fil|omi|dts' -p 'omi'"
alias fvsm	"tb $NHPPS_SYS_NAME Monitor;simplemon -l 'top|fil'"
alias cleanonly 'plrun --cleanonly'
alias clean 'submit -c'
alias nsafetch  "curl -ks -E $NHPPS_PIPEAPPSRC/dcp/nsa.pem:'Mosaic DHS' 'https://voserver:7503?fileRef=\!:1' > \!:1"
alias wget	"wget --no-check-certificate"
alias   plrsync '/usr/bin/rsync -vauzP --exclude-from=$HOME/SharedHome/rsync.exclude \!* | egrep -v /$'
alias ckingest	"grep Created \!:1  | cut -d ' ' -f 7 ; grep Processing \!:1 | wc"
alias   plrsync '/usr/bin/rsync -vauzP --exclude-from=$HOME/SharedHome/rsync.exclude \!* | egrep -v /$'
alias fitsverify '/shared2/cfitsio/bin64/fitsverify'
alias fornodes "foreach node (`echo $PROCNODES|tr ',' ' '`)"
alias plrestart 'plssh $PROCNODES stoppipe --app=$NHPPS_SYS_NAME \!:1; sleep 2;plssh $PROCNODES runpipe --app=$NHPPS_SYS_NAME --nInst=\!:2 \!:1'
alias bsudo 'echo b3rl1n12 | sudo -S -p "" \!:*'
alias ds9imtool 'ds9 -view info no -view panner no -view magnifier no -view buttons no -view colorbar no'
alias tv '(setenv iraf /iraf/tv/; pipecl)'
alias kumbha 'ssh iraf@kumbhakarna'
alias kipmi0 'bsudo /net/pipe1/mospl/SharedHome/bin/kipmi0'
alias display 'echo Not in CL: display'
alias imdisplay '/usr/bin/display'
alias gifdisp '/usr/bin/display'
alias tb 'printf "\033]0;\!:*\007"'
alias plconfig 'vi $PipeConfig/ConfigMap.list'
alias plqueue 'vi $PipeConfig/\!:1next.dat'
alias plver 'vi $plroot/Versions'
alias su 'ssh \!:1@$HOST'
alias arhdr 'archivepath -v'

alias nersc 'echo "N3rsc@LBL"; ssh fvaldes@edison.nersc.gov'
alias ckps 'plssh -svn $PROCNODES ps xh | grep -v "[cs]sh" | grep -v python | grep -v grep | grep -v ps'

# Herrera
alias hg        "history | grep \!:1"
alias mw        'more `which  \!:1`'
alias mzlsstage	"cpstage -d schlegeld -p 2016A-0453 -e 'oo' -s mayall-data@desi.lbl.gov \!:*"
