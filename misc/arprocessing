#!/bin/csh -f

set instr = "DECam"
set filter = ""
switch ($NHPPS_SYS_NAME)
case DCP:
    set instr="DECam"
    breaksw
case MOSPL3:
    set instr="MOSAIC3"
    breaksw
case BOK:
    set instr="90prime"
    set filter = "and filter in ('g', 'bokr', '')"
    breaksw
endsw

#set exclude = "0001|0005|9999"
set exclude = "0001|0005"
set type = count
while ($# > 0)
    switch ($1)
    case -h:
        goto Usage
	breaksw
    case -c:
    case -count:
        set type = count
	breaksw
    case -u:
    case -unprocessed:
        set type = unprocessed
	breaksw
    case -p:
    case -processed:
        set type = processed
	breaksw
    case -e:
    case -exclude:
        set exclude = "$2"
	shift
	breaksw
    case -f:
        set filter = "and filter='"$2"'"
	shift
	breaksw
    default:
        break
    endsw
    shift
end

switch ($#)
case 1:
    set d1  = $1
    set d2  = $1
    breaksw
case 2:
    set d1  = $1
    set d2  = $2
    breaksw
default:
    goto Usage
endsw

# Create temporary tables over desired dates.

set sql = `mktemp --tmpdir --suffix=.sql arprocessingXXX`

cat << EOF > $sql
-- Create indexed table of basic data.
-- The substring on dtnsanam is to workaround a bug.

CREATE TEMP TABLE raw AS
SELECT date(start_date) date, substring(dtnsanam FROM 1 FOR 17) dtnsanam,
    dtpropid, COALESCE (dtpi,dtpropid) dtpi FROM voi.siap
WHERE instrument='$instr' AND proctype='raw' $filter
AND obstype IN ('object', 'standard')
AND object NOT SIMILAR TO '%(focus|donut|junk|pointing|skyflat|sky flat|twilight flat)%'
AND dtpropid NOT SIMILAR TO '%(${exclude})%'
AND start_date BETWEEN TIMESTAMP '$d1' AND TIMESTAMP '$d2';

CREATE INDEX rawidx ON raw (dtnsanam);

CREATE TEMP TABLE proc AS
SELECT date(start_date) date, substring(dtnsanam FROM 1 FOR 17) dtnsanam,
    dtpropid, COALESCE (dtpi,dtpropid) dtpi FROM voi.siap
WHERE instrument='$instr' and proctype='InstCal' and prodtype='image' $filter
AND start_date BETWEEN TIMESTAMP '$d1' and TIMESTAMP '$d2';

CREATE INDEX procidx ON proc (dtnsanam);

-- Create count tables.

CREATE TEMP TABLE unprocessed AS
SELECT date, dtpropid, dtpi, count(date) unprocessed FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a
GROUP BY date, dtpropid, dtpi;

CREATE TEMP TABLE processed AS
SELECT date, dtpropid, dtpi, count(date) processed FROM
(SELECT * FROM raw INTERSECT SELECT * FROM proc) a
GROUP BY date, dtpropid, dtpi;

CREATE TEMP TABLE unprocessed_only AS
SELECT a.date, a.dtpropid, a.dtpi, 0 processed, unprocessed FROM unprocessed a,
(SELECT date, dtpropid, dtpi FROM unprocessed EXCEPT SELECT date, dtpropid, dtpi FROM processed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.dtpi=b.dtpi;

CREATE TEMP TABLE processed_only AS
SELECT a.date, a.dtpropid, a.dtpi, processed, 0 unprocessed FROM processed a,
(SELECT date, dtpropid, dtpi FROM processed EXCEPT SELECT date, dtpropid, dtpi FROM unprocessed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.dtpi=b.dtpi;

CREATE TEMP TABLE foo AS
SELECT a.date, a.dtpropid, a.dtpi, processed, unprocessed
FROM processed a, unprocessed b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.dtpi=b.dtpi;

-- Create output.

EOF

switch ($type)
case count:
cat << EOF >> $sql
SELECT * FROM processed_only UNION
SELECT * FROM unprocessed_only UNION
SELECT * FROM foo
ORDER BY date, dtpropid, dtpi;
EOF
set flags = -q
breaksw

case unprocessed:
cat << EOF >> $sql
SELECT date, dtpropid, dtpi, dtnsanam FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a
ORDER BY date, dtpropid, dtpi, dtnsanam
;
EOF
set flags = -q
#set flags = -Aqt
breaksw

case processed:
cat << EOF >> $sql
SELECT date, dtpropid, dtpi, dtnsanam FROM
(SELECT * FROM raw INTERSECT SELECT * FROM proc) a
ORDER BY date, dtpropid, dtpi
;
EOF
set flags = -q
breaksw

default:
rm $sql
goto Usage
endsw

# Execute query
psql $flags < $sql

# Clean up.
#rm $sql

exit

Usage:

    echo "Usage: arprocessing [options] start_date start_date"
    echo "  Example: arprocessing 20130201 20130228"
    echo
    echo "  -c|-count        : count of files by date"
    echo "  -u|-unprocessed  : list raw files without processing"
    echo "  -p|-processed    : list raw files with processing"
    echo "  -e|-exclude list : list of proposals to exclude (default $exclude)"
