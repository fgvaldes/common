-- 
-- caldb.sql
-- 
-- SQL script to create the calibration database tables.
-- 
-- WARNING: the script simply DROPs any previous tables without warning.
--

DROP TABLE caldb;
CREATE TABLE caldb (
    CLASS       CHARACTER VARYING(32) NOT NULL, 
    DIR         CHARACTER VARYING(128) NULL, 
    MJD		REAL NULL,
    MJDWIN1     REAL NULL, 
    MJDWIN2     REAL NULL, 
    DETECTOR    CHARACTER VARYING(70) NULL, 
    IMAGEID     CHARACTER VARYING(70) NULL, 
    FILTER      CHARACTER VARYING(70) NULL, 
    EXPTIME     REAL NULL, 
    NCOMBINE	INTEGER NULL,
    QUALITY	REAL NULL,
    MATCH	CHARACTER VARYING(512) NULL,
    ADDED	TIMESTAMP DEFAULT now(),
    DATATYPE    CHARACTER VARYING(10) NULL, 
    VALUE       CHARACTER VARYING(128) NULL
);
