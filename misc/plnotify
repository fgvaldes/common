#!/bin/csh -f
#
## ============
## plnotify.csh
## ============
## ---------------------------------------------
## Send notifications to principle investigators
## ---------------------------------------------
## 
## :Manual section: 1
## :Manual group:   misc
## 
## Usage
## -----
## 
# usage1
## plnotify [options]
## 
## -s|--send_mail (default no)
##     Send notification mail to PI's
## -t|--test email_list
##     Send test notification only to specified recipient list
## -r|--Reply-to email_list (default sdmhelp\@noao.edu)
##     Set Reply-to or test recipient list
## -b|-Bcc email_list (default archive-notice\@noao.edu)
##     Set Bcc recipient list.  This appends to default Bcc.
## -c|-cc email_list
##     Set cc recipient list. This appends to default cc.
## -q|--query_archive (default no)
##     Query archive for proposals
## -e|--erase (default no)
##     Erase and repopulate notification datase. USE WITH CAUTION.
## -n|--new_dataset (default no)
##     Force notification even if PI has received previous email
## -i|--instrument instr (optional)
##     Archive instrument name
## --dates date1 date2 (optional)
##     Range of archive start_dates
## -l|--limit number (default 1)
##     Maximum number of PI's to notify
## -p|--proposal proposal
##     Limit mail only to specified proposal.
## -d|--dataset dataset
##     Limit mail only to specified dataset.
## -h|--help
##     Display the help page
## -v|--verify (default yes)
##     Change default for verification (i.e. turn verify off)
# usage2
## 
## Description
## -----------
## 
## This command interacts with the archive and pipeline scheduling databases
## to select principle investigators to send notification emails about reduced
## data in the archive.  It keeps a record of principle investigator programs
## (based on the proposal identifiers) which have been sent emails and will
## not send additional emails unless the ``new_dataset`` flag is set or the
## date sent field is cleared externally from the NOTIFY table.  This
## command is typically run as a cron job for past processed data and
## from a CGI script tied to a link in the operator's dataset processing
## interface.
## 
## If the ``query_archive`` option is set then a query is made for proposals
## with `InstCal image` data constrained by the optional instrument and
## calendar dates (the `start_date` field).  These proposals are matched to
## principle investigator names, emails, and pipeline datasets (`pldname`
## field).  This information is stored in a NOTIFY table in the user's
## pipeline scheduling database along with a UT date of any emails.  Note
## that at least one query must be made before notifications can begin.
## 
## If the ``new_dataset`` option is set any date sent value in the NOTIFY
## database is cleared.  This option is used to send a notification after new
## data for the same proposal have been processed and entered in the archive.
## The text is also slightly different in this case.  This option is normally
## used in conjunction with the ``dates`` and ``instrument`` options and a CGI
## script attached to a link in the operator's pipeline datasets interface.
## 
## The ``limit`` option is useful to limit the number of notifications in
## each call to this command.  This is typically used in conjunction with
## a ``cron`` table.
## 
## The notification is generated when the ``send_mail`` option is set.
## This will send email to the principle investigator email of record in
## the archive (which may be wrong for older programs).  The email is
## send from "archive-noreply@noao.edu".  The ``Reply-to`` and ``Bcc`` lists
## have defaults that can be set appropriately.  This option also sets
## the UT date in the NOTIFY table for any datasets in the pipeline
## scheduling database.
## 
## The ``test`` option overrides the ``send_mail`` and sends a test
## notification mail to the specified test recipient list.  The other email
## lists are ignored.  Also notification date fields are not updated in
## database tables.

if ($# == 0) then
    sed -n -e '/# usage1/,/# usage2/s/^## //p' `which plnotify`
    exit 0
endif


# Archive database parameters.
setenv PGUSER		arcsoft
setenv PGDATABASE	metadata
#setenv PGHOST		archdbn2.tuc.noao.edu
setenv PGHOST		db.sdm.noao.edu
setenv PGPORT		5432
setenv PGPASSWORD	Royrkval

# Process arguments.
set message = ""
set reply = "sdmhelp@noao.edu"
set instr = ""
set bcc = "archive-notice@noao.edu mario_prog@noao.edu sdmstatus@noao.edu"
set cc = ""
set test = 0
set query_archive = 0
set erase = 0
set send_mail = 0
set dates = ""
set new_dataset = 0
set limit = 1000
set new = 0
set proprequest = ""
set dsrequest = ""
set verify = 1
set eflag = ""

if ($?NHPPS_SYS_NAME) then
    switch ($NHPPS_SYS_NAME)
    case DCP:
        set message = "decam"
	set instr = "AND instrument LIKE 'DECam'"
	set bcc = "archive-notice@noao.edu mario_prog@noao.edu awalker@noao.edu sdmstatus@noao.edu"
	breaksw
    case MOSPL1:
	set instr = "AND instrument LIKE 'mosaic_1_1'"
        set message = "mosaic"
	breaksw
    case MOSPL3:
	set instr = "AND instrument LIKE 'mosaic3'"
        set message = "mosaic3"
	set bcc = "archive-notice@noao.edu mario_prog@noao.edu adey@noao.edu sdmstatus@noao.edu"
	breaksw
    case NEWFIRM:
	set message = "newfirm"
	breaksw
    endsw
endif

while ($# > 0)
    switch ($1)
    case -h:
    case --help:
	sed -n -e '/# usage1/,/# usage2/s/^## //p' `which plnotify`
	exit 0
    case -r:
    case --Reply-to:
        shift
	set reply = "$1"
	breaksw
    case -b:
    case --Bcc:
        shift
	set bcc = ("$bcc" "$1")
	breaksw
    case -c:
    case --cc:
        shift
	set cc = ("$cc" "$1")
	breaksw
    case -l:
    case --limit:
        shift
	set limit = $1
	breaksw
    case -i:
    case --instrument:
        shift
	set instr = "AND instrument LIKE '$1'"
	breaksw
    case --dates:
        shift
	set dates = 'AND start_date BETWEEN TIMESTAMP'
	set dates = ($dates \'$1\' "AND TIMESTAMP" \'$2\')
	shift
	breaksw
    case -q:
    case --query_archive:
        set query_archive = 1
	breaksw
    case -e:
    case --erase:
        set erase = 1
	breaksw
    case -n:
    case --new_dataset:
        set new_dataset = 1
	breaksw
    case -s:
    case --send_mail:
        set send_mail = 1
	breaksw
    case -v:
    case --verify:
        set verify = 0
	breaksw
    case -t:
    case --test:
        shift
	set test_email = "$1"
        set test = 1
        set send_mail = 1
	breaksw
    case -p
    case --propsal:
        shift
	set proprequest = "$1"
	breaksw
    case -d:
    case --dataset:
        shift
	set dsrequest = "$1"
	breaksw
    endsw
    shift
end

if ($query_archive) then

if ( "$instr" == "" ) then
    echo ERROR: an instrument name is needed when the archive is queried.
    echo Rerun plnotify including -i \<instrument name\>.
    exit 0
endif

# Make list of all proposals with InstCal data.
psql -Aqt -F '|' << EOF > notify.tmp
SELECT DISTINCT proposal, email_address, plqname
FROM voi.siap, viewspace.principal_investigator
WHERE prop_id=proposal
AND proposal != 'noao' AND proposal NOT LIKE '%-999_'
AND proctype='InstCal'
AND prodtype='image'
$instr
$dates
;
EOF

# Drop notification history if requested.
if ( $erase ) then
    echo ===== WARNING ===== PROCEED WITH CAUTION ===== WARNING =====
    echo The notification history database is about to be erased and
    echo "repopulated! Are you sure you want to continue (y/n)?"
    set answer = $<
    if ( $answer == 'y' ) then
        psqdb <<EOF
DROP TABLE NOTIFY;
EOF
    else
        echo Exiting...
        exit 0
    endif
endif

# Make a notification history database.
psqdb << EOF
CREATE TABLE IF NOT EXISTS NOTIFY (
    proposal		char(32),
    email_address	char(32),
    dataset		char(32),
    date		char(32)        DEFAULT NULL,
    PRIMARY KEY (proposal, dataset)
    );
EOF

# Enter information.
foreach line (`cat notify.tmp`)
set line = `echo $line | tr -d "'"` 
set values = (`echo $line | tr '|' ' '`)

if ($#values > 2) then
    if ($new_dataset) then
        psqdb << EOF
REPLACE INTO NOTIFY (proposal,email_address, dataset, date)
VALUES ('$values[1]','$values[2]','$values[3]',NULL);
EOF
        if (! (proposal == "noao" || proposal == "2012B-9999") @ new++
    else
# Use of replace is incorrect because it first deletes the entry,
# and as a result the notification timestamp is lost. 
        psqdb << EOF
INSERT OR IGNORE INTO NOTIFY (proposal,email_address,dataset)
VALUES ('$values[1]','$values[2]','$values[3]');
EOF
    endif
else
    psqdb << EOF
REPLACE INTO NOTIFY (proposal,email_address)
VALUES ('$values[1]','$values[2]');
EOF
endif

end

# Reset limit for the case of new dataset(s).
if ($new > 0) set limit = $new

endif

# Send mail to proposals which have not been previously notified.
if ($send_mail) then
@ l = 0
foreach proposal (`echo "SELECT DISTINCT proposal FROM NOTIFY WHERE date IS NULL  ORDER BY dataset DESC;"|psqdb`)

if ($proprequest != "" && $proposal != $proprequest) continue

set dataset = `echo "SELECT dataset FROM NOTIFY WHERE date IS NULL AND proposal='$proposal' ORDER BY dataset DESC LIMIT 1;"|psqdb`
set name = `echo "SELECT email_address, last_name FROM viewspace.principal_investigator WHERE proposal='$proposal';"|psql -Aqt -F ' '`

if ($dsrequest != "" && $dataset != $dsrequest) continue

@ l++
if ($l > $limit) break

if ($verify) then
    echo "Sending notification to $name[1] ($name[2-], $proposal, $dataset)"
    echo -n "  OK? (y|n|e|q) "; set ans = $<
    switch ("$ans")
    case e:
        set eflag = "-e"
	breaksw
    case y:
        breaksw
    default:
        break
    endsw
endif

if ($test) then
plnotify_mail $eflag -t $test_email $test_email "" "" "$name[2-]" $proposal $dataset $message
else if ($new_dataset) then
plnotify_mail $eflag $name[1] $reply "$cc" "$bcc" "$name[2-]" $proposal $dataset $message new
else
plnotify_mail $eflag $name[1] $reply "$cc" "$bcc" "$name[2-]" $proposal $dataset $message
endif

# Mark notification.
if ($test) then
set date = `date +%FT%T`
cat << EOF
UPDATE NOTIFY SET date='$date' WHERE proposal='$proposal' AND date is NULL;
EOF
foreach dataset (`echo "SELECT dataset FROM NOTIFY WHERE dataset IS NOT NULL AND proposal='$proposal';"|psqdb`)
#foreach queue (`echo "SELECT DISTINCT plqueue FROM PLDATASETS WHERE pldataset='$dataset';"|psqdb`)
#cat << EOF
#UPDATE $queue SET notified='$date' WHERE dataset='$dataset';
#EOF
#end
end
else
set date = `date +%FT%T`
cat << EOF
UPDATE NOTIFY SET date='$date' WHERE proposal='$proposal' AND date is NULL;
EOF
psqdb << EOF
UPDATE NOTIFY SET date='$date' WHERE proposal='$proposal' AND date is NULL;
EOF
foreach dataset (`echo "SELECT dataset FROM NOTIFY WHERE dataset IS NOT NULL AND proposal='$proposal';"|psqdb`)
#foreach queue (`echo "SELECT DISTINCT plqueue FROM PLDATASETS WHERE pldataset='$dataset';"|psqdb`)
#cat << EOF
#UPDATE $queue SET notified='$date' WHERE dataset='$dataset';
#EOF
#psqdb << EOF
#UPDATE $queue SET notified='$date' WHERE dataset='$dataset';
#EOF
#end
end
endif

end
endif

if (-e notify.tmp) rm notify.tmp
