#!/bin/tcsh -f
# Counts how many files have been submitted to the archive
# and how many are left for a particular dataset
# Examples:
#    stbcount
#    stbcount 20130311
#    stbcount 20130311 DEC13A_20130311_8460e09-filu-grp

set nsanames
set plnames
while ($# > 0)
    switch ($1)
    case -h:
    case --help:
        goto Usage
    case -m:
    case --mss:
        set mss; breaksw
    case -t:
    case --total:
        set total
	breaksw
    case -s:
    case --submitdir:
        set submitdir; breaksw
    case -n:
        unset plnames; breaksw
    case -p:
        unset nsanames; breaksw
    default:
        break
    endsw
    shift
end

if ($# == 0) then
    set dirs = (.)
else
    set dirs = ($*)
endif

set pushdsilent

if (! -e $dirs[1]) cd $NHPPS_DATAAPP/Final

@ totpend = 0; @ totsub = 0
#foreach dir (`find $dirs -maxdepth 3 -name '*-dts' -type d`)
foreach dir (`find $dirs -maxdepth 0 -type d`)
    pushd $dir
    set ds = `basename $dir -dts | sed 's+N964+N+'`

    rm /tmp/stbcount.tmp
    if ($?plnames) then
	ls *_[cosr][ikwde].fits.fz >>& /tmp/stbcount.tmp
	ls *_si1.fits.fz >>& /tmp/stbcount.tmp
    endif
    if ($?nsanames) then
	ls *_[zdfopihr][cokps][ijdwe]_*fz >>& /tmp/stbcount.tmp
    endif
    set j = `grep -v match /tmp/stbcount.tmp | wc -l`

    if (-e Submitted && $?plnames && $?nsanames) then
	cd Submitted
	ls *.fz >& /tmp/stbcount.tmp
	set k = `grep -v match /tmp/stbcount.tmp | wc -l`
    else
        set k = 0
    endif	

    if ($j > 0 || $k > 0) then
        @ totpend += $j; @ totsub += $k
	if (! $?total) then
	    if ($?submitdir) then
		printf "%s\n" $PWD/${ds}-dts/Submitted
	    else
		if (! $?hdr) then
		    set hdr
		    if ($?mss) then
			printf "%-45s %9s %9s %9s\n" "" Pending Submitted MSS
		    else
			printf "%-45s %9s %9s\n" "" Pending Submitted
		    endif
		endif
		if ($?mss) then
		    set l = `archivepath @/tmp/stbcount.tmp | wc -l`
		    printf "%-45s %9d %9d %9d\n" ${ds}: $j $k $l
		else
		    printf "%-45s %9d %9d\n" ${ds}: $j $k
		endif
	    endif
	endif
    endif
    popd
end

if ($totpend > 0 || $totsub > 0) then
    if (! $?total) printf "-----------------------------------------------------------------\n"
    printf "%-45s %9d %9d\n" Total: $totpend $totsub
endif

#if ($?mss) then
#    echo; lpq -s -P dts@dec01
#endif

exit

Usage:

echo 'Usage: stbcount [options] [directories]'
echo '    For each specified directory find subdirectories ending'
echo '    in -dts with fz files to show submission status'
echo '    The default directory is the current directory.'
echo '    If the specified directory is not found from the current directory'
echo '    it is sought rooted at $NHPPS_DATAAPP/Final.'
echo
echo '  -h, --help: Usage help'
echo '  -m, --mss:  Check MSS and lpq'
echo '  -s, --submitdir:  Show paths to submit directories'
echo '  -n: NSA names only'
echo '  -p: pipeline names only'
echo '  -t: print only the total line'
echo
echo 'Examples (executed from the Final/DEC13A directory):'
echo '    stbcount'
echo '    stbcount .'
echo '    stbcount -m .'
echo '    stbcount 20130311'
echo '    stbcount 20130311/*filr*'
