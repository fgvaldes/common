# GOOD_FOR_PGR -- Rule to determine if an exposure should be used for pupil ghost.

procedure good_for_pgr (filter, exptime, skymean, dmag, maxobjarea, skyfrac,
	ra, dec)

string	filter			{prompt="Filter"}
real	exptime			{prompt="Exposure time (sec)"}
real	skymean			{prompt="Mean sky rate (e/sec)"}
real	dmag			{prompt="Departure from expected magnitude"}
int	maxobjarea		{prompt="Maximum object area (pixels)"}
real	skyfrac			{prompt="Sky fraction"}
real	ra			{prompt="Field center (hr)"}
real	dec			{prompt="Field center (deg)"}

struct	*fd

begin
	struct	reason = "N"
	real	rareg, decreg, rreg, r1, d1, r2, d2
	real	c1, c2, x, y, z, sep
	string	subdir
	file	caldir = "MC$"

	# Check processing options.
	getcal ("calops", filter=filter)
	if (getcal.nfound > 0 && stridx ("G", getcal.value) > 0)
	    reason = "Y"

	# Check exposure time, sky, dmag, maximum object area, and sky fraction.
	if (reason=="Y" && !isindef(exptime) && exptime < sft_minexptime)
	    printf ("N exptime: %d < %d\n", exptime, sft_minexptime) |
	        scan (reason)
#	if (reason=="Y" && !isindef(exptime) && !isindef(skymean)) {
#	    getcal ("", "sftskycounts", cm, "", obstype="", detector="",
#	        imageid="", filter=filter, exptime="", quality=INDEF,
#		mjd=50000, dmjd=INDEF, match="", > "dev$null")
#	    if (getcal.statcode == 0) {
#		x = exptime * skymean
#		y = real (getcal.value)
#		if (x < y)
#		    printf ("N skycounts: %.2f < %.2f\n", x, y) |
#			scan (reason)
#	    }
#	}
	if (reason=="Y" && !isindef(dmag) && dmag < -1)
	    printf ("N dmag: %.2f < %.2f\n", dmag, -1) |
	        scan (reason)
#	if (reason=="Y" && !isindef(maxobjarea) && maxobjarea > sft_maxobjarea)
#	    printf ("N maxobjarea %d < %d\n", maxobjarea, sft_maxobjarea) |
#	        scan (reason)
#	if (reason=="Y" && !isindef(skyfrac) && skyfrac < sft_minskyfrac)
#	    printf ("N skyfrac %.2f < %.2f\n", skyfrac, sft_minskyfrac) |
#	        scan (reason)

	# Get region file if necessary.
#	if (reason=="Y" && !isindef(ra) && !isindef(dec) &&
#	    access(caldir // "sftregions.txt")==NO)
#	    getcal ("", "sftregions", cm, caldir,
#	        obstype="", detector="", imageid="", filter="",
#		exptime="", mjd = "50000", >& "dev$null")

	# Check regions.
	if (reason=="Y" && !isindef(ra) && !isindef(dec) &&
	    access(caldir // "sftregions.txt")==YES) {
	    fd = caldir // "sftregions.txt"
	    while (fscan (fd, rareg, decreg, rreg) != EOF) {
	        r1 = ra * 15.
		d1 = dec
	        r2 = rareg * 15.
	        d2 = decreg

		c1 = dcos(d1)
		c2 = dcos(d2)
		x = dcos(r1) * c1 - dcos(r2) * c2
		y = dsin(r1) * c1 - dsin(r2) * c2
		z = dsin(d1) - dsin(d2)
		c1 = (x*x + y*y + z*z) / 4.
		c2 = max (0., 1.-c1)
		sep = 2 * datan2(sqrt(c1),sqrt(c2))
		if (sep < rreg) {
		    printf ("N region %g < %g\n", sep, rreg) |
			scan (reason)
		    break
		}
	    }
	    fd = ""
	}

	print (reason)
end
