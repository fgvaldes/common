# ADDASTPROB -- Compute added asteroid detection probabilities.

procedure addastprob ()

string	addastcats = "addast.list"	{prompt="List of field catalogs"} 
string	addastmpcs = "addast_mpc.list"	{prompt="List of field MPC files"} 
real	match = 2		{prompt="Coordinate match (arcsec)"}
file	photref = "../photref.dat"	{prompt="Photref file"}
bool	verbose = yes		{prompt="Diagnostic output?"}

string	work = "addastprob"	{prompt="Work file root"}

struct	*fd1, *fd2

begin
	file	root, out, mpc, effdata, mc, svg, outyes, outno
	file	tmpid, tmpyes, tmpno, tmp, tmp1
	int	i, j, k
	bool	b1
	string	s1, s2, s3, s4, s5, s6, s7, s8,s9, s10, s11, s12, s13, s14
	real	d, mag1, mag2, rate1, rate2
	struct	line

	if (!defpac ("plot"))
	    plot

	# Set root name.
	head (addastmpc, nl=1) | scan (root)
	root = substr (root, strlstr("DEC",root), strlstr("VR",root)+1)

	# Filenames.
	out = root // ".txt"
	mpc = root // "_mpc.txt"
	effdata = root // "_effdata.txt"
	mc = root // "_prob.mc"
	svg = root // "_prob.svg"
	outyes = root // "_yes.txt"
	outno = root // "_no.txt"
	tmp = work//".tmp"
	tmp1 = work//"1.tmp"
	tmpid = work//"_id.tmp"
	tmpyes = work//"_yes.tmp"
	tmpno = work//"_no.tmp"

	if (access (effdata))
	    delete (effdata)

#delete (root//"*")

	# Add asteroid IDs across all fields and merge.  Sort by ID.
	#if (access(out)==NO && addastcats != "") {
	if (access(out)==NO && addastmpcs != "") {
	    if (verbose)
	        printf ("Create catalog of added asteroids: %s\n", out)
	    #files ("@"//addastcats, > tmp)

	    # Instead of the list of all cats use only those for which
	    # mpc records were found.
	    files ("@"//addastmpc, > tmp)
	    fd1 = tmp
	    while (fscan (fd1, s1) != EOF) {
	        s2 = substr (s1, 1, strstr("C_mpc",s1)-1) // "addast.txt"
		print (s2, >> tmp1)
	    }
	    list = ""
	    rename (tmp1, tmp)

	    fd1 = tmp
	    for (i=1000; fscan(fd1,s1)!=EOF; i+=1000)
		tcalc (s1, "ID", i//"+COGRPID", datatype="int")
	    fd1 = ""
	    tmerge ("@"//tmp, tmp1, "append", allcols+,
	        tbltype="default", extracol=0)
	    match ("^\#", tmp1, > out)
	    match ("^\#", tmp1, stop+) | sort (col=14, num+, >> out)
	    #printf ("!!sort -n -k 14 %s\n", tmp1) | cl (> out) 
	    delete (tmp//","//tmp1)
	}

	# Merge MPC files.
	if (access(mpc)==NO && addastmpcs != "") {
	    if (verbose)
	        printf ("Create detected MPC records: %s\n", mpc)
	    if (photref != "") {
		if (verbose)
		    printf ("Correct magnitudes\n")
		fd1 = addastmpcs
		while (fscan (fd1, s1) != EOF)
		    mpcmagfix (s1, photref=photref)
	    }
	    files ("@"//addastmpcs, > tmp1)
	    match ("^     ", "@"//tmp1, print-, > tmp)
	    delete (tmp1)

	    # Convert MPC records into a catalog.
	    print ("#c GRPID ch*7", > mpc)
	    print ("#c DATE1 ch*5", >> mpc)
	    print ("#c DATE2 ch*2", >> mpc)
	    print ("#c DATE3 ch*8", >> mpc)
	    print ("#c RA d %13.2h hours", >> mpc)
	    print ("#c DEC d %13.1h degrees", >> mpc)
	    print ("#c MAG d %4.1f", >> mpc)

	    fd1 = tmp
	    while (fscan (fd1,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,mag1) != EOF)
		printf ("%s %s %s %s %s:%s:%s %s:%s:%s %4.1f\n",
		    s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, mag1, >> mpc)
	    fd1 = ""; delete (tmp)
	}

	# Match detected against input and make list of IDs.
	if (!access(outyes)) {
	    if (verbose)
		printf ("Match detected against added asteroid catalog\n")
	    tmatch (mpc, out, tmp, "RA,DEC", "RA,DEC",
		match/3600., incol1="GRPID,DATE1,DATE2,DATE3,RA,DEC,MAG",
		incol2="", factor=" ", diagfile="", nmcol1=" ", nmcol2="",
		sphere=yes)
	    fields (tmp, 21) | sort (num+) | uniq (> tmpid)

	    # Compute magnitude calibration and add magnitude field.
	    if (verbose)
		printf ("Calibrate added asteroid magnitudes\n")
	    tproject (tmp, tmp1, "MAG COMAG CORATE")
	    fields (tmp1, "2,3,1") | sort | uniq (> tmp1)
	    surfit (tmp1, image="", coordinates="", fit="",
		function="polynomial", xorder=2, yorder=2, xterms="half",
		weighting="user", xmin=INDEF, xmax=INDEF, ymin=INDEF,
		ymax=INDEF, zmin=INDEF, zmax=INDEF) |
		fields ("STDIN", 3, lines="16-18") | table |
		    scan (d, mag1, rate1)
	    printf ("%.5g+%.5g*COMAG+%.5g*CORATE\n", d, mag1, rate1) | scan (s1)
	    tcalc (out, "MAG1", s1)
	    delete (tmp//","//tmp1)

	    # Separate found and not found.
	    if (verbose)
		printf ("Separate found and not found asteroids:\n\t%s, %s\n",
		   outyes, outno)
	    delete (outyes//","//outno, >& "dev$null")
	    fd1 = out; fd2 = tmpid; j = 0; s5 = ""
	    while (fscan(fd1,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,i,s14) != EOF) {
		if (s1 == "#c") {
		    printf ("%s %s %s %s %s\n", s1, s2, s3, s4, s5, >> outno)
		    printf ("%s %s %s %s %s\n", s1, s2, s3, s4, s5, >> outyes)
		    next
		}
		;
		if (i > j) {
		    if (fscan(fd2, j) == EOF)
			j = 1000000
		    ;
		}
		if (i == j)
		    printf ("%s %s %s %s %s %s %s %s %s %s %s %s %s %d %s\n",
			s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13,
			    i, s14, >> outyes)
		else
		    printf ("%s %s %s %s %s %s %s %s %s %s %s %s %s %d %s\n",
			s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13,
			    i, s14, >> outno)
		;
	    }
	    fd1 = ""; fd2 = ""; delete (tmpid)
	}

	if (verbose)
	    printf ("Create probability plot\n")
	mag1 = 20; mag2 = 26; rate1 = 60; rate2 = 110; k = 0; b1 = no
	for (rate1=60; rate1<360; rate1+=75) {
	    rate2 = rate1 + 75
	    printf ("COMAG>=%.1f&&COMAG<%.1f&&CORATE>=%d&&CORATE<%d\n",
		mag1, mag2, rate1, rate2) | scan (line)
	    tselect (outyes, tmp, line)
	    fields (tmp, 15) | uniq (> tmpyes)
	    delete (tmp)
	    tselect (outno, tmp, line)
	    fields (tmp, 15) | uniq (> tmpno)
	    delete (tmp)

	    tselect (outyes, tmp, line)
	    fields (tmp, 15) | uniq (> tmp1)
	    phist (tmp1, z1=20., z2=26., binwidth=0.5, nbins=512,
		autoscale=yes, top_closed=no, hist_type="normal",
		listout=yes, title="Artificial Asteroids", xlabel="Mag",
		ylabel="Counts", wx1=INDEF, wx2=INDEF, wy1=0., wy2=2000.,
		logx=no, logy=no, round=no, plot_type="box", box=yes,
		ticklabels=yes, majrx=5, minrx=5, majry=5, minry=5,
		fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=no,
		pattern="solid", device="stdgraph", > tmpyes)
	    delete (tmp//","//tmp1)

	    tselect (outno, tmp, line)
	    fields (tmp, 15) | uniq (> tmp1)
	    phist (tmp1, z1=20., z2=26., binwidth=0.5, nbins=512,
		autoscale=yes, top_closed=no, hist_type="normal",
		listout=yes, title="Artificial Asteroids", xlabel="Mag",
		ylabel="Counts", wx1=INDEF, wx2=INDEF, wy1=0., wy2=2000.,
		logx=no, logy=no, round=no, plot_type="box", box=yes,
		ticklabels=yes, majrx=5, minrx=5, majry=5, minry=5,
		fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=yes,
		pattern="dashed", device="stdgraph", > tmpno)
	    delete (tmp//","//tmp1)

	    join (tmpyes, tmpno, out=tmp)
	    fd1 = tmp
	    while (fscan (fd1, x, i, x, j) != EOF) {
		j += i
		if (j == 0)
		    next
		#x += 0.25
		y = real (i) / j
		printf ("%4.1f %4.2f\n", x, y, >> tmp1)
	    }
	    fd1 = ""

	    k += 1
	    if (b1 == no && access(mc))
	        delete (mc)
	    printf ("\nrate %d - %d:\n", rate1, rate2, >> effdata)
	    concat (tmp1, >> effdata)
	    graph (tmp1, wx1=20, wx2=26, wy1=0., wy2=1., wcs="logical",
		axis=1, transpose=no, pointmode=no, marker="box",
		szmarker=0.005, ltypes=k, colors=k, logx=no,
		logy=no, box=yes, ticklabels=yes, xlabel="Magnitude",
		ylabel="Probability", xformat="wcsformat", yformat="",
		title="Simulated Asteroids: "//root, lintran=no,
		p1=0., p2=0., q1=0., q2=1., vx1=0., vx2=0., vy1=0.,
		vy2=0., majrx=5, minrx=5, majry=6, minry=5, overplot=no,
		append=b1, device="stdgraph", round=no, fill=yes, >>G mc)
	    b1 = yes

	}
	if (access(mc)) {
	    if (access(svg))
		delete (svg)
	    stdplot (mc, dev="g-svg")
	    sleep (5)
	    files ("*.svg") | scan (s1)
	    if (access(s1))
		rename (s1, svg)
	}

	delete (work//"*tmp")
end
