# ALLREVIEW -- This is a quick script to review detections given in MPC records.

procedure allreview (quality)

string	quality			{prompt="Quality level (A|B)"}
string	ids = ""		{prompt="ID source"}
string	level = "[hm]?"		{prompt="Interest file pattern"}
string	movdir = "MOV/"		{prompt="Directory with MOV files"}
file	rates = ""		{prompt="Rates output"}

struct	*fd

begin
	string	mpc
	file	exps
	string	movfile
	string	work

	string	q, last, movid, stbid, ds, mov, dir, id
	int	i, j, grpid

	# Set parameters.
	q = quality
	id = ids
	mpc = "*_"//q//"_"//level//".txt"
	exps = "*_"//q//"_exp.cat"
	movfile = "_"//q//"_mov.fits"
	work = "allreview"//q

	# Initialize.
	delete (work//"*.tmp")

	# Make a list of identifiers.
	if (id == "")
	    id = mpc
	files (id, > work//"1.tmp")
	count (work//"1.tmp") | scan (i)
	if (i < 1)
	    return

	fd = work//"1.tmp"
	while (fscan (fd, movid) != EOF) {
	    if (access(movid))
		concat (movid, work//".tmp", append+)
	    else
	        print (movid, >> work//".tmp")
	}
	fd = ""; delete (work//"1.tmp")
	        

	fd = work//".tmp"; last = ""
	while (fscan (fd, movid, stbid) != EOF) {
	    if (nscan () > 1) {
		if (substr(stbid,1,3) != "C20")
		    next
	    }
	    if (movid == last)
	        next
	    last = movid

	    ds = ""; match (movid, mpc, print-) | scan (ds)
	    if (ds == "")
		next

	    movid (movid=movid, dir="from", v-)
	    stbid = movid.stbid
	    grpid = movid.grpid
	    ds = ""; match (stbid, exps, print-) | scan (ds)
	    if (ds == "")
		next
	    mov = movdir // ds // movfile
	    if (!access(mov)) {
	        i = stridx("-",ds)
	        j = strldx("-",ds)
		dir = substr(ds,1,i) // "fil" // substr(ds,i+1,j-1) // "-grp-stk" // substr(ds,j+1,999) // "/"
	        mov = "NHPPS_DATAAPP$/DCP_STK/" // dir // mov
#print (mov)
	    }
	    if (!access(mov))
	        next
	    printf ("%s = %s/%d\n", movid, ds, grpid)
	    if (access(work//".list")) {
		match (movid, work//".list") | count | scan (i)
		if (i > 0)
		    next
	    }
	    movreview (movfile=mov, id=movid, onlyone+, rates=rates)
	    print (movid, >> work//".list")
	}
	fd = ""; delete (work//"*.tmp")
end
