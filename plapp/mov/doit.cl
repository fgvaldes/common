s1 = "DEC13A_20130415_test2-r-112670M_addast.txt"
s2 = "DEC13A_20130415_test2-r-112670M_C_obs.cat"
s3 = "DEC13A_20130415_test2-r-112670M_C_grp.cat"

#delete ("test_join.cat")
#tjoin (s2, s3, "test_join.cat", "groupid", "groupid", extrarows="neither",
#    tolerance="0.0", casesens=yes)

delete ("testmatch.tmp")
tmatch (s1, "test_join.cat", "testmatch.tmp", "RA,DEC", "RA_OBS,DEC_OBS",
    00:00:02, incol1="MAG,RATE,PA,ID",
    incol2="GROUPID,MAG,NOBS,PA,RATE", factor=" ",
    diagfile="test_diag", nmcol1="MAG,RATE,PA,ID",
    nmcol2="GROUPID,MAG,NOBS,PA,RATE", sphere=yes)

#delete ("testmatch1.tmp")
#tquery ("testmatch.tmp", "testmatch1.tmp", "yes", "", "MAG_1") 

