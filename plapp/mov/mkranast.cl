# Make a random asteroid distribution
#
# The distributions are not intended to be realistic but to sample the
# interesting detection space.

procedure mkranast ()

int	num = 300		{prompt = "Number of asteroids"}
real	radius = 4000		{prompt = "Radius from tangent point (arcsec)"}
real	minmag = 22.0		{prompt = "Minimum magnitude"}
real	maxmag = 26.0		{prompt = "Minimum magnitude"}
real	magpow = 2.		{prompt = "Magnitude power law distribution"}
real	minrate = 60.		{prompt = "Minimum rate (arcsec/hr)"}
real	maxrate = 360.		{prompt = "Maximum rate (arcsec/hr)"}
real	ratepow = 0.25		{prompt = "Rate power law distribution"}
int	seed = INDEF		{prompt = "Random number seed"}

struct	*fd

begin
	real	p1, p2, p3, p4
	real	ran, r1, r2, r3, r4, r5

	p1 = minmag ** magpow
	p2 = maxmag ** magpow
	p3 =  minrate ** ratepow
	p4 =  maxrate ** ratepow

	# Generate random numbers.
	urand (num, 5, seed=seed, nd=4, scale=1., > "mkranast.dat")

	fd = "mkranast.dat"
	while (fscan (fd, r1, r2, r3, r4, r5) != EOF) {
	    ran = r1
	    r2 *= 360
	    r1 = radius * ran * dcos (r2)
	    r2 = radius * ran * dsin (r2)
	    r3 = p2 * r3 + p1 * (1 - r3)
	    r3 = r3 ** (1./ magpow)
	    r4 = p4 * r4 + p3 * (1 - r4)
	    r4 = r4 ** (1. / ratepow)
	    r5 *= 360

	    print (r1, r2, r3, r4, r5)
	}
	list = ""; delete ("mkranast.dat", ver-)
end
