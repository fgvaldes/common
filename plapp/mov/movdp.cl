# MOVDP -- Create moving data products.
#
# See: http://www.minorplanetcenter.net/iau/info/ObsDetails.html
# See: 

procedure movdp ()

file	input = "*_mov.fits"	{prompt="Input cutout file"}
string	output = ""		{prompt="Output root name"}
string	select = "AA?*"		{prompt="Extensions to select"}
bool	update = yes		{prompt="Update cutout file?"}
bool	fast = no		{prompt="Fast update?"}
file	movdata = "NHPPS_PIPEAPPSRC$/mov/movdata/" {prompt="Data files"}

string	cod = "W84"			{prompt="Observatory code"}
string	con1 = "L. Allen, NOAO"		{prompt="Contact information"}
string	con2 = "950 N. Cherry Ave., P.O. Box 26732, Tucson, AZ 85726"
string	con3 = "P.I. 2013B-0536"
string	con4 = "[lallen@noao.edu]"
string	obs = "L. Allen, D. Trilling, D. James, D. Herrera, T. Axelrod, J. Rajagopal"	{prompt="List of observers"}
string	mea = "F. Valdes"		{prompt="List of measurers"}
string	tel = "4.0-m CTIO reflector + DECam"	{prompt="Telescope details"}
string  net = "2MASS,PS1,USNO-B1"	{prompt="List of calibration catalogs"}
string	bnd = "V"			{prompt="Band"}
string	com1 = "The DECam NEO Survey"	{prompt="Comments"}
string	com2 = "NOAO CP + MODS Tracklets"
string	com3 = ""
string	com4 = ""
string	ack = "2013B-0536: NOAO CP + MODS Tracklets"	{prompt="Acknowledgement"}
string	ac2 = "valdes@noao.edu"		{prompt="List of acknowledgement emails"}


struct	*fd

begin
	bool	upd
	int	i, j, clid, nexp, ngrp, nobs, nmag, cllast, row
	real	xi, eta, xi1, xi2, eta1, eta2, t, t1, t2, mag, magav
	real	rate, pa
	file	in, out, out1, out2, out3, im, tmp, tmp1, tmp2, tmp3, tmp4
	string	dataset, procid, stbid, filt, mpc, mpctxt

	int	ival, ival1
	real	rval, rval1
	string	sval
	struct  lval

	# Get query parameters.
	in = input
	out = output
	upd = update

	# Set temporary files.
	tmp = mktemp ("mov")
	tmp1 = tmp // "1.tmp"
	tmp2 = tmp // "2.tmp"
	tmp3 = tmp // "3.tmp"
	tmp4 = tmp // "4.tmp"

	# Set input cutout file.
	files (in) | count | scan (i)
	if (i != 1)
	    error (1, "Unknown or ambiguous input file\n")
	files (in) | scan (in)
	i = strstr (".fits", in) - 1
	if (i > 0)
	    in = substr (in, 1, i)
	if (upd) {
	    imrename (in//".fits", tmp//".fits")
	    imcopy (tmp//"[0]", in, verbose-)
	    mscextract (tmp, in, extname=select, verbose-)
	    imdelete (tmp)
	} else {
	    #imcopy (in//"[0]", tmp, verbose-)
	    #mscextract (in, tmp, extname=select, verbose-)
	    #in = tmp
	    ;
	}

	# Set output.
	if (out == "") {
	    out = in
	    i = strstr ("_mov", out) - 1
	    if (i > 0)
		out = substr (out, 1, i)
	}

	if (update) {
	    # Create catalog.
	    hselect (in//"[0]", "CODATA,COGAMMA,COBETA", yes) |
	        scan (sval, xi, eta)
	    print ("#c CODATA ch*48", > tmp1)
	    print ("#c COGRPID i %4d", >> tmp1)
	    print ("#c COEXPID i %2d", >> tmp1)
	    print ("#c CORA d %12.2h hr", >> tmp1)
	    print ("#c CODEC d %12.1h deg", >> tmp1)
	    print ("#c COTSEP r %8.1f sec", >> tmp1)
	    print ("#c COMAG r %6.2f", >> tmp1)
	    print ("#c EXTNAME ch*16 %-16s", >> tmp1)
	    printf ("#k COGAMMA = %12.2h\n", xi, >> tmp1)
	    printf ("#k COBETA = %12.2h\n", eta, >> tmp1)
	    mscselect (in, "CODATA,COGRPID,COEXPID,CORA,CODEC,COTSEP,COMAG,EXTNAME",
	        expr="(CORA!='INDEF')", extname=select, >> tmp1)
	    tinfo (tmp1, ttout-)
	    ngrp = tinfo.nrows
	    if (ngrp > 0) {
		tcalc   (tmp1, "N", "ROWNUM", datatype="int") 
#		acecopy (tmp1, tmp2, catdef=movdata//"movdp.dat",
#		    filter="", verb-)
#		delete (tmp1, verify-)
#		rename (tmp2, tmp1)
		tsort (tmp1, "COGRPID,COTSEP", ascend+)

		# Make cluster catalog.
#		tdump (tmp1, cdfile="", pfile="", datafile=tmp3,
#		    columns="COGRPID,XI,ETA,COTSEP,COMAG", rows="-", pwidth=-1)
		tdump (tmp1, cdfile="", pfile="", datafile=tmp3,
		    columns="COGRPID,CORA,CODEC,COTSEP,COMAG", rows="-", pwidth=-1)
		fd = tmp3; cllast = 0; nobs = 0; ngrp = 0
		while (fscan (fd, clid, xi, eta, t, mag) != EOF) {
		    if (clid != cllast) {
			# Output cluster information.
			if (nobs > 0) {
			    ngrp += 1
			    t1 = max (0.01, (t2 - t1) / 3600.)
			    movmotion (xi1, eta1, t1, ra2=xi2, dec2=eta2) |
			        scan (rate, pa)
if (rate > 500) {
print (xi1, eta1, t1, xi2, eta2)
print (rate, pa)
}
#			    xi1 = (xi2 - xi1) / t1
#			    eta1 = (eta2 - eta1) / t1
#			    pa = datan2 (-xi1, eta1)
#			    rate = sqrt (xi1**2 + eta1**2)
			    if (nmag > 0)
				magav /= nmag
			    printf ("%d %d %6.1f %6.1f %5.1f\n",
				cllast, nobs, rate, pa, magav, >> tmp2)
			}

			# Initialize new cluster.
			cllast = clid
			nobs = 0
			nmag = 0
			xi1 = xi
			eta1 = eta
			t1 = t
			magav = 0.
		    }

		    nobs += 1
		    xi2 = xi
		    eta2 = eta
		    t2 = t
		    if (!isindef(mag)) {
			nmag += 1
			magav += mag
		    }
		}
		fd = ""; delete (tmp3, verify-)

		# Output cluster information.
		if (nobs > 0) {
		    ngrp += 1
		    t1 = max (0.01, (t2 - t1) / 3600.)
		    movmotion (xi1, eta1, t1, ra2=xi2, dec2=eta2) |
			scan (rate, pa)
if (rate > 500) {
print (xi1, eta1, t1, xi2, eta2)
print (rate, pa)
}
#		    xi1 = (xi2 - xi1) / t1
#		    eta1 = (eta2 - eta1) / t1
#		    pa = datan2 (-xi1, eta1)
#		    rate = sqrt (xi1**2 + eta1**2)
		    if (nmag > 0)
			magav /= nmag
		    printf ("%d %d %6.1f %6.1f %5.1f\n",
			cllast, nobs, rate, pa, magav, >> tmp2)
		}

		# Join cluster information.
		tjoin (tmp1, tmp2, tmp3, "COGRPID", "c1", extrarows="neither",
		    tolerance="0.0", casesens=yes)
		delete (tmp1, verify-)
		delete (tmp2, verify-)

		# Update cutout file.
		if (fast)
		    tdump (tmp3, cdfile="", pfile="", datafile=tmp4,
			columns="CODATA,COGRPID,C2,C3,C4,C5",
			rows="-", pwidth=-1)
		else {
		    tdump (tmp3, cdfile="", pfile="", datafile=tmp1,
			columns="EXTNAME,C2,C3,C4,C5",
			rows="-", pwidth=-1)
		    delete (tmp3, verify-)
		    fd = tmp1
		    while (fscan(fd,im,nobs,rate,pa,mag)!=EOF) {
			printf ("%s[%s]\n", in, im) | scan (im)
			hedit (im, "CONOBS", nobs, verify-, show-, update+)
			hedit (im, "CORATE", rate, verify-, show-, update+)
			hedit (im, "COPA", pa, verify-, show-, update+)
			hedit (im, "COMAG", mag, verify-, show-, update+)
		    }
		    fd = ""
		}
	    }
	    delete (tmp1, verify-, >& "dev$null")
	    hedit (in//"[0]", "CONGRP", ngrp, verify-, show-, update+)
	}

	hselect (in//"[0]", "CODATA,CONGRP", yes) | scan (sval, ngrp)
	i = stridx ("-", sval)
	if (i > 0) {
	    dataset = substr (sval, 1, i-1)
	    j = strldx ("_", dataset)
	    if (j > 0) {
		dataset = substr (sval, 1, j-1) // substr (sval, i, 999)
		procid = substr (sval, j+1, i-1)
	    } else
	        procid = "unknown"
	} else {
	    dataset = sval
	    procid = "unknown"
	}

	# Make output tables.

	# Initialize SQL file.
	out2 = out // ".sql"
	if (access(out2))
	    delete (out2, verify-)
	concat (movdata//"movdp.sql", out2)

	# Dataset table.
	out1 = out // "_ds.cat"
	print ("#c dataset ch*48", > out1)
	print ("#c cutouts ch*52", >> out1)
	print ("#c nexposures i %2d", >> out1)
	print ("#c date ch*24", >> out1)
	print ("#c mjd d %15.8f", >> out1)
	print ("#c lst d %13.1h hr", >> out1)
	print ("#c ra_field d %13.2h hr", >> out1)
	print ("#c dec_field d %13.1h deg", >> out1)
	print ("#c gamma_field d %13.1h deg", >> out1)
	print ("#c beta_field d %13.1h deg", >> out1)
	print ("#c opposition d %13.1h deg", >> out1)
	print ("#c filter ch*64", >> out1)
	print ("#c ngrp i %3d", >> out1)
	hselect (in//"[0]",
	    "CODATA,COFILE,CONEXP,CODATE,COMJD,COLST,CORACEN,CODECCEN,COGAMMA,COBETA,CORACEN,COFILTER,CONGRP",
	    yes, >> out1)
	hselect (in//"[0]", "COMJD,CORACEN", yes) | scan (t, xi)
	suncoord (t, verbose-)
	pa = (mod (suncoord.ra + 12, 24) - xi) * 15
	if (pa < -180)
	    pa += 360
	if (pa > 180)
	    pa -= 360
	partab (pa, out1, "OPPOSITION", 1)

	tdump (out1, cd="", pf="", data="STDOUT", col="", rows="-", pwidth=-1) |
	    words (> tmp1)
	fd = tmp1
	while (fscan (fd, sval) != EOF) {
	    printf ("REPLACE INTO MOVDS VALUES ('%s',\n'%s',\n",
		dataset, procid, >> out2)
	    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
	    i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
	    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
	    i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.2h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, lval); printf ("'%s',\n", lval, >> out2)
	    i = fscan (fd, ival); printf ("%d);\n",    ival, >> out2)
	}
	fd = ""; delete (tmp1)

	# Exposure table.
	hselect (in//"[0]", "CONEXP", yes) | scan (nexp)
	out1 = out // "_exp.cat"
	print ("#c dataset ch*48", > out1)
	print ("#c expname ch*48", >> out1)
	print ("#c dateobs ch*32", >> out1)
	print ("#c mjdobs d %15.8f", >> out1)
	print ("#c expid i %2d", >> out1)
	print ("#c tsep r %d sec", >> out1)
	print ("#c exptime r %d sec", >> out1)
	for (i = 1; i <= nexp; i += 1) {
	    hselect (in//"[0]",
	        "CODATA,COIM"//i//",CODATE"//i//",COMJD"//i//",COEXPI"//i//",COTSEP"//i//",COEXPT"//i,
		yes, >> out1)
	}
	tsort (out1, "tsep", ascend+)

	tdump (out1, cd="", pf="", data="STDOUT", col="", rows="-", pwidth=-1) |
	    words (> tmp1)
	fd = tmp1
	while (fscan (fd, sval) != EOF) {
	    printf ("REPLACE INTO MOVEXP VALUES ('%s',\n'%s',\n",
		dataset, procid, >> out2)
	    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
	    i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
	    i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
	    i = fscan (fd, rval); printf ("%g);\n",   rval, >> out2)
	}
	fd = ""; delete (tmp1)

	if (ngrp > 0) {

	    # Group catalog.
	    out1 = out // "_grp.cat"
	    
	    print ("#c dataset ch*48", > out1)
	    print ("#c groupid i %4d", >> out1)
	    print ("#c nobs i %2d", >> out1)
	    print ("#c rate r %5.1f arcsec/hr", >> out1)
	    print ("#c pa r %4d deg", >> out1)
	    print ("#c mag r %5.1f", >> out1)
	    if (access(tmp4)) {
	        concat (tmp4, >> out1)
		delete (tmp4)
	    } else
		mscselect (in, "CODATA,COGRPID,CONOBS,CORATE,COPA,COMAG",
		    expr="(CORA!='INDEF')", >> out1)
	    tproject (out1, tmp1, "", uniq+)
	    rename (tmp1, out1)
	    tsort (out1, "rate", ascend+)

	    tdump (out1, cd="", pf="", data="STDOUT",
	        col="", rows="-", pwidth=-1) | words (> tmp1)
	    fd = tmp1
	    while (fscan (fd, sval) != EOF) {
		printf ("REPLACE INTO MOVGRP VALUES ('%s',\n'%s',\n",
		    dataset, procid, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
		i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
		i = fscan (fd, rval); printf ("%g);\n",   rval, >> out2)
	    }
	    fd = ""; delete (tmp1)

	    # Observation catalog.
	    out1 = out // "_obs.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c groupid i %4d", >> out1)
	    print ("#c expid i %2d", >> out1)
	    print ("#c imageid ch*16", >> out1)
	    print ("#c ccd i %02d", >> out1)
	    print ("#c ra_obs d %13.2h hr", >> out1)
	    print ("#c dec_obs d %13.1h deg", >> out1)
	    print ("#c x d %6.1f pix", >> out1)
	    print ("#c y d %6.1f pix", >> out1)
	    print ("#c extname ch*16", >> out1)
	    mscselect (in, "CODATA,COGRPID,COEXPID,COIMID,CCDNUM,CORA,CODEC,COPX,COPY,EXTNAME",
		expr="(CORA!='INDEF')", >> out1)
	    tsort (out1, "groupid,expid", ascend+)

	    # MPC catalog and text.
	    out1 = out // "_mpc.cat"
	    mpctxt = out // "_mpc.txt"

	    printf ("COD %.3s\n", cod, > mpctxt)
	    if (con1 != "")
		printf ("CON %.77s\n", con1, >> mpctxt)
	    if (con2 != "")
		printf ("CON %.77s\n", con2, >> mpctxt)
	    if (con3 != "")
		printf ("CON %.77s\n", con3, >> mpctxt)
	    if (con4 != "")
		printf ("CON %.77s\n", con4, >> mpctxt)
	    if (obs != "")
		printf ("OBS %.77s\n", obs, >> mpctxt)
	    if (mea != "")
		printf ("MEA %.77s\n", mea, >> mpctxt)
	    if (tel != "")
		printf ("TEL %.77s\n", tel, >> mpctxt)
	    if (net != "")
		printf ("NET %.77s\n", net, >> mpctxt)
	    if (bnd != "")
		printf ("BND %-.2s\n", bnd, >> mpctxt)
	    if (com1 != "")
		printf ("COM %.77s\n", com1, >> mpctxt)
	    if (com2 != "")
		printf ("COM %.77s\n", com2, >> mpctxt)
	    if (com3 != "")
		printf ("COM %.77s\n", com3, >> mpctxt)
	    if (com4 != "")
		printf ("COM %.77s\n", com4, >> mpctxt)
	    if (ack != "")
		printf ("ACK %.77s\n", ack, >> mpctxt)
	    if (ac2 != "")
		printf ("AC2 %.77s\n", ac2, >> mpctxt)

	    print ("#c dataset ch*48", > out1)
	    print ("#c movid ch*6", >> out1)
	    print ("#c groupid i %4d", >> out1)
	    print ("#c expid i %2d", >> out1)
	    print ("#c mpcrec ch*80", >> out1)

	    mscselect (in, "$SB_ID") | sort | scan (stbid)
	    if (stbid == "INDEF")
		mscselect (in, "$FILENAME") | sort | scan (stbid)
	    tjoin (out//"_ds.cat", out//"_exp.cat", tmp1,
	        "DATASET", "DATASET")
	    tjoin (tmp1, out//"_obs.cat", tmp2,
	        "DATASET,EXPID", "DATASET,EXPID")
	    tjoin (tmp2, out//"_grp.cat", tmp3,
	        "DATASET,GROUPID", "DATASET,GROUPID")
	    delete (tmp//"[12].tmp", verify-)
	    tdump (tmp3, cd="", pf="", data=tmp1,
	        col="DATEOBS,EXPTIME,GROUPID,EXPID,TSEP,MAG,RA_OBS,DEC_OBS", rows="-", pwidth=-1)
	    delete (tmp3, verify-)

	    fd = tmp1
	    for (row=1; fscan(fd,sval,t,ival,ival1,rval,mag,xi,eta)!=EOF; row+=1) {
		mpc = ""
		movid (stbid=stbid, grpid=ival, dir="to", verbose-)
		#printf ("%11.11s\n", movid.movid) | scan (lval)
		printf ("     %-7.7s\n", movid.movid) | scan (lval)
		mpc += lval
	        print (sval) | translit ("STDIN", "T-", " ", del-) |
		    scan (i, j, t1, t2)
		#t1 += (t2 + rval / 3600) / 24
		t1 += t2 / 24
		t1 += t / 2 / 3600. / 24.
		if (access (tmp2)) {
		    sval = ""; match (mpc//"*", tmp2, meta=no) | scan (sval)
		}
		if (sval == "") {
		    printf ("* C%04d %02d %08.5f\n", i, j, t1) | scan (lval)
		} else {
		    printf ("/ C%04d %02d %08.5f\n", i, j, t1) | scan (lval)
		    mag = INDEF
		}
		mpc += lval
	        printf ("%011.2h\n", xi) |
		    translit ("STDIN", ":", " ", del-) | scan (lval)
		printf (" %11s\n", lval) | scan (lval)
		mpc += lval
	        printf ("%010.1h\n", abs(eta)) |
		    translit ("STDIN", ":", " ", del-) | scan (lval)
		if (eta < 0)
		    printf (" -%10s\n", lval) | scan (lval)
		else
		    printf (" +%10s\n", lval) | scan (lval)
		mpc += lval
		if (isindef(mag))
		    printf ("%9w%5w %2w\n") | scan (lval)
		else
		    printf ("%9w%5.1f %2w\n", mag) | scan (lval)
		mpc += lval
		printf ("%5w%.3s\n", cod) | scan (lval)
		mpc += lval
		printf ("%s %s %d %d '%s'\n",
		    dataset, movid.movid, ival, ival1, mpc) |
		    translit ("STDIN", "/", " ", >> out1)
		print (mpc, >> tmp2)
	    }
	    fd = ""; delete (tmp1)

	    sort (tmp2) | translit ("STDIN", "*/", " ", >> mpctxt)
	    delete (tmp2)
	    digest2 (mpctxt, > out//"_digest2.txt")

	    tdump (out1, cd="", pf="", data="STDOUT",
	        col="", rows="-", pwidth=-1) | words (> tmp1)
	    fd = tmp1
	    while (fscan (fd, sval) != EOF) {
		printf ("REPLACE INTO MOVMPC VALUES ('%s',\n'%s',\n",
		    dataset, procid, >> out2)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, lval); printf ("'%s');\n", lval, >> out2)
	    }
	    fd = ""; delete (tmp1)
	} else
	    printf ("WARNING: No groups found\n")

	if (!upd)
	    imdelete (tmp)

end
