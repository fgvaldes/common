# MOVCLASSES -- Make emails by digest2 interest levels.

procedure movmail (mpc, digest, subject, outhi, outme, outlo)

file	mpc			{prompt="MPC file to be divided"}
file	digest			{prompt="Digest2 file"}
string	subject			{prompt="Subject/ACK string"}
file	outhi			{prompt="100 interest file"}
file	outme			{prompt="High interest file"}
file	outlo			{prompt="Low interest file"}

int	th = 40			{prompt="Interest threshold level"}

string	to = "obs@cfa.harvard.edu"
string	from = "Francisco Valdes <valdes@noao.edu>"
string	cchi = "lallen@noao.edu,David.Trilling@nau.edu,cfuentes@labxg.cl,herrera@noao.edu,djj@ctio.noao.edu"
string	ccme = "lallen@noao.edu,David.Trilling@nau.edu,cfuentes@labxg.cl,herrera@noao.edu,djj@ctio.noao.edu"
string	cclo = ""
string	bcc = "valdes@noao.edu"

struct	*fd

begin
	file	m, d, sub
	file	out, oh, om, ol
	string	id, lastid, cc
	real	rms, interest
	struct	mpcrec, digestrec, rest, s

	# Set query parameters.
	m = mpc
	d = digest
	sub = subject
	oh = outhi
	om = outme
	ol = outlo

	# Check digest file.
	if (!access(d))
	    return
	if (access(oh))
	    delete (oh)
	if (access(om))
	    delete (om)
	if (access(ol))
	    delete (ol)

	# Read mpc file and divide by interest levels.
	fd = m; lastid = ""
	while (fscan (fd, mpcrec) != EOF) {
	    if (substr(mpcrec,1,1) != ' ')
	        next
	    if (fscan (mpcrec, id) < 1)
	        next
	    if (id != lastid) {
		lastid = id
	        match (id, d) | scan (digestrec) 
		if (fscan (digestrec, id, rms, interest, rest) < 4)
		    next
	    }
	    if (interest >= 99.5 && oh != "")
	        out = oh
	    else if (interest >= th)
	        out = om
	    else
	        out = ol
	    if (out == "")
	        next

	    if (!access(out)) {
	        printf ("To: %s\n", to, >> out)
	        printf ("From: %s\n", from, >> out)
	        printf ("Reply-to: %s\n", from, >> out)
		if (interest >= 99.5) {
		    printf ("%s: 100%% Interest\n", sub) | scan (s)
		    cc = cchi
		} else if (interest >= th) {
		    printf ("%s: Interest >= %d%%\n", sub, th | scan (s)
		    cc = ccme
		} else {
		    #printf ("%s: Interest < %d%%\n", sub, th) | scan (s)
		    printf ("%s: Interest < %d%%\n", "DECam IA Survey", th) | scan (s)
		    cc = cclo
		}
		printf ("Subject: %s\n", s, >> out)
		if (cc != "")
		    printf ("Cc: %s\n", cc, >> out)
		if (bcc != "")
		    printf ("Bcc: %s\n", bcc, >> out)
		match ("^ ", m, stop+) | match ("^ACK", stop+, >> out)
		printf ("ACK %s\n", s, >> out)
	    }
	    print (mpcrec, >> out)
	}
	fd = ""
end
