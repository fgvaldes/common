# MOVMOTION -- Compute either rate and position angle from two positions
# or a second position from one postion and a rate and position angle.
# This requires the desired quantities to be set to INDEF.
#
# The computation of rate and position uses full spherical trigonometry
# but the reverse uses plane trigonometry assuming small motions not at the
# pole.

procedure movmotion (ra, dec, dt)

real	ra			{prompt = "RA (hours)"}
real	dec			{prompt = "DEC (degrees)"}
real	dt			{prompt = "Time interval(arb unit)"}
real	ra2 = INDEF		{prompt = "RA (hours)"}
real	dec2 = INDEF		{prompt = "DEC (degrees)"}
real	rate = INDEF		{prompt = "Rate (arcsec/time unit)"}
real	pa = INDEF		{prompt = "Position angle (deg)"}

begin
	real	r1, d1, r2, d2, t, r, p
	real	x1, y1, z1, x2, y2, z2
	real	cosd1, sind1, cosd2, sind2

	r1 = 15 * ra
	d1 = dec
	t = dt

	# Compute rate and position angle.
	if (isindef(rate) && isindef(pa)) {

	    r2 = 15 * ra2
	    d2 = dec2

	    cosd1 = dcos (d1)
	    sind1 = dsin (d1)
	    x1 = dcos (r1) * cosd1
	    y1 = dsin (r1) * cosd1
	    z1 = sind1

	    cosd2 = dcos (d2)
	    sind2 = dsin (d2)
	    x2 = dcos (r2) * cosd2
	    y2 = dsin (r2) * cosd2
	    z2 = sind2

	    r = ((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2) / 4
	    r = 2 * datan2 (sqrt(r), sqrt(max(0.,1.-r)))
	    r *= 3600 / dt

	    z1 = r2 - r1
	    y1 = dsin(z1) * cosd2
	    x1 = sind2 * cosd1 - cosd2 * sind1 * dcos(z1)
	    p = datan2 (y1, x1)

	    printf ("%.4g %.1f\n", r, p)

	# Compute ra2 and dec2.
	} else if (isindef(ra2) && isindef(dec2)) {

	    r = rate * dt / 3600
	    p = pa

	    d2 = d1 + r * dcos(pa)
	    r2 = r1 + r * dsin(pa) / dcos(d2)

	    printf ("%.2H %.1h\n", r2, d2)

	# Error
	} else
	    error (1, "Either ra2/dec2 or rate/pa must be INDEF")
end
