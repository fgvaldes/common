procedure movselect (icat, ocat)

file	icat			{prompt="Input catalog"}
file	ocat			{prompt="Output catalog"}

int	maxclust = 10		{prompt="Maximum cluster size to select"}
int	minexp = 3              {prompt="Min exposures to select"}
real    minerr = 6              {prompt="Min rate error to select"}
real    maxerr = 10             {prompt="Max rate error to reject"}
real    minrate = 2             {prompt="Min rate to select"}
real    maxrate = 999           {prompt="Max rate to select"}

file	addast = ""		{prompt="Added asteroid list"}

string	work = "movselect"	{prompt="Working rootname"}

struct	*fd1, *fd2, *fd3

begin
	bool	doA, doB, doC, doD
	file	ic, oc, oc1, oc2, w1, w2, w3, w4, w5, w6, w7
	int	c, c1, clast, nclust
	int	i, j, n
	real	r, d, t, r1, d1, m1, t1, r2, d2, m2, t2, r3, d3, m3, t3
	real	sumr, sumd, summ, sumrr, sumdd
	string	class
	struct	rec

	# Initialize.
	doA = NO; doB = NO; doC = NO; doD = NO
	w1 = work // "1.tmp"
	w2 = work // "2.tmp"
	w3 = work // "3.tmp"
	w4 = work // "4.tmp"
	w5 = work // "5.tmp"
	w6 = work // "6.tmp"
	w7 = work // "7.tmp"
	delete (work//"[1-9].tmp")

	# Set input query parameters.
	ic = icat
	oc = ocat
	c = strldx (".", oc)
	if (c > 0) {
	    oc1 = substr (oc, 1, c-1)
	    oc2 = substr (oc, c, 999)
	} else {
	    oc1 = oc
	    oc2 = ""
	}

	# Sort by cluster ID.
	movsrtsel (ic, > w1)

	# Initialize matching table for finding duplicates and added asteroids.
	print ("#c id i %5d", >> w4)
	print ("#c ra d %13.2h hours", >> w4)
	print ("#c dec d %13.1h degrees", >> w4)
	print ("#c mag d %4.1f", >> w4)
	print ("#c t d %.1f sec", >> w4)
	print ("#c n i %2d", >> w4)

	# Collect each cluster, evaluate, and record.
	fd1 = w1; clast = 0; nclust = 0
	while (YES) {
	    if (fscan (fd1, c, r1, d1, m1, t1) == EOF) {
	        if (clast == EOF)
		    break
	        c = EOF
	    } else if (nscan() < 5)
	        next
	    if (c == clast) {
		nclust += 1
		print (c, r1, d1, m1, t1, >> w2)
		clast = c
		next
	    }
	    if (nclust < minexp) {
		nclust = 1
		print (c, r1, d1, m1, t1, > w2)
		clast = c
		next
	    }
	    clast = c

	    # We now have a cluster so evaluate.
	    fd2 = w2; n = 0; sumr = 0; sumrr = 0; sumd = 0; sumdd = 0; summ = 0
	    for (i=1; fscan(fd2,c,r2,d2,m2,t2)!=EOF; i+=1) {
	        summ += m2
		fd3 = w2
		for (j=1; fscan (fd3,c,r3,d3,m3,t3)!=EOF; j+=1) {
		    if (j <= i)
		        next
		    t = (t3 - t2) / 3600.
		    if (abs(t) > 0.0001) {
		       r = 54000 * (r2 - r3) * dcos(d3) / t
		       d = 3600 * (d2 - d3) / t
		       sumr += r; sumrr += r * r
		       sumd += d; sumdd += d * d
		       n += 1
		    }
		}
		fd3 = ""
	    }
	    fd2 = ""

	    if (n > 1 && nclust > 1) {
		sumr /= n; sumd /= n; summ /= nclust
		sumrr = sqrt (max(0., sumrr/n - sumr**2))
		sumdd = sqrt (max(0., sumdd/n - sumd**2))
		r = sqrt (max(0., sumr**2+sumd**2))
		d = sqrt (max(0., sumrr**2+sumdd**2))

		# Catagorize and output desired classes.
		if (nclust<=maxclust && d<=maxerr && r>=minrate && r<=maxrate) {
		    if (d<=minerr && d/max(1,r) < 0.25 && nclust>3) {
			doA = YES
			class = 'A'
			printf ("%4d: N = %d, Rate = %5.1f += %.1f, RA = %6.1f \
			    += %.1f, DEC = %6.1f +- %.1f %.1f %s\n",
			    c, nclust, r, d, sumr, sumrr, sumd, sumdd, summ,
			    class)
			printf ("%d %s\n", c, class, >> w3)
		    } else if (d/max(1,r) < 0.5) {
			doB = YES
			class = 'B'
			printf ("%4d: N = %d, Rate = %5.1f += %.1f, RA = %6.1f \
			    += %.1f, DEC = %6.1f +- %.1f %.1f B\n",
			    c, nclust, r, d, sumr, sumrr, sumd, sumdd, summ)
			printf ("%d %s\n", c, class, >> w3)
		    }

		    fd2 = w2
		    while (fscan (fd2, rec) != EOF)
			printf ("%s %2d\n", rec, nclust, >> w4)
		    fd2 = ""
		} else {
		    printf ("%4d: N = %d, Rate = %5.1f += %.1f, RA = %6.1f \
			+= %.1f, DEC = %6.1f +- %.1f %.1f D\n",
			c, nclust, r, d, sumr, sumrr, sumd, sumdd, summ, >> w5)
		}
	    }

	    nclust = 1
	    print (clast, r1, d1, m1, t1, > w2)
	}
#	if (access(w5))
#	    concat (w5)
	fd1 = ""; delete (work//"[125].tmp")

	# Check for any data.
	tinfo (w4, ttout-)
	if (tinfo.nrows == 0) {
	    delete (work//"[1-6].tmp")
	    return
	}

	# Find duplicates.
	tmatch (w4, w4, w6, "RA,DEC", "RA,DEC", 00:00:02,
	    incol1="ID,N", incol2="ID,N", factor="", diagfile="",
	    nmcol1="", nmcol2="", sphere+)
	tinfo (w6, ttout-)
	if (tinfo.nrows > 0) {
	    tquery (w6, w5, "id_1>id_2", "", "", uniq+)
	    delete (w6)
	    fd1 = w5; clast = 0; nclust = 0
	    while (fscan (fd1, c, n, c1, i) != EOF) {
		if (nscan() == 0)
		    next
		if (i < n)
		    c = c1
		print (c, >> w6)
	    }
	    fd1 = ""; delete (w5)

	    if (access(w6)) {
		doA = NO; doB = NO; doD = NO
		fd1 = w3; fd2 = w6; clast = 0
		while (fscan (fd1, c, class) != EOF) {
		    if (c > clast) {
			if (fscan (fd2, clast) == EOF)
			    clast = 10000
		    }
		    if (c == clast) {
			doD = YES
			class = "D"
		    }
		    if (class == "A")
			doA = YES
		    if (class == "B")
			doB = YES
		    printf ("%d %s\n", c, class, >> w5)
		}
		fd1 = ""; fd2 = ""; delete (w6)
		rename (w5, w3)
	    }
	}

	# Find added asteroids.
	if (addast != "") {
	    # Make catalog of unmatched added positions.
	    tmatch (addast, w4, w5, "RA,DEC", "RA,DEC",
		00:00:02, incol1="ID", incol2="", factor="", diagfile=w6,
		nmcol1="COIMID,COCCD,RA,DEC,COMAG,CORATE,COPA,COGRPID,COEXPID,CONOBS,COTSEP",
		nmcol2="", sphere+)
	    fd1 = w6
	    while (fscan (fd1, rec) != EOF) {
	        if (strstr("not matched", rec) > 0)
		    break
	    }
	    oc = oc1 // "_E" // oc2
	    while (fscan (fd1, class, rec) == 2) {
	        if (!access(oc)) {
		    print ("#c COIMID ch*16 %-16s", >> oc)
		    print ("#c COCCD ch*16 %-16s", >> oc)
		    print ("#c RA d %13.2h hours", >> oc)
		    print ("#c DEC d %13.1h degrees", >> oc)
		    print ("#c COMAG d %.1f", >> oc)
		    print ("#c CORATE d %d arcsec/hr", >> oc)
		    print ("#c COPA d %d deg", >> oc)
		    print ("#c COGRPID i %d", >> oc)
		    print ("#c COEXPID i %2d", >> oc)
		    print ("#c CONOBS i %d", >> oc)
		    print ("#c COTSEP i %d sec", >> oc)
		}
		print (rec, >> oc)
	    }
	    fd1 = ""; delete (w6)

	    # Now do the matched detections.
	    tmatch (w4, addast, w5, "RA,DEC", "RA,DEC",
		00:00:02, incol1="ID", incol2="", factor="", diagfile="",
		nmcol1="ID,RA,DEC", nmcol2="", sphere+)
	    tinfo (w5, ttout-)
	    if (tinfo.nrows > 0) {
		fd1 = w5; clast = 0; nclust = 0
		while (fscan (fd1, c) != EOF) {
		    if (nscan() == 0)
			next
		    if ( c == clast)
			nclust += 1
		    else {
			if (nclust > 1)
			    print (clast, >> w6)
			nclust = 1
			clast = c
		    }
		}
		if (nclust > 1)
		    print (clast, >> w6)
		fd1 = ""; delete (w5)

		doA = NO; doB = NO; doC = NO
		fd1 = w3; fd2 = w6; clast = 0
		while (fscan (fd1, c, class) != EOF) {
		    if (c > clast) {
			if (fscan (fd2, clast) == EOF)
			    clast = 10000
		    }
		    if (c == clast && class != "D") {
			doC = YES
			class = "C"
		    }
		    if (class == "A")
			doA = YES
		    if (class == "B")
			doB = YES
		    printf ("%d %s\n", c, class, >> w5)
		}
		fd1 = ""; fd2 = ""; delete (w6)
		rename (w5, w3)
	    }
	    delete (w4)
	}

	# Output selected tracklets and classes.
	delete (oc1//"_[ABCD]"//oc2)
	fd1 = ic; clast = 10000
	while (fscan (fd1, rec) != EOF) {
	    if (fscan (rec, c) == 0) {
		if (doA) {
		    oc = oc1 // "_A" // oc2
		    print (rec, >> oc)
		}
		if (doB) {
		    oc = oc1 // "_B" // oc2
		    print (rec, >> oc)
		}
		if (doC) {
		    oc = oc1 // "_C" // oc2
		    print (rec, >> oc)
		}
		if (doD) {
		    oc = oc1 // "_D" // oc2
		    print (rec, >> oc)
		}
		if (clast != 0) {
		    if (access(w3)) {
			fd3 = w3; clast = 0
		    } else
		        clast = 10000
		}
		next
	    }

	    if (c > clast) {
	        if (fscan (fd3, clast, class) == EOF)
		    clast = 10000
	    }
	    if (c != clast)
	        next
	    oc = oc1 // "_" // class // oc2
	    print (rec, >> oc)
	}
	fd1 = ""; fd3 = ""

	# Clean up.
	delete (work//"[1-6].tmp")
end
