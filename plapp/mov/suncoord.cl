# SUNCOORD -- Compute the RA of the sun.
#
# From Astronomical Almanac.

procedure suncoord (mjd)

real	mjd		{prompt="Modified julian date"}
real	ra		{prompt="RA of sun (hr)"}
real	dec		{prompt="DEC of sun (deg)"}
bool	verbose = yes	{prompt="Print results"}

begin
	real	n, L, g, l, e, f, t

	n = mjd - 51545
	L = 280.460 + 0.9856474 * n
	g = 357.528 + 0.9856003 * n
	l = L + 1.915 * dsin (g) + 0.20 * dsin (2 * g)
	e = 23.439 - 0.0000004 * n
	f = 180. / PI
	t = dtan (e / 2) ** 2
	ra = mod (l - f * t * dsin(2*l) + (f/2) * t**2 * dsin(4*l), 360.)
	dec = dasin (dsin(e) * dsin (l))

	ra /= 15
	if (verbose)
	    printf ("%h %h\n", ra, dec)
end
