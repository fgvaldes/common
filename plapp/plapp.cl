#{ PLIRAF - Pipeline specific IRAF package

cl < "plapp$lib/zzsetenv.def"
package plapp, bin = plappbin$

# Define some Unix commands.
# Use the Bourne shell which is faster to start up in the pipeline environment.
task	$vi		= "$!vi $(*)"
task	$ed		= "$!vi $(*)"
task	$sed		= "$!sed $(*)"
task	$find		= "$!find $(*)"
task	$grep		= "$!grep $(*)"
task	$ls		= "$!ls $(*)"
task	$mv		= "$!mv $(*)"
task	$rm		= "$!rm $(*)"
task	$rmdir		= "$! rm -r $(*)"
task	$mkpdir		= "$!mkdir -p $(*)"
task	$ln		= "$!ln $(*)"
task	$rsync		= "$!rsync $(*)"
task	$scp		= "$!scp $(*)"
task	$fpack		= "$!fpack $(*)"
task	$funpack	= "$!funpack $(*)"

task	$mkgraphic	= "$!mkgraphic"
task	$convert	= "$!convert"

task	$pldb		= "$!pldb"

task	$mklocal	= "$!mklocal"

task	ckpropid	= "plappsrc$ckpropid.cl"
task	getcal		= "plappsrc$getcal.cl"
cache	getcal
unlearn	getcal
task	putcal		= "plappsrc$putcal.cl"
cache	putcal
unlearn	putcal
task	getbokwcs	= "plappsrc$getbokwcs.cl"
cache	getbokwcs
unlearn getbokwcs
task	$getwcsdb	= "$!getwcsdb"

task	getrefcats	= "plappsrc$getrefcats.cl"

task	photdpth	= "plappsrc$photdpth.cl"
cache	photdpth

task	mksky		= "plappsrc$mksky.cl"
task	streak		= "plappsrc$streak.cl"
task	scmb		= "plappsrc$scmb.cl"
task	select		= "plappsrc$select.cl"
task	sigclip		= "plappsrc$sigclip.cl"
task	montage		= "plappsrc$montage.cl"
cache	select, sigclip

task	skysep		= "plappsrc$skysep.cl"
task	skygrid		= "plappsrc$skygrid.cl"
task	skygroup	= "plappsrc$skygroup.cl"
cache	skysep

task	astmap,
	astmatch,
	lincor,
	mefmerge,
	mimsurfit,
	phtmatch,
	voverlay	= "plappsrc$x_plapp.e"

task	chwcs		= "plappsrc$chwcs.cl"

task	nsahdr		= "plappsrc$nsahdr.cl"
task	nsarwcs		= "plappsrc$nsarwcs.cl"

task	mklincor	= "plappsrc$mklincor.cl"

set	mov		= "plapp$mov/"
task	$mov.pkg	= "mov$mov.cl"

keep
