# ASTMATCH.H -- Catalog information required by the task.
# The mapping from actual catalogs to these quantities is handled by ACECAT.

define	ID_RA		0 # d hr %g
define	ID_DEC		2 # d deg %g
define	ID_MREF		4 # r mag %g
define	ID_WX		6 # d hr %g
define	ID_WY		8 # d deg %g
define	ID_X		10 # d pix %g
define	ID_Y		12 # d pix %g
define	ID_MAG		14 # r mag %g
define	ID_FWHM		15 # r pix %g
define  ID_WA           16 # r pix %g
define  ID_WB           17 # r pix %g
define	ID_FLAGS	18 # 8 "" ""
define	ID_PTR		22 # ii		/ Pointer/integer for internal use

define	ACM_RA		RECD($1,ID_RA)
define	ACM_DEC		RECD($1,ID_DEC)
define	ACM_MREF	RECR($1,ID_MREF)
define	ACM_WX		RECD($1,ID_WX)
define	ACM_WY		RECD($1,ID_WY)
define	ACM_X		RECD($1,ID_X)
define	ACM_Y		RECD($1,ID_Y)
define	ACM_MAG		RECR($1,ID_MAG)
define	ACM_FWHM	RECR($1,ID_FWHM)
define  ACM_WA          RECR($1,ID_WA)
define  ACM_WB          RECR($1,ID_WB)
define  ACM_FLAGS       RECT($1,ID_FLAGS)
define	ACM_PTR		RECI($1,ID_PTR)

define	ACM_BP		RECC($1,ID_FLAGS,4)
