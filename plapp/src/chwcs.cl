# CHWCS -- Change WCS.

procedure chwcs (input)

file	input			{prompt="Input image"}
file	output = ""		{prompt="Output image or velvect"}
string	wcs = "tpv"		{prompt="New WCS (tan, tnx, tpv)",
				    enum="tan|tnx|tpv"}
bool	verbose = yes		{prompt="Verbose output?"}
real	gridspacing = 200	{prompt="Approx. fitting grid spacing (pixels)\n"}

int	xxorder = 4             {prompt="Order of xi fit in x"}
int	xyorder = 4             {prompt="Order of xi fit in y"}
string	xxterms = "half"        {prompt="Xi fit cross terms type",
				    enum="full|half|none"}
int	yxorder = 4             {prompt="Order of eta fit in x"}
int	yyorder = 4             {prompt="Order of eta fit in y"}
string	yxterms = "half"        {prompt="Eta fit cross terms type",
				    enum="|full|half|none|"}
struct	*fd

begin
	file	im, in, out, grid, tmp1, tmp2
	int	nx, ny, i, j, m, n
	real	x, y, naxis1, naxis2, xstep, ystep
	string	tmp, ctype1, ctype2, lngref, latref, proj, key
	struct	line

	# Set temporary files.
	#tmp = mktemp ("tmp$iraf")
	tmp = mktemp ("chwcs")
	tmp1 = tmp // "a.tmp"
	tmp2 = tmp // "b.tmp"
	grid = tmp // "c.tmp"

	# Set parameters.
	in = input
	out = output
	if (out == "")
	    out = in

	# Set output.
	if (out == "-")
	    out = in
#	if (out != in && imaccess(out)) {
#	    printf ("WARNING: %s already exists - skipping\n", out)
#	    return
#	}

	if (gridspacing > 0) {
	    # Make grid of fitting coordinates.
	    hselect (in, "naxis1, naxis2, ctype1, ctype2", yes) |
		scan (naxis1, naxis2, ctype1, ctype2)
	    nx = nint ((naxis1 - 1.) / gridspacing) + 1
	    ny = nint ((naxis2 - 1.) / gridspacing) + 1
	    xstep = (naxis1 - 1.) / (nx - 1)
	    ystep = (naxis2 - 1.) / (ny - 1)
	    nx = 0; ny = 0
	    for (y = 1; y <= naxis2; y += ystep) {
		ny += 1
		nx = 0
		for (x = 1; x <= naxis1; x += xstep) {
		    nx += 1
		    printf ("%.1f\t%.1f\t%.1f\t%.1f\n", x, y, x, y, >> tmp1)
		}
	    }
	    if (substr(ctype1,1,2)=="RA") {
		lngref = "CRVAL1"
		latref = "CRVAL2"
		wcsctran (tmp1, grid, in, "logical", "world", col="3 4",
		    units="", formats="%.3H %.2h", min=9, verb-)
	    } else {
		lngref = "CRVAL2"
		latref = "CRVAL1"
		wcsctran (tmp1, grid, in, "logical", "world", col="4 3",
		    units="", formats="%.3H %.2h", min=9, verb-)
	    }
	    mscctran (tmp1, tmp2, in, "logical", "astro", col="1 2",
		units="", formats="", min=12, verb-)
	    delete (tmp1, v-)

	    # Fit new WCS and update header.
	    if (out != in) {
		imcopy (in, out, verb-)
		im = out
	    } else
		im = in

	    if (wcs == "tpv")
		proj = "tnx"
	    else
		proj = wcs
	    ccmap (grid, "dev$null", solutions="", images=im, results="",
		xcolumn=1, ycolumn=2, lngcolumn=3, latcolumn=4,
		xmin=1., xmax=naxis1, ymin=1., ymax=naxis2,
		lngunits="hours", latunits="degrees", insystem="j2000",
		refpoint="user", xref="CRPIX1", yref="CRPIX2",
		lngref=lngref, latref=latref, refsystem="INDEF",
		lngrefunits="degrees", latrefunits="degrees", projection=proj,
		fitgeometry="general", function="polynomial",
		xxorder=xxorder, xyorder=xyorder, xxterms=xxterms,
		yxorder=yxorder, yyorder=yyorder, yxterms=yxterms,
		maxiter=0, reject=3, update=yes, pixsystem="logical",
		verbose=no, interactive=no, > "dev$null")
	    delete (grid, ver-)

	    # Compute residuals from previous WCS.
	    mscctran (tmp2, grid, out, "logical", "astro", col="3 4",
		units="", formats="", min=12, verb-)
	    delete (tmp2, v-)

	    fd = grid
	    while (fscan (fd, x, y, xstep, ystep) != EOF) {
		x -= xstep
		y -= ystep
		x *= 1000
		y *= 1000
		print (x, >> tmp1)
		print (y, >> tmp2)
	    }
	    fd = ""; delete (grid, v-)

	    # Output information.
	    if (verbose) {
		printf ("%s (%s)\n  -> %dx%d(%s) %dx%d(%s)\n  -> %s (%s):\n",
		    substr(in, strldx("/",in)+1,999), substr(ctype1,6,99),
		    xxorder, xyorder, xxterms, yxorder, yyorder, yxterms,
		    substr(out, strldx("/",out)+1,999), strupr(wcs))
		tstat (tmp1, "c1", out="", low=INDEF, high=INDEF, rows="-")
		printf ("  X residuals:   %-9.1f %-9.1f %-9.1f %-9.1f %-9s\n",
		    tstat.mean, tstat.stddev, tstat.vmin, tstat.vmax, "marcsec")
		tstat (tmp2, "c1", out="", low=INDEF, high=INDEF, rows="-")
		printf ("  Y residuals:   %-9.1f %-9.1f %-9.1f %-9.1f %-9s\n",
		    tstat.mean, tstat.stddev, tstat.vmin, tstat.vmax, "marcsec")
	    }

	    if (im == "velvect") {
		imdelete (im, v-)
		rtextimage (tmp1, tmp1, dim=nx//","//ny, header-)
		rtextimage (tmp2, tmp2, dim=nx//","//ny, header-)
		velvect (tmp1, tmp2, title="TPV Fit")
		imdelete (tmp1//","//tmp2, v-)
		delete (tmp1//","//tmp2, v-)
		bye
	    }
	    delete (tmp1//","//tmp2, v-)
	} else {
	    if (out != in) {
		imcopy (in, out, verb-)
		im = out
	    } else
		im = in
	}

	hselect (im, "ctype1, ctype2", yes) | scan (ctype1, ctype2)
	if (wcs == "tpv" && strstr ("TNX",ctype1) > 0) {
	    flpr
	    if (verbose)
		printf ("Convert %s -> %s\n", im, wcs)
	    # Format TPV keywords from tnx.
	    hedit (im, "PV?_*", del+)
	    imhead (im, l+) | match ("^WAT1_") | sort |
	        fields ("STDIN", 2, > tmp2)
	    fd = tmp2
	    while (fscan (fd, line) != EOF)
		printf ("%s", substr(line,1,68), >> tmp1)
	    fd = ""; delete (tmp2)
	    printf ("\n", >> tmp1)
	    words (tmp1) | words | fields ("STDIN", 1, lines="13-", > tmp1)
	    fd = tmp1; m = 0; n = 0; i = 0
	    while (fscan (fd, x) != EOF) {
		j = m + n
		if (j == 1) {
		    if (m == 1) x += 1
		    i = 1 + n
		} else if (j == 2)
		    i = 4 + n
		else if (j == 3)
		    i = 7 + n
		else if (j == 4)
		    i = 12 + n
		else if (j == 5)
		    i = 17 + n
		else if (j == 6)
		    i = 24 + n
		else if (j == 7)
		    i = 31 + n
		#printf ("C%d%d = PV1_%d = %g\n", m, n, i, x)
		printf ("PV1_%d\n", i) | scan (key)
		hedit (im, key, x, add+)
		m += 1
		if (m == xxorder - n) {
		    m = 0; n += 1
		}
	    }
	    fd = ""; delete (tmp1)

	    imhead (im, l+) | match ("^WAT2_") | sort |
	        fields ("STDIN", 2, > tmp2)
	    fd = tmp2
	    while (fscan (fd, line) != EOF)
		printf ("%s", substr(line,1,68), >> tmp1)
	    fd = ""; delete (tmp2)
	    printf ("\n", >> tmp1)
	    words (tmp1) | words | fields ("STDIN", 1, lines="13-", > tmp1)
	    fd = tmp1; m = 0; n = 0; i = 0
	    while (fscan (fd, x) != EOF) {
		j = m + n
		if (j == 1) {
		    if (n == 1) x += 1
		    i = 1 + m
		} else if (j == 2)
		    i = 4 + m
		else if (j == 3)
		    i = 7 + m
		else if (j == 4)
		    i = 12 + m
		else if (j == 5)
		    i = 17 + m
		else if (j == 6)
		    i = 24 + m
		else if (j == 7)
		    i = 31 + m
		#printf ("C%d%d = PV2_%d = %g\n", m, n, i, x)
		printf ("PV2_%d\n", i) | scan (key)
		hedit (im, key, x, add+)
		m += 1
		if (m == xxorder - n) {
		    m = 0; n += 1
		}
	    }
	    fd = ""; delete (tmp1)

	    ctype1 = substr(ctype1, 1, 5) // "TPV"
	    ctype2 = substr(ctype2, 1, 5) // "TPV"
	    hedit (im, "CTYPE1", ctype1)
	    hedit (im, "CTYPE2", ctype2)
	    hedit (im, "WAT*", del+)
	    flpr
	}
end
