# GETBOKWCS -- Get a BOK WCS indexed by FOCUSVAL.

procedure getbokwcs (image)

file	image			{prompt="Image"}
file	wcsdb = ""		{prompt="WCS database"}
string	focusval = ""		{prompt="Image focus values"}
string	best = ""		{prompt="Best available focus"}
real	dmin = INDEF		{prompt="Distance"}
int	nfound = 0		{prompt="Number of WCS databases"}

struct	*fd

begin
	real	mjd, f1, f2, f3, a, b, c, d
	string	im, filter, val
	file	focvals
	struct	focval

	# Get parameters.
	im = image

	# Get the image values.
	filter = "g"; hselect (im, "FILTER", yes) | scan (filter)
	hselect (im, "MJD-OBS", yes) | scan (mjd)
	hselect (im, "$FOCUSVAL", yes) |
	    translit ("STDIN", "*", " ") | scan (focval)
	if (focval == "INDEF")
	    focval = "1.069 0.947 1.084"
	else
	    focval = substr (focval, 2, 999)

	getwcsdb ("-f", filter, mjd, focval) | scan (val)
	if (nscan() == 0)
	    getwcsdb ("-f", filter, "-d", 1000, mjd, focval) | scan (val)
	if (nscan() == 0)
	    getwcsdb ("-d", 1000, mjd, focval) | scan (val)

	if (nscan() != 1) {
	    nfound = 0
	    wcsdb = ""
	} else {
	    nfound = 1
	    wcsdb = val
	}
	focusval = focval
end
