# GETCAL -- Get calibration data from the calibration database.
#
# This is a simplified version of the old MOSAIC/NEWFIRM version
# with some semantic and syntactic differences.
#
# Parameters which are constraints must be strings like:
#   "=[value]", "like [value]", "between [value] and [value", etc.

procedure getcal (class)

string	class			{prompt="Class"}
bool	path = no		{prompt="Format path?"}
file	root = "NHPPS_PIPECAL$/" {prompt="Path root"}
string	image = ""		{prompt="Image"}
string	dir = ""		{prompt="Directory"}
string	detector = ""		{prompt="Detector"}
string	imageid = ""		{prompt="Image ID"}
string	filter = ""		{prompt="Filter"}
string	exptime = ""		{prompt="Exposure time"}
real	dexptime = 0.1		{prompt="Fractional Exposure time window"}
string	ncombine = ""		{prompt="NCOMBINE constraint"}
string	quality = ""		{prompt="Quality constraint"}
string	mjd = ""		{prompt="Desired MJD constraint"}
real	dmjd = INDEF		{prompt="MJD window"}
string	match = ""		{prompt="Match RE"}			
string	vmatch = ""		{prompt="Value RE"}
file	sql = ""		{prompt="File for sql query"}
file	results = ""		{prompt="File for results"}
bool	update = no		{prompt="Update image header?"}

int	nfound = 0		{prompt="Number found"}
struct	value			{prompt="Value"}
int	statcode		{prompt="Status code"}
string	statstr			{prompt="Status string"}

struct	*list

begin
	bool	b
	int	i
	file	im, s, r, tmp
	string	cls, str
	struct	det, imid, filt, exp, mj, nc, qual, subdir, mtch, vmtch

	# Set parameters.
	cls = class
	im = image
	det = detector
	imid = imageid
	filt = filter
	exp = exptime
	mj = mjd
	nc = ncombine
	qual = quality
	subdir = dir
	mtch = match
	vmtch = vmatch
	s = sql
	r = results

	tmp = mktemp ("getcal")
	if (s == "" || s == "STDOUT")
	    s = tmp // "_sql.tmp"
	if (r == "" || r == "STDOUT")
	    r = tmp // "_val.tmp"

	# Initialize output.
	nfound = 0
	value = ""
	statcode = 0
	statstr = "OK"
	if (access(s))
	    delete (s, verify-)
	if (access(r))
	    delete (r, verify-)

	# Get image keywords if needed.
	b = (stridx('!',det)>0  || stridx('!',imid)>0   ||
	     stridx('!',filt)>0 || stridx('!',exp)>0    ||
	     stridx('!',mj)>0   || stridx('!',nc)>0     ||
	     stridx('!',qual)>0 || stridx('!',subdir)>0 ||
	     stridx('!',mtch)>0)

	if (b && (im == "" || imaccess(im)==NO)) {
	    statcode = 1
	    statstr = "No image for keywords"
	}

	if (b && im != "") {
	    if (stridx('!',det) > 0  && statcode == 0) {
		files (det) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(det,2,9), yes) | scan(det)
		else {
		    files (det, > tmp//".tmp")
		    list = tmp//".tmp"; det = ""
		    while (fscan (list, str) != EOF) {
			if (det != "")
			    det += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			det += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',imid) > 0  && statcode == 0) {
		files (imid) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(imid,2,9), yes) | scan(imid)
		else {
		    files (imid, > tmp//".tmp")
		    list = tmp//".tmp"; imid = ""
		    while (fscan (list, str) != EOF) {
			if (imid != "")
			    imid += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			imid += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',filt) > 0  && statcode == 0) {
		files (filt) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(filt,2,9), yes) | scan(filt)
		else {
		    files (filt, > tmp//".tmp")
		    list = tmp//".tmp"; filt = ""
		    while (fscan (list, str) != EOF) {
			if (filt != "")
			    filt += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			filt += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',exp) > 0  && statcode == 0) {
		files (exp) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(exp,2,9), yes) | scan(exp)
		else {
		    files (exp, > tmp//".tmp")
		    list = tmp//".tmp"; exp = ""
		    while (fscan (list, str) != EOF) {
			if (exp != "")
			    exp += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			exp += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',mj) > 0  && statcode == 0) {
		files (mj) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(mj,2,9), yes) | scan(mj)
		else {
		    files (mj, > tmp//".tmp")
		    list = tmp//".tmp"; mj = ""
		    while (fscan (list, str) != EOF) {
			if (mj != "")
			    mj += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			mj += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',nc) > 0  && statcode == 0) {
		files (nc) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(nc,2,9), yes) | scan(nc)
		else {
		    files (nc, > tmp//".tmp")
		    list = tmp//".tmp"; nc = ""
		    while (fscan (list, str) != EOF) {
			if (nc != "")
			    nc += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			nc += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',qual) > 0  && statcode == 0) {
		files (qual) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(qual,2,9), yes) | scan(qual)
		else {
		    files (qual, > tmp//".tmp")
		    list = tmp//".tmp"; qual = ""
		    while (fscan (list, str) != EOF) {
			if (qual != "")
			    qual += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			qual += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',subdir) > 0  && statcode == 0) {
		files (subdir) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(subdir,2,9), yes) | scan(subdir)
		else {
		    files (subdir, > tmp//".tmp")
		    list = tmp//".tmp"; subdir = ""
		    while (fscan (list, str) != EOF) {
			if (subdir != "")
			    subdir += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			subdir += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',mtch) > 0  && statcode == 0) {
		files (mtch) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(mtch,2,9), yes) | scan(mtch)
		else {
		    files (mtch, > tmp//".tmp")
		    list = tmp//".tmp"; mtch = ""
		    while (fscan (list, str) != EOF) {
			if (mtch != "")
			    mtch += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			mtch += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	}

	# Check for parameter errors.
	if (statcode != 0)
	    return

	# Create SQL.
	if (path)
	    printf ("SELECT '%s'||dir||'/'||value", root, > s)
	else
	    printf ("SELECT value", cls, > s)
	printf (" FROM caldb\nWHERE class='%s'\n", cls, >> s)
	if (subdir != "")
	    printf ("AND dir='%s'\n", subdir, >> s)
	if (imid != "")
	    printf ("AND imageid='%s'\n", imid, >> s)
	if (filt != "")
	    printf ("AND filter='%s'\n", filt, >> s)
	if (det != "")
	    printf ("AND detector='%s'\n", det, >> s)
	if (exp != "")
	    printf ("AND abs(exptime-%s)<=%.2f\n", exp, dexp, >> s)
	if (nc != "")
	    printf ("AND ncombine %s\n", nc, >> s)
	if (quality != "")
	    printf ("AND quality %s\n", qual, >> s)
	if (mtch != "")
	    printf ("AND match ~ '%s'\n", mtch, >> s)
	if (vmtch != "")
	    printf ("AND value ~ '%s'\n", vmtch, >> s)

	# Add MJD constraints.
	if (mj != "") {
	    printf ("AND (%s-mjd) BETWEEN COALESCE(mjdwin1,-10000) \
		AND COALESCE (mjdwin2,10000)\n", mj, >> s)
	    if (!isindef(dmjd))
	        printf ("AND abs(%s-mjd)<%g\n", mj, dmjd, >> s)
	    printf ("ORDER BY abs(%s-mjd) ASC,quality \
	        DESC,ncombine DESC,added DESC\n", mj, >> s)
	} else
	    printf ("ORDER BY added DESC\n", mj, >> s)

	# Query database with output to standard out.
	pldb ("-Aqt", "-f", s, >> r)

	# Set the number found.
	count (r) | scan (nfound)
	head (r, nl=1) | scan (value)

	# Update header with first result.
	if (update && im != "" && nfound > 0) {
	    if (path)
		str = substr (value, strldx("/",value)+1, 999)
	    else
	        str = value
	    hedit (im, cls, str, add+, show-)
	}

	# Output result.
	if (sql == "STDOUT")
	    concat (s)
	if (results == "STDOUT")
	    concat (r)

	# Clean up.
	if (sql != s && access(s))
	    delete (s, verify-)
	if (results != r && access(r))
	    delete (r, verify-)
end
