
	# Get image keywords if needed.
	b = (stridx('!',det)>0  || stridx('!',imid)>0   ||
	     stridx('!',filt)>0 || stridx('!',exp)>0    ||
	     stridx('!',mj)>0   || stridx('!',nc)>0     ||
	     stridx('!',qual)>0 || stridx('!',subdir)>0 ||
	     stridx('!',mtch)>0)

	if (b && (im == "" || imaccess(im)==NO)) {
	    statcode = 1
	    statstr = "No image for keywords"
	}

	if (b && im != "") {
	    if (stridx('!',det) > 0  && statcode == 0) {
		files (det) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(det,2,9), yes) | scan(det)
		else {
		    files (det, > tmp//".tmp")
		    list = tmp//".tmp"; det = ""
		    while (fscan (list, str) != EOF) {
			if (det != "")
			    det += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			det += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',imid) > 0  && statcode == 0) {
		files (imid) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(imid,2,9), yes) | scan(imid)
		else {
		    files (imid, > tmp//".tmp")
		    list = tmp//".tmp"; imid = ""
		    while (fscan (list, str) != EOF) {
			if (imid != "")
			    imid += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			imid += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',filt) > 0  && statcode == 0) {
		files (filt) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(filt,2,9), yes) | scan(filt)
		else {
		    files (filt, > tmp//".tmp")
		    list = tmp//".tmp"; filt = ""
		    while (fscan (list, str) != EOF) {
			if (filt != "")
			    filt += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			filt += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',exp) > 0  && statcode == 0) {
		files (exp) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(exp,2,9), yes) | scan(exp)
		else {
		    files (exp, > tmp//".tmp")
		    list = tmp//".tmp"; exp = ""
		    while (fscan (list, str) != EOF) {
			if (exp != "")
			    exp += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			exp += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',mj) > 0  && statcode == 0) {
		files (mj) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(mj,2,9), yes) | scan(mj)
		else {
		    files (mj, > tmp//".tmp")
		    list = tmp//".tmp"; mj = ""
		    while (fscan (list, str) != EOF) {
			if (mj != "")
			    mj += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			mj += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',nc) > 0  && statcode == 0) {
		files (nc) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(nc,2,9), yes) | scan(nc)
		else {
		    files (nc, > tmp//".tmp")
		    list = tmp//".tmp"; nc = ""
		    while (fscan (list, str) != EOF) {
			if (nc != "")
			    nc += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			nc += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',qual) > 0  && statcode == 0) {
		files (qual) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(qual,2,9), yes) | scan(qual)
		else {
		    files (qual, > tmp//".tmp")
		    list = tmp//".tmp"; qual = ""
		    while (fscan (list, str) != EOF) {
			if (qual != "")
			    qual += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			qual += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',subdir) > 0  && statcode == 0) {
		files (subdir) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(subdir,2,9), yes) | scan(subdir)
		else {
		    files (subdir, > tmp//".tmp")
		    list = tmp//".tmp"; subdir = ""
		    while (fscan (list, str) != EOF) {
			if (subdir != "")
			    subdir += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			subdir += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',mtch) > 0  && statcode == 0) {
		files (mtch) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(mtch,2,9), yes) | scan(mtch)
		else {
		    files (mtch, > tmp//".tmp")
		    list = tmp//".tmp"; mtch = ""
		    while (fscan (list, str) != EOF) {
			if (mtch != "")
			    mtch += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			mtch += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	}
