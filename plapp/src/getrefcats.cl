# GETREFCATS -- Get reference catalogs.
#
# The input can either be a file of catalog filenames or a coordinate;
#
#     e.g. "5:15:10,32:20:12" or "5:15:10 32:20:10"
#
# For input catalogs the minimum and maximum values of the ra and dec
# columns is determined and an optional buffer added to to define the
# desired rectangular region.  For coordinate input the buffer defines the
# desired rectangular region.  All tile reference catalogs which overlap the
# region are returned.  The catalogs contain the columns RA, DEC, and MAG.
# The coordinates are in degrees.
#
# Currently available reference catalogs are:
# 
#     ub   = USNO-B1      (g, r, i)
#     ps1  = Pan-STARRS1  (g, r, i, z, Y, V) (g, r, z with Gaia astrometry)
#     tm   = 2MASS        (j, h, k)
#     gaia = GAIA         (G)

procedure getrefcats (region, refcat, reffilt)

file	region			{prompt="Coordinate or file of catalog names"}
string	refcat			{prompt="Reference catalog"}
string	reffilt			{prompt="Reference filter"}

real	buffer = 0		{prompt="Coordinate buffer (deg)"}
string	racol = "ra"		{prompt="RA column in input catalogs"}
string	deccol = "dec"		{prompt="DEC column in input catalogs"}
string	raunits = "hr"		{prompt="RA units (hr|deg)"}
file	dir = "/shared/catalogs/" {prompt="Reference catalogs root directory"}
bool	verbose = no		{prompt="Verbose?"}

struct  *list

begin

	file	in, ref, f, d, cat
	real	ra, dec, vmin, vmax, ramin, ramax, decmin, decmax

	# Set query parameters.
	in = region
	ref = refcat
	f = reffilt

	if (verbose) {
	    printf ("  region = %s\n", region)
	    printf ("  refcat = %s\n", refcat)
	    printf ("  reffilt = %s\n", reffilt)
	    printf ("  buffer = %g\n", buffer)
	    printf ("  racol = %s\n", racol)
	    printf ("  deccol = %s\n", deccol)
	    printf ("  raunits = %s\n", raunits)
	    printf ("  dir = %s\n", dir)
	}

	# Initialize.
	printf ("%s%s/%s/\n", dir, ref, f) | scan (d)
	if (!access(d))
	    error (1, "Unknown reference catalog ("//d//")")
	d += ref

	# Determine input region.
	ramin = INDEF; decmin = INDEF
	if (access(in)) {
	    # Minimum and maximum of all input catalog sources.
	    list = in
	    while (fscan (list, cat) != EOF) {
		tinfo (cat, ttout-)
		if (tinfo.nrows == 0)
		    next
		;
		tstat (cat, racol, outtable="")
		if (raunits == "hr") {
		    vmin = 15 * tstat.vmin; vmax = 15 * tstat.vmax
		} else {
		    vmin = tstat.vmin; vmax = tstat.vmax
		}
		if (verbose)
		    printf ("  %s %g %g\n", substr(cat, strldx("/",cat)+1,999),
		        vmin, vmax)
		if (isindef(ramin)) {
		    ramin = vmin
		    ramax = vmax
		} else {
		    ramin = min (ramin, vmin)
		    ramax = max (ramax, vmax)
		}
		if (verbose)
		    printf ("  min/max: %g %g\n", ramin, ramax)
		tstat (cat, deccol, outtable="")
		if (isindef(decmin)) {
		    decmin = tstat.vmin
		    decmax = tstat.vmax
		} else {
		    decmin = min (decmin, tstat.vmin)
		    decmax = max (decmax, tstat.vmax)
		}
	    }
	    list = ""
	} else {
	    # Check for a coordinate.
	    print (in) | translit ("STDIN", ",", " ") | scan (ra, dec)
	    if (nscan() == 2) {
		ramin = ra; ramax = ra; decmin = dec; decmax = dec
		if (raunits == "hr") {
		    ramin *= 15; ramax *= 15
		}
	    }
	}

	if (isindef(ramin))
	    error (1, "No input sources ("//in//")")

	# Check for equinox boundary.
	if (ramax - ramin > 180) {
	    ramin = INDEF
	    # Minimum and maximum of all input catalog sources.
	    list = in
	    while (fscan (list, cat) != EOF) {
		tinfo (cat, ttout-)
		if (tinfo.nrows == 0)
		    next
		;
		tstat (cat, racol, outtable="")
		if (raunits == "hr") {
		    vmin = 15 * tstat.vmax; vmax = 15 * tstat.vmin
		} else {
		    vmin = tstat.vmax; vmax = tstat.vmin
		}
		if (vmin > 180)
		    vmin -= 360
		if (vmax > 180)
		    vmax -= 360
		if (verbose)
		    printf ("  %s %g %g\n",
			substr(cat, strldx("/",cat)+1,999), vmin, vmax)
		if (isindef(ramin)) {
		    ramin = vmin
		    ramax = vmax
		} else {
		    ramin = min (ramin, vmin)
		    ramax = max (ramax, vmax)
		}
		if (verbose)
		    printf ("  min/max: %g %g\n", ramin, ramax)
	    }
	    list = ""
	}

	if (verbose)
	    printf ("  %g %g %g %g\n", ramin, ramax, decmin, decmax)

	# Add a buffer.
	decmin = max (-90, decmin - buffer)
	decmax = min (90, decmax + buffer)
	dec = (decmin + decmax) / 2
	ramin = ramin - buffer / dcos(dec)
	ramax = ramax + buffer / dcos(dec)
	if (ramin < 0.)
	    ramin += 360.
	if (ramax > 360.)
	    ramax -= 360.

	if (verbose)
	    printf ("  %g %g %g %g\n", ramin, ramax, decmin, decmax)

	# Create output list.
	#if (ramax - ramin > 5) {
	if (ramax < ramin) {
	    for (j=nint(decmin+0.1); j<=nint(decmax-0.1); j+=1) {
		for (i=nint(ramin+0.1); i<=359; i+=1) {
		    if (j >= 0)
			printf ("%s_%s+%02d_%03d.fits\n", d, f, j, i) |
			    scan (cat)
		    else if (j == -1) {
			if (ref == "ps1" && stridx("g,r,z",f) > 0)
			    printf ("%s_%s+%02d_%03d.fits\n", d, f, 0, i) |
				scan (cat)
			else
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j, i) |
				scan (cat)
		    } else {
			if (ref == "ps1" && stridx("grz",f) > 0)
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j-1, i) |
				scan (cat)
			else
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j, i) |
				scan (cat)
		    }
		    if (verbose || access (cat))
			print (cat)
		}
		for (i=0; i<=nint(ramax-0.1); i+=1) {
		    if (j >= 0)
			printf ("%s_%s+%02d_%03d.fits\n", d, f, j, i) |
			    scan (cat)
		    else if (j == -1) {
			if (ref == "ps1" && stridx("grz",f) > 0)
			    printf ("%s_%s+%02d_%03d.fits\n", d, f, 0, i) |
				scan (cat)
			else
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j, i) |
				scan (cat)
		    } else {
			if (ref == "ps1" && stridx("grz",f) > 0)
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j-1, i) |
				scan (cat)
			else
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j, i) |
				scan (cat)
		    }
		    if (verbose || access (cat))
			print (cat)
		}
	    }
	} else {
	    for (j=nint(decmin+0.1); j<=nint(decmax-0.1); j+=1) {
		for (i=nint(ramin+0.1); i<=nint(ramax-0.1); i+=1) {
		    if (j >= 0)
			printf ("%s_%s+%02d_%03d.fits\n", d, f, j, i)|
			    scan (cat)
		    else if (j == -1) {
			if (ref == "ps1" && stridx("grz",f) > 0)
			    printf ("%s_%s+%02d_%03d.fits\n", d, f, 0, i) |
				scan (cat)
			else
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j, i) |
				scan (cat)
		    } else {
			if (ref == "ps1" && stridx("grz",f) > 0)
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j-1, i) |
				scan (cat)
			else
			    printf ("%s_%s-%02d_%03d.fits\n", d, f, -j, i) |
				scan (cat)
		    }
		    if (verbose || access (cat))
			print (cat)
		}
	    }
	}
end
