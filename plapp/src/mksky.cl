# MKSKY -- Make exposure ACE sky images (maybe sky or sigma).

procedure mksky (input, output)

file	input			{prompt="File of sky images"}
file	output			{prompt="Output sky image"}
real	blank = 0.		{prompt="Blank value"}
bool	verbose = no		{prompt="Verbose?"}

begin
	file	in, out, im
	int	i, j, k, i1, i2, j1, j2, ni, nj, nc, nl
	string	sec

	in = input
	out = output

	# This is not total general.  Recognize 4x2 and 2x2 mosaics.
	count (in) | scan (i)
	if (i == 8) {
	    ni = 4; nj = 2
	} else if (i == 4) {
	    ni = 2; nj = 2
	} else {
	    sec = ""; hselect ("@"//input, "INSTRUME", yes) | scan (sec)
	    if (sec == "Mosaic3") {
	        ni = 2; nj = 2
	    } else
		error (1, "Unexpected number of sky images")
	}

	k = 0
	for (j=1; j<=nj; j+=1) {
	    for (i=1; i<=ni; i+=1) {
	        k += 1
		im = ""; match ("-"//k//"_", in) | scan (im)
		if (im == "")
		    next
		if (!imaccess(out)) {
		    mkglbhdr ("@"//in, "mkskyhdr", ref="", exclude="")
		    hselect (im, "NAXIS1,NAXIS2", yes) | scan (nc, nl)
		    i2 = nc * ni; j2 = nl * nj
		    mkpattern (out, pattern="constant", option="replace",
			v1=blank, size=1, pixtype="real", ndim=2,
			ncols=i2, nlines=j2, header="mkskyhdr")
		    imdelete ("mkskyhdr")
		}
		i1 = (i - 1) * nc + 1; i2 = i * nc
		j1 = (j - 1) * nl + 1; j2 = j * nl
		printf ("%s[%d:%d,%d:%d]\n", out, i1, i2, j1, j2) | scan (sec)
		imcopy (im, sec, verbose=verbose) 
	    }
	}
end
