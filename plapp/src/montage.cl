# FTRMONTAGE -- Make a montage figure for FTRSKY.

#global	figno

procedure ftrmontage (input, output, title)

string	input			{prompt="Input images"}
file	output			{prompt="Output rootname"}
string	title = ""		{prompt="Title"}

file	html = "index.html"	{prompt="Output HTML"}
file	olist = ""		{prompt="Output list to append png names"}
string	work = ""		{prompt="Work rootname"}

int	ncol = 10		{prompt="Number of columns in montage"}
int	nstep = 45		{prompt="Cell size (pix)"}
int	ltrim = 0		{prompt="Left trim for grid match"}

struct	*fd

begin
	int	n
	string	fign
	file	in, out, png, grid, offsets, stats, scales, template
	real	val, minval
	struct	str

	in = input
	out = output

	figno += 1
	fign = out // figno
	png = out // figno // ".png"
	if (work != "") {
	    grid = work // "_grid.tmp"
	    offsets = work // "_offsets.tmp"
	    stats = work // "_stats.tmp"
	    scales = work // "_scls.tmp"
	    template = work // "_template_tmp"
	} else {
	    grid = nhppsargs.module // "_grid.tmp"
	    offsets = nhppsargs.module // "_offsets.tmp"
	    stats = nhppsargs.module // "_stats.tmp"
	    scales = nhppsargs.module // "_scls.tmp"
	    template = nhppsargs.module // "_template_tmp"
	}

	# Set the stats.
	imstatistics ("@"//in, fields="mode", format-, lower=-999, > stats)
	fd = stats; minval = INDEF
	while (fscan (fd, val) != EOF) {
	    val = 1 / max (val, 1e-8)
	    if (isindef(minval))
	        minval = val
	    else
	        minval = min (minval, val)
	    print (val, >> scales)
	}
	fd = ""; delete (stats)

	# Make the gridded image.  Create the grid and template if needed.
	flpr
	if (access(grid)==NO) {
	    printf ("grid %d %d 1000 %d\n", ncol, nstep, nstep) | scan (str)
	    imcombine ("@"//in, fign, headers="", bpmasks="",
		rejmasks="", nrejmasks="", expmasks="", sigmas="",
		logfile=grid, combine="sum", reject="none", project=no,
		outtype="real", outlimits="", offsets=str,
		masktype="none", maskvalue=0, blank=0., scale="@"//scales,
		zero="none", weight="none", statsec="", expname="",
		lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1,
		nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise=0.,
		gain=1., snoise=0., sigscale=0.1, pclip=-0.5, grow=0.)
	    imexpr ("a!=0?0 : a", template, fign, verbose-)
	} else {
	    fd = in
	    while (fscan (fd, str) != EOF) {
		str = substr (str, ltrim+1, 999)
		match (str, grid) | fields ("STDIN", "3,4", >> offsets)
	    }
	    fd = ""
	    count (offsets) | scan (n, n)
	    if (n > 0)
		print ("0 0", >> offsets)
	    else
	        offsets = ""
	    print (minval, >> scales)
	    imcombine ("@"//in//","//template, fign, headers="", bpmasks="",
		rejmasks="", nrejmasks="", expmasks="", sigmas="",
		logfile="", combine="sum", reject="none", project=no,
		outtype="real", outlimits="", offsets=offsets,
		masktype="none", maskvalue="0", blank=0., scale="@"//scales,
		zero="none", weight="none", statsec="", expname="",
		lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1,
		nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise="0.",
		gain="1.", snoise="0.", sigscale=0.1, pclip=-0.5, grow=0.)
	    delete (offsets)
	}
	delete (scales)
	flpr

	# Make the graphic.
	touch (dataset.fprotect);
	mkgraphic (fign, fign, "world", "png", 1, "'i1>0'")
	imdelete (fign)

	# Add figure to HTML output.
	printf ('<P>Figure %d: %s\n', figno, title, >> html)
	printf ('<P><IMG SRC="%s">\n', fign//".png", >> html)

	# Add PNG to output list.
	if (olist == "")
	    print (fign//".png", >> dataset.fdelete)
	else
	    pathname (fign//".png", >> olist)
end
