# NSAHDR -- Set header for data product.

procedure nsahdr (image)

file	image			{prompt="Image header to edit"}
string	extname	= ""		{prompt="Extension name"}
string	dsname= ""		{prompt="Dataset short name"}
string	obstype= ""		{prompt="Observation type"}
string	proctype= ""		{prompt="Processing type"}
string	prodtype= ""		{prompt="Product type"}

begin
	int	i, j, k
	real	x1, x2
	file	im, tmp
	string  plfname, pldname, plqueue, plqname, plprocid, subset
	string	s1, s2, s3
	struct	plver, line

	if (nhppsargs.module == "None")
	    tmp = "nsahdr.tmp"
	else
	    tmp = nhppsargs.module // "_nsahdr.tmp"

	im = image

	# Set the PL keywords.
	match ("PLVER:", "mosaic$/Versions", > tmp)
	tail (tmp, nl=1) | scan (s1, plver)
	print (dsname) | translit ("STDIN", "-", " ") |
	    scan (pldname, subset)
	print (pldname) | translit ("STDIN", "_", " ") |
	    scan (plqueue, plqname, plprocid)
	plfname = im
	i = strldx ("/", im)
	if (i > 0)
	    plfname = substr (im, i+1, 999)

	nhedit (im, "OBSTYPE", obstype, "Observation type", add+)
	nhedit (im, "PROCTYPE", proctype, "Processing type", add+)
	nhedit (im, "PRODTYPE", prodtype, "Product type", add+)
	nhedit (im, "PLVER", plver, "Pipeline version", add+)
	nhedit (im, "PLDNAME", "dummy", "Pipeline dataset name", add+)
	nhedit (im, "PLQUEUE", "dummy", "Pipeline queue", add+)
	nhedit (im, "PLQNAME", "dummy", "Pipeline queue name", add+)
	nhedit (im, "PLPROCID", "dummy", "Pipeline processing ID", add+)
	#nhedit (im, "PLOFNAME", "(FILENAME)", "Original filename", add+)
	nhedit (im, "FILENAME", plfname, "Filename", add+)
	nhedit (im, "PLFNAME", plfname, "Pipeline filename", add+)
	nhedit (im, "PLDNAME", pldname, "Pipeline dataset name", add+)
	nhedit (im, "PLQUEUE", plqueue, "Pipeline queue", add+)
	nhedit (im, "PLQNAME", plqname, "Pipeline queue name", add+)
	nhedit (im, "PLPROCID", plprocid, "Pipeline processing ID", add+)
			
	if (obstype != "zero" && obstype != "dark") {

	    # Set filter parameters.  The "lamrange" keyword contains the
	    # central wavelength, the FWHM, and the source of these numbers,
	    # where kpno means that the numbers have been obtained from
	    # http://www.noao.edu/kpno/mosaic/filters/ ctio means they come
	    # from http://www.ctio.noao.edu/instruments/FILTERS/index.html
	    # and pipeline means the FWHM has been determined by
	    # the pipeline team from the transmission curves on
	    # http://www.noao.edu/kpno/mosaic/filters/

	    hselect (im, "FILTER", yes) | scan (line)
	    s1 = substr (line, strlen(line)-4, 999)
	    nhedit (im, "FILTID", s1, "Unique filter identification",
		after="FILTER", add+)

	    getcal ("filtwave", image=im, filter="!FILTER")
	    if (getcal.nfound == 0)
		printf ("Warning: Failed to set filter parameters (%s)\n", s1)
	    else {
	        i = fscan (getcal.value, x1, x2)
		x1 = x1 / 10.; x2 = x2 / 10.
		nhedit (im, "PHOTCLAM", x1, "[nm] Filter wavelength width",
		    after="FILTID", add+)
		nhedit (im, "PHOTFWHM", x2, "[nm] Filter wavelength FWHM",
		    after="FILTID", add+)
		nhedit (im, "PHOTBW", x2, "[nm] Filter wavelength FWHM",
		    after="FILTID", add+)
	    }
	}
	
#	# Set associated calibration files
#	#
#	# This is not as general as it should be since it doesn't make
#	# use of the names routines.  The assumption is that after
#	# the last hyphen their will be an underscore component.
#	# It does work for both InstCal and Resampled.  In the latter
#	# case the parent MEF name is set for information purposes.
#
#	s3 = extname
#	if (s3 != "")
#	    s3 = "[" // s3 // "]"
#	    
#        hselect( im, "BPM", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 ) // "_bpm" // s3
#	    nhedit (im, "BPM", "DQMASK", rename+)
#	    nhedit (im, "DQMASK", s2, "Data quality file")
#	}
#        hselect( im, "ZERO", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 ) // s3
#	    nhedit (im, "ZERO", "BIASFIL", rename+)
#	    nhedit (im, "BIASFIL", s2, "Bias reference file")
#	}
#        hselect( im, "DARK", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 ) // s3
#	    nhedit (im, "DARK", "DARKFIL", rename+)
#	    nhedit (im, "DARKFIL", s2, "Dark reference file")
#	}
#        hselect( im, "FLAT", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 ) // s3
#	    nhedit (im, "FLAT", "FLATFIL", rename+)
#	    nhedit (im, "FLATFIL", s2, "Dome flat reference file")
#	}
#        hselect( im, "SFLAT", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 )
#	    if (j > i) {
#	        if (k > j)
#		    s2 = s2 // "-" // substr (s1, j+1, k-1)
#		else
#		    s2 = s2 // "-" // substr (s1, j+1, 1000)
#	    }
#	    s2 = s2 // s3
#	    nhedit (im, "SFLAT", "SFLATFIL", rename+)
#	    nhedit (im, "SFLATFIL", s2, "Sky flat reference file")
#	}
#        hselect( im, "FRINGE", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 )
#	    if (j > i) {
#	        if (k > j)
#		    s2 = s2 // "-" // substr (s1, j+1, k-1)
#		else
#		    s2 = s2 // "-" // substr (s1, j+1, 1000)
#	    }
#	    s2 = s2 // s3
#	    nhedit (im, "FRINGE", "FRNGFIL", rename+)
#	    nhedit (im, "FRNGFIL", s2, "Fringe template reference file")
#	}
#        hselect( im, "PUPGHOST", yes ) | scan( s1 )
#	if (nscan()==1) {
#	    i = strldx ("-", s1)
#	    j = strldx ("_", s1)
#	    k = strldx (".", s1)
#	    s2 = substr( s1, 1, i-1 )
#	    if (j > i) {
#	        if (k > j)
#		    s2 = s2 // "-" // substr (s1, j+1, k-1)
#		else
#		    s2 = s2 // "-" // substr (s1, j+1, 1000)
#	    }
#	    s2 = s2 // s3
#	    nhedit (im, "PUPGHOST", "PUPLFIL", rename+)
#	    nhedit (im, "PUPLFIL", s2, "Pupil template reference file")
#	}

	delete (tmp)
end
