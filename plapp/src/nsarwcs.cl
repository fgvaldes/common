# NSARWCS -- Set header for WCS keywords.

procedure nsarwcs (image)

string	image			{prompt="Image header to edit"}

begin
	int	i1, i2
	real	xc, yc, x1, x2, x3, x4, y1, y2, y3, y4
	string  s1

	# Compute the celestial coordinates of the center and corners.
	hselect (image, "NAXIS1,NAXIS2", yes) | scan (i1, i2)
	xc = (i1 + 1) / 2.; yc = (i2 + 1) / 2.
	printf("%g %g\n", xc, yc) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (xc, yc)
	printf("%.2H\n", xc) | scan (s1)
	printf("%.1h\n", yc) | scan (s2)
	printf("%d %d\n", 1, 1) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x1, y1)
	printf("%d %d\n", i1, 1) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x2, y2)
	printf("%d %d\n", i1, i2) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x3, y3)
	printf("%d %d\n", 1, i2) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x4, y4)

	# Update the header.
	hedit (image, "RA",       s1)
	hedit (image, "DEC",      s2)
	hedit (image, "CENTRA",   xc)
	hedit (image, "CENTDEC",  yc)
	hedit (image, "CORN1RA",  x1)
	hedit (image, "CORN2RA",  x2)
	hedit (image, "CORN3RA",  x3)
	hedit (image, "CORN4RA",  x4)
	hedit (image, "CORN1DEC", y1)
	hedit (image, "CORN2DEC", y2)
	hedit (image, "CORN3DEC", y3)
	hedit (image, "CORN4DEC", y4)
end
