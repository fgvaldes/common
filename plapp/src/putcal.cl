# PUTCAL -- Put calibration images into the calibration database.
#
# Note this copies the files.

procedure putcal (class, image)

string	class			{prompt="Class"}
string	image = ""		{prompt="Image"}
string	dir = ""		{prompt="Directory"}
string	detector = ""		{prompt="Detector"}
string	imageid = ""		{prompt="Image ID"}
string	filter = ""		{prompt="Filter"}
string	exptime = ""		{prompt="Exposure time"}
string	ncombine = ""		{prompt="NCOMBINE constraint"}
string	quality = ""		{prompt="Quality constraint"}
string	mjd = ""		{prompt="Desired MJD"}
string	mjdwin1 = ""		{prompt="MJD window (relative to MJD)"}
string	mjdwin2 = ""		{prompt="MJD window (relative to MJD)"}
string	match = ""		{prompt="Match constraint"}			
string	action = "copy"		{prompt="copy|move|none"}
file	sql = ""		{prompt="File for sql query"}
file	results = ""		{prompt="File for results of the query"}

int	statcode		{prompt="Status code"}
string	statstr			{prompt="Status string"}

struct	*list

begin
	bool	b
	int	i
	file	im, val, s, r, tmp
	string	cls, caldir, str
	struct	det, imid, filt, exp, mj, nc, qual, subdir, mtch

	# Set parameters.
	cls = class
	im = image
	det = detector
	imid = imageid
	filt = filter
	exp = exptime
	mj = mjd
	nc = ncombine
	qual = quality
	subdir = dir
	mtch = match
	s = sql
	r = results

	val = substr (im, strldx("/",im)+1, 999)
	tmp = mktemp ("putcal")
	if (s == "")
	    s = tmp // "_sql.tmp"
	if (r == "" || r == "STDOUT")
	    r = tmp // "_val.tmp"

	# Initialize output.
	statcode = 0
	statstr = "OK"
	if (access(s))
	    delete (s, verify-)
	if (access(r))
	    delete (r, verify-)

	# Get image keywords if needed.
	b = (stridx('!',det)>0  || stridx('!',imid)>0   ||
	     stridx('!',filt)>0 || stridx('!',exp)>0    ||
	     stridx('!',mj)>0   || stridx('!',nc)>0     ||
	     stridx('!',qual)>0 || stridx('!',subdir)>0 ||
	     stridx('!',mtch)>0)

	if (b && (im == "" || access(im)==NO)) {
	    statcode = 1
	    statstr = "No file for keywords"
	}

	if (b && im != "") {
	    if (stridx('!',det) > 0  && statcode == 0) {
		files (det) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(det,2,9), yes) | scan(det)
		else {
		    files (det, > tmp//".tmp")
		    list = tmp//".tmp"; det = ""
		    while (fscan (list, str) != EOF) {
			if (det != "")
			    det += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			det += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',imid) > 0  && statcode == 0) {
		files (imid) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(imid,2,9), yes) | scan(imid)
		else {
		    files (imid, > tmp//".tmp")
		    list = tmp//".tmp"; imid = ""
		    while (fscan (list, str) != EOF) {
			if (imid != "")
			    imid += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			imid += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',filt) > 0  && statcode == 0) {
		files (filt) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(filt,2,9), yes) | scan(filt)
		else {
		    files (filt, > tmp//".tmp")
		    list = tmp//".tmp"; filt = ""
		    while (fscan (list, str) != EOF) {
			if (filt != "")
			    filt += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			filt += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',exp) > 0  && statcode == 0) {
		files (exp) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(exp,2,9), yes) | scan(exp)
		else {
		    files (exp, > tmp//".tmp")
		    list = tmp//".tmp"; exp = ""
		    while (fscan (list, str) != EOF) {
			if (exp != "")
			    exp += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			exp += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',mj) > 0  && statcode == 0) {
		files (mj) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(mj,2,9), yes) | scan(mj)
		else {
		    files (mj, > tmp//".tmp")
		    list = tmp//".tmp"; mj = ""
		    while (fscan (list, str) != EOF) {
			if (mj != "")
			    mj += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			mj += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',nc) > 0  && statcode == 0) {
		files (nc) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(nc,2,9), yes) | scan(nc)
		else {
		    files (nc, > tmp//".tmp")
		    list = tmp//".tmp"; nc = ""
		    while (fscan (list, str) != EOF) {
			if (nc != "")
			    nc += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			nc += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',qual) > 0  && statcode == 0) {
		files (qual) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(qual,2,9), yes) | scan(qual)
		else {
		    files (qual, > tmp//".tmp")
		    list = tmp//".tmp"; qual = ""
		    while (fscan (list, str) != EOF) {
			if (qual != "")
			    qual += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			qual += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',subdir) > 0  && statcode == 0) {
		files (subdir) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(subdir,2,9), yes) | scan(subdir)
		else {
		    files (subdir, > tmp//".tmp")
		    list = tmp//".tmp"; subdir = ""
		    while (fscan (list, str) != EOF) {
			if (subdir != "")
			    subdir += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			subdir += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	    if (stridx('!',mtch) > 0  && statcode == 0) {
		files (mtch) | count | scan (i)
		if (i == 1)
		    hselect (im, "$"//substr(mtch,2,9), yes) | scan(mtch)
		else {
		    files (mtch, > tmp//".tmp")
		    list = tmp//".tmp"; mtch = ""
		    while (fscan (list, str) != EOF) {
			if (mtch != "")
			    mtch += ','
		        if (substr(str,1,1) == '!')
			    hselect (im, "$"//substr(str,2,9), yes) | scan(str)
			mtch += str
		    }
		    list = ""; delete (tmp//".tmp")
		}
	    }
	}

	# Check for parameter errors.
	if (statcode != 0)
	    return

	# Put the image in the calibration directory.
	if (access(im)) {
	    if (subdir == "")
	        subdir = cls
	    else if (subdir == "\tINDEF")
	        subdir = cls
	    else
	        subdir = cls // "/" // subdir
	    printf ("NHPPS_PIPECAL$/%s\n", subdir) | scan (caldir)
	    mkpdir (caldir)
	    if (action == "copy")
		copy (im, caldir)
	    else if (action == "move")
		move (im, caldir)
	}

	# Create SQL.
	printf ("INSERT INTO caldb (", cls, > s)
	printf ("CLASS,VALUE,DATATYPE", >> s)
	if (subdir != "")
	    printf (",DIR", >> s)
	if (mj != "")
	    printf (",MJD", >> s)
	if (mjdwin1 != "")
	    printf (",MJDWIN1", >> s)
	if (mjdwin2 != "")
	    printf (",MJDWIN2", >> s)
	if (det != "")
	    printf (",DETECTOR", >> s)
	if (imid != "")
	    printf (",IMAGEID", >> s)
	if (filt != "")
	    printf (",FILTER", >> s)
	if (exp != "")
	    printf (",EXPTIME", >> s)
	if (nc != "")
	    printf (",NCOMBINE", >> s)
	if (qual != "")
	    printf (",QUALITY", >> s)
	if (mtch != "")
	    printf (",MATCH", >> s)
	printf (" )\n", >> s)

	if (strstr(".fits",val) > 0)
	    printf (" VALUES('%s','%s','%s'", cls, val, "image", >> s)
	else
	    printf (" VALUES('%s','%s','%s'", cls, val, "file", >> s)
	if (subdir != "")
	    printf (",'%s'", subdir, >> s)
	if (mj != "")
	    printf (",%s", mj, >> s)
	if (mjdwin1 != "")
	    printf (",%s", mjdwin1, >> s)
	if (mjdwin2 != "")
	    printf (",%s", mjdwin2, >> s)
	if (det != "")
	    printf (",'%s'", det, >> s)
	if (imid != "")
	    printf (",'%s'", imid, >> s)
	if (filt != "")
	    printf (",'%s'", filt, >> s)
	if (exp != "")
	    printf (",%s", exp, >> s)
	if (nc != "")
	    printf (",%s", nc, >> s)
	if (qual != "")
	    printf (",%s", qual, >> s)
	if (mtch != "")
	    printf (",'%s'", mtch, >> s)
	printf (" );\n", >> s)

	# Execute the insert.
	pldb ("-f", s, > r)

	# Output result.
	if (results == "STDOUT")
	    concat (r)

	# Clean up.
	if (sql == "" && access(s))
	    delete (s, verify-)
	if (results != r && access(r))
	    delete (r, verify-)
end
