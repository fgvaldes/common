# SELECT -- Select sky stack images and set scale factors.
# The scale factors are based on the SKYMEAN values.
# Note this has output parameters and so the task should be cached.

procedure select (key, input, output, scales)

string	key			{prompt="Selection keyword"}
file	input			{prompt="File of input images"}
file	output			{prompt="File of selected images"}
file	scales			{prompt="File of scale factors"}
string	work = ""		{prompt="Work root name"}
bool	cksftflag = yes		{prompt="Check SFTFLAG?"}
int	nselect			{prompt="Number selected"}
int	nforced			{prompt="Number of forced selections"}
string	filter			{prompt="Filter"}

begin
	file	in, out, im, im1, wrk, tmp, otmp
	string	sftflag
	real	skymean, scale, norm
	struct	flt

	in = input
	out = output
	wrk = work
	if (work == "")
	    wrk = nhppsargs.module
	if (out == in)
	    out = wrk // "_out.tmp"
	tmp = wrk // "_select.tmp"

	# Initialize.
	if (out != "dev$null") {
	    if (access(out))
		delete (out, verify-)
	    touch (out)
	}
	if (access(scales))
	    delete (scales, verify-)
	nselect = 0
	nforced = 0
	filter = ""

	# Check images.
	hselect ("@"//input, "$I,SKYMEAN,"//key, yes) |
	    translit ("STDIN", '"', delete+, > tmp)
	list = tmp; norm = INDEF
	while (fscan (list, im, skymean, sftflag) != EOF) {
	    im1 = substr (im, strldx("/",im)+1, strlen(im))
	    if (nscan() < 3) {
		printf ("WARNING: Missing keywords (%s)\n", im1)
		next
	    }
	    if (sftflag == "Y!" || sftflag == "N!")
		nforced += 1
	    if (cksftflag && substr(sftflag,1,1) == "N")
	        next
	    if (skymean <= 0.0001) {
		printf ("WARNING: SKYMEAN must be positive (%s %s)\n",
		    im1, str(skymean))
		next
	    }
	    if (isindef(norm))
	        norm = skymean
	    scale = norm / skymean
	    print (im, >> out)
	    print (scale, >> scales)
	    nselect += 1
	    if (filter == "") {
		hselect (im, "FILTER", yes) | scan (flt)
		filter = flt
	    }
	}
	list = ""; delete (tmp, verify-)

	if (filter == "")
	    filter = "default"

	if (out == wrk // "_out.tmp")
	    rename (out, in)

end
