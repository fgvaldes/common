# FTRSIGCLIP -- Sigma clip on a table.
# The input is a table with a filename and a numeric value to clip against.
# The good file is a list of input good files and output remaining good files.
# The rej file is a list of appended rejected files.

#global	figno

procedure ftrsigclip (input, good, rej)

file	input			{prompt="Input clipping data"}
file	good			{prompt="Input and output good files"}
file	rej			{prompt="Input and output rejected files"}

file	work = ""		{prompt="Work root name"}
file	html = "index.html"	{prompt="HTML output"}
file	logfile = "STDOUT"	{prompt="Logfile output"}
string	label = "Clipping"	{prompt="Label for logging"}

int	lclip = 2		{prompt="Low clipping factor for statistics"}
int	hclip = 2		{prompt="High clipping factor for statistics"}
int	lsigma = 3		{prompt="Final low clipping factor"}
int	hsigma = 3		{prompt="Final high clipping factor"}

real	low = INDEF		{prompt="Low threshold"}
real	high = INDEF		{prompt="High threshold"}

real	mean			{prompt="Mean"}
real	sigma			{prompt="Sigma"}

begin
	string	fname, fname1
	file	tmp, tmpgood, tmprej
	real	lcut, hcut, x
	int	i, n, lastn

	# Return if there are not good files left.
	if (!access(good))
	    return

	# Set temporary files.
	if (work != "") {
	    tmp = work // ".tmp"
	    tmpgood = work // "good.tmp"
	    tmprej = work // "rej.tmp"
	} else {
	    tmp = nhppsargs.module // "_sc.tmp"
	    tmpgood = nhppsargs.module // "_scgood.tmp"
	    tmprej = nhppsargs.module // "_screj.tmp"
	}

	# Sigma clip to determine the mean and sigma.
	mean = INDEF
	for (i=1; i<=10; i+=1) {
	    if (isindef(mean)) {
		tstat (input, "c2", low=low, high=high, >> tmp)
		mean = tstat.mean; sigma = tstat.stddev
		lastn = tstat.nrows+1; n = tstat.nrows
	    } else if (n < lastn) {
		lcut = mean-lclip*sigma; hcut = mean+hclip*sigma
		tstat (input, "c2", low=lcut, high=hcut, >> tmp)
		mean = tstat.mean; sigma = tstat.stddev
		lastn = n; n = tstat.nrows
	    }
	}

	# Reject from good list to the bad list.
	lcut = mean - lsigma * sigma; hcut = mean + hsigma * sigma
	if (isindef(low) == NO)
	    lcut = max (low, lcut)
	if (isindef(high) == NO)
	    hcut = min (high, hcut)
	printf ("%s = %.4f\n", label, mean, >> tmp)
	printf ("%s limits = %.5f %.5f\n", label, lcut, hcut, >> tmp)
	list = good
	while (fscan(list,fname)!=EOF) {
	    match (fname, input) | scan (fname1, x)
	    if (x < lcut || x > hcut) {
		printf ("%s # %s\n", fname, label, >> tmprej)
		printf ("Rejected: %s (%g)\n", fname, x, >> tmp)
	    } else
		print (fname, >> tmpgood)
	}
	list = ""

	# Log results.
	if (logfile != "") {
	    printf ("%s clipping:\n", label, >> logfile)
	    concat (tmp, logfile, append+)
	}
	if (html != "") {
	    figno += 1
	    printf ('<P>Figure %d: %s clipping.\n', figno, label, >> html)
	    printf ('<PRE>\n', >> html)
	    concat (tmp, >> html)
	    printf ('</PRE>\n', >> html)
	}
	delete (tmp)

	if (access(tmprej)) {
	    concat (tmprej, >> rej)
	    delete (tmprej)
	} else if (html != "")
	    printf ('No files rejected based on %s.\n', strlwr(label), >> html)

	if (access(tmpgood))
	    rename (tmpgood, good)
	else {
	    delete (good)
	    if (html != "")
		printf ('All remaining files rejected based on %s.\n',
		    strlwr(label), >> html)
	}
end
