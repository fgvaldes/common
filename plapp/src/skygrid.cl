# SKYGRID -- Return tangent point from grid on the sky.

procedure skygrid (ra, dec)

real	ra			{prompt="RA (hr)"}
real	dec			{prompt="Dec (deg)"}
real	grid = 1		{prompt="Grid interval (deg)"}

begin
	int	igrid, rgrid, dgrid
	real	grid1, r1, d1, r2, d2, c, dr

	r1 = ra * 15
	d1 = dec

	igrid = nint (10 * grid)
	grid1 = igrid / 10.
	dgrid = nint (d1 / grid1) + 1000
        d2 = grid1 * (dgrid - 1000)
	if (abs(d2) <= 89.) {
	    c = dcos (d2)
	    dr = 360. * c / nint (360. * c / grid1)
	    rgrid = nint (r1 * c / dr)
	    r2 = dr / c * rgrid
	} else {
	    rgrid = 0
	    r2 = 0.
	}

	r2 = nint (60 * r2) / 60.
	d2 = nint (60 * d2) / 60.

        printf ("%.0H %.0h %02d%04d%04d\n", r2, d2, igrid, rgrid, dgrid)
end
