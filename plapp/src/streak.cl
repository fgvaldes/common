# STREAK -- Find streaks in an image.

# cache tinfo, tstat

procedure streak (image, catalog, objmask, streakmask)

file	image		{prompt = "Image"}
file	catalog		{prompt = "Catalog of sources"}
file	objmask		{prompt = "Object mask of sources"}
file	streakmask	{prompt = "Output streak mask"}

file	configdir="plappsrc$"	{prompt = "Configuration directory"}
file	work = "streak"	{prompt = "Prefix for working files"}

real	ratio = 0.5	{prompt = "Minimum boundry/area ratio"}
int	npix = 2000	{prompt = "Minimum number of pixels per source"}
real	ellip = 0.85	{prompt = "Minimum ellipticity per source"}
int	maxstreak = 5	{prompt = "Maximum number of streak clusters"}

struct	*fd

begin
	bool	found
	int	i, j, n, nc, nl, nx, ny
	real	theta, a, b, x1, x2, y1, y2, l1, l2, width
	file	im, cat, iobm, oobm
	string	dummy
	struct	filt

	if (maxstreak == 0)
	    return

	# Initialize
	im = image
	cat = catalog
	iobm = objmask
	if (! (imaccess(im) && access(cat) && imaccess(iobm))) {
	    printf ("ERROR: Not all input found for streak finding.\n")
	    return
	}
	oobm = streakmask
	found = NO
	delete (work//"*", verify-)
	if (imaccess(streakmask))
	    imdelete (streakmask)

	# Check configuration files.
	if (!(access(configdir//"istreak.def") &&
	      access(configdir//"ostreak.def") &&
	      access(configdir//"streak_curfit.dat")))
	    error (1, "Configuration files not found")

	# Extract streak candidates from the catalog.
	printf ("CR>%.2f&&NPIX>%d&&ELLIP>%.2f\n", ratio, npix, ellip) |
	    scan (filt)
	acefilter (im, icatalogs=cat, ocatalogs=work//"cat.tmp",
	    nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	    catfilter=filt, logfiles="", verbose=0,
	    extnames="", omtype="number", catomid="NUM", update=no)
	tinfo (work//"cat.tmp", tt-)
	if (tinfo.nrows == 0) {
	    delete (work//"*", verify-)
	    return
	}

	# Cluster the streak candidates and check if there are too many.
	acecluster (work//"cat.tmp", work//"clust.tmp", "3,100,100",
	    icatdef=configdir//"istreak.def",
	    ocatdef=configdir//"ostreak.def",
	    mincluster=1, ifilter="", ofilter="")
	tinfo (work//"clust.tmp", tt-)
	if (tinfo.nrows == 0) {
	    delete (work//"*", verify-)
	    return
	}
	tstat (work//"clust.tmp", "STREAK", out="")
	n = tstat.vmax
	if (n > maxstreak) {
	    delete (work//"*", verify-)
	    printf ("WARNING: Too many streak candidates found (%d)\n", n)
	    return
	}

	# For each streak mask it with a vector.

	hselect (iobm, "NAXIS1,NAXIS2", yes) | scan (nc, nl)
	mkpattern (work//"obm.pl", pixtype="int", ndim=2, nc=nc, nl=nl)
	for (i=1; i<=n; i+=1) {

	    # Select cluster, set theta, and extract from mask.
	    tquery (work//"clust.tmp", work//"clust1.tmp", "streak="//i,
		"NUM,AVTHETA", "")
	    tdump (work//"clust1.tmp", cd="", pf="", data="STDOUT",
		col="", rows="")
	    tstat (work//"clust1.tmp", "AVTHETA", out=""); theta = tstat.mean
	    delete (work//"dat*.tmp", verify-)
	    tdump (work//"clust1.tmp", cd="", pf="", data=work//"nums.tmp",
		col="NUM", rows="")
	    fd = work//"nums.tmp"
	    while (fscan (fd, j) != EOF)
		acefilter (im, icatalogs=work//"clust1.tmp", ocatalogs="",
		    nmaxrec=INDEF, iobjmasks=iobm, oobjmasks=work//"dat.tmp",
		    catfilter="NUM=="//j, logfiles="", verbose=0, extnames="",
		    omtype="list", catomid="NUM", update=no)
	    fd = ""; delete (work//"nums.tmp", verify-)
	    delete (work//"clust1.tmp", verify-)
	    count (work//"dat.tmp") | scan (j)
	    if (j < 100)
	        next

	    # Set regression axis.
	    if (abs(theta) > 60.) {
	        nx = nl; ny = nc
		concat (work//"dat.tmp") |
		    fields ("STDIN", "2,1", > work//"dat.tmp")
	    } else {
	        nx = nc; ny = nl
	    }
	    curfit (work//"dat.tmp", function="legendre",
		weighting="uniform", order=2, interactive+, axis=1, listdata-,
		verbose-, calctype="real", power+, device="stdvdm",
		cursor=configdir//"streak_curfit.dat", > work//"fit.tmp")
	    tail (work//"fit.tmp", nl=2) | table ("STDIN") |
		scan (dummy, j, a, dummy, j, b)

	    # Set vector endpoints.
	    x1 = 1; x2 = nx
	    if (b == 0) {
	        y1 = a; y2 = a
	    } else if (b < 0) {
		y1 = a + b * x2; y2 = a + b * x1
		if (y1 < 1)
		    x2 = (1 - a) / b
		if (y2 > ny)
		    x1 = (ny - a) / b
		y1 = a + b * x2; y2 = a + b * x1
	    } else {
		y1 = a + b * x1; y2 = a + b * x2
		if (y1 < 1)
		    x1 = (1 - a) / b
		if (y2 > ny)
		    x2 = (ny - a) / b
		y1 = a + b * x1; y2 = a + b * x2
	    }

	    # Compare lengths.
	    tstat (work//"dat.tmp", "c1", out="")
	    l1 = sqrt ((1 + b**2) * (tstat.vmax - tstat.vmin)**2)
	    l2 = sqrt ((1 + b**2) * (x2 - x1)**2)
	    if (abs(theta) > 88 || abs(theta) < 1 ||
	        (l1 < 300 && l1 / l2 < 0.5) ||
	        (abs(theta) < 4 && (y1 < 100 || y2 > ny - 100))) {
		delete (work//"dat.tmp,"//work//"fit.tmp", verify-)
		printf ("  Skipping: theta=%.1f, l1=%d, l2=%d, y1=%d, y2=%d\n",
		    theta, l1, l2, y1, y2)
		next
	    } else
		printf ("  Accepting: theta=%.2f, l1=%d, l2=%d\n",
		    theta, l1, l2)

	    match ("RMS", work//"fit.tmp") | scan (dummy, dummy, dummy, width)
	    width = min (100, 2 * width + 5)
	    if (tstat.vmin - x1 > 200)
	        x1 = tstat.vmin
	    ;
	    if (x2 - tstat.vmax > 200)
	        x2 = tstat.vmax
	    ;
	    x1 -= width / 2
	    x2 += width / 2
	    y1 = a + b * x1
	    y2 = a + b * x2
	    if (abs(theta) > 60.) {
		printf ("vector %.1f %.1f %.1f %.1f %.1f\n",
		    y1, x1, y2, x2, width)
		voverlay (work//"obm.pl", 1, y1, x1, y2, x2, width)
	    } else {
		printf ("vector %.1f %.1f %.1f %.1f %.1f\n",
		    x1, y1, x2, y2, width)
		voverlay (work//"obm.pl", 1, x1, y1, x2, y2, width)
	    }
	    found = YES
	}

	if (found)
	    rename (work//"obm.pl", oobm)
	
	delete (work//"*", verify-)
end
