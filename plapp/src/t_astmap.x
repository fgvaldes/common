include	<error.h>
include	<fset.h>
include	<mach.h>
include	<math.h>
include	<imhdr.h>
include	<acecat.h>
include	"astmatch.h"

define	STRUCTDEF	"plapp$src/astmatch.h"
define	DEBUG		0
define	SZ_CMD		(4*SZ_LINE)

procedure t_astmap ()

int	icats			# List of input matched catalogs
int	wcs			# List of catalog WCS
pointer	catdef			# Catalog definitions
pointer	filter			# Catalog filter
bool	mosaic			# Mosaic mode?
pointer	fitgeom			# Fit geometry
real	reject			# Rejection sigma
real	rms			# RMS to accept (arcsec)
bool	interactive		# Interactive?
int	ngrid			# Number of constraining grid points
int	logfd			# Logfile
int	erraction		# Error action

int	i, j, ncats, nfit, mode
double	scale
pointer	sp, icat, iwcs, fitfile, str
pointer	cat, mw, mwa, mwg, im

bool	clgetb(), streq()
int	clgeti(), clgwrd(), imtopenp(), imtlen(), imtgetim(), nowhite(), open()
double	clgetd()
pointer	immap()
errchk	acm_mw, acm_gcat, acm_geomap, acm_ccmap_cat, open

begin
	call smark (sp)
	call salloc (icat, SZ_FNAME, TY_CHAR)
	call salloc (iwcs, SZ_FNAME, TY_CHAR)
	call salloc (catdef, SZ_LINE, TY_CHAR)
	call salloc (filter, SZ_LINE, TY_CHAR)
	call salloc (fitgeom, SZ_FNAME, TY_CHAR)
	call salloc (fitfile, SZ_FNAME, TY_CHAR)
	call salloc (str, SZ_LINE, TY_CHAR)

	call mktemp ("tmp$iraf", Memc[fitfile], SZ_FNAME)

	# Get task parameters.
	icats = imtopenp ("cats")
	wcs = imtopenp ("wcs")
	call clgstr ("catdef", Memc[catdef], SZ_LINE)
	call clgstr ("filter", Memc[filter], SZ_LINE)
	mosaic = clgetb ("mosaic")
	call clgstr ("fitgeometry", Memc[fitgeom], SZ_FNAME)
	interactive = clgetb ("interactive")
	reject = clgetd ("reject")
	rms = clgetd ("rms")
	ngrid = clgeti ("ngrid")

	erraction = clgwrd ("erraction", Memc[str], SZ_LINE, "|abort|warn|")
	switch (erraction) {
	case 2:
	    erraction = EA_WARN
	default:
	    erraction = EA_ERROR
	}

	call clgstr ("logfile", Memc[str], SZ_LINE)
	if (nowhite (Memc[str], Memc[str], SZ_LINE) > 0) {
	    logfd = open (Memc[str], APPEND, TEXT_FILE)
	    call fseti (logfd, F_FLUSHNL, YES)
	} else
	    logfd = NULL

	if (logfd != NULL) {
	    call sysid (Memc[str], SZ_LINE)
	    call fprintf (logfd, "ASTMAP: %s\n")
	        call pargstr (Memc[str])
	}

	# Check lists.
	ncats = imtlen (icats)
	i = imtlen (wcs)
	if (i != ncats && i > 1)
	    call error (1, "WCS list doesn't match image catalog list")

	# Allocate memory for the various pointers.  For a mosaic we
	# use an arrays of pointers while for independent data we just
	# have one set of pointers per loop.
	
	if (mosaic)
	    ncats = imtlen (icats)
	else
	    ncats = 1

	call salloc (cat, ncats, TY_POINTER)
	call salloc (mw, ncats, TY_POINTER)
	call salloc (mwa, ncats, TY_POINTER)

	# Initialize.
	call aclri (Memi[cat], ncats)
	call aclri (Memi[mw], ncats)
	call aclri (Memi[mwa], ncats)
	Memc[iwcs] = EOS
	mwg = NULL

	# Set catalog access mode based on ngrid.
	if (ngrid == 0)
	    mode = READ_ONLY
	else
	    mode = READ_WRITE

	while (imtgetim (icats, Memc[icat], SZ_FNAME) != EOF) {

	    iferr {
		do i = 0, ncats-1 {
		    if (i > 0)
			j = imtgetim (icats, Memc[icat], SZ_FNAME)
		    if (imtgetim (wcs, Memc[str], SZ_LINE) != EOF)
			j = nowhite (Memc[str], Memc[iwcs], SZ_FNAME)

		    if (Memc[iwcs]==EOS || streq(Memc[iwcs],Memc[icat])) {
			if (mode == READ_WRITE)
			    call catopen (Memi[cat+i], Memc[icat], Memc[icat],
			        "", STRUCTDEF, NULL, 1)
			else
			    call catopen (Memi[cat+i], Memc[icat], "",
			        "", STRUCTDEF, NULL, 1)
			im = CAT_IHDR(Memi[cat+i])
			call acm_mw (im, Memi[mw+i], Memi[mwa+i], scale)
		    } else {
			im = immap (Memc[iwcs], READ_ONLY, 0)
			call acm_mw (im, Memi[mw+i], Memi[mwa+i], scale)
			call imunmap (im)
		    }
		
		    call acm_gcat1 (Memc[icat], Memc[catdef], Memc[filter],
			Memi[mw+i], mode, Memi[cat+i], logfd)
		    call acm_geomap_data (Memi[cat+i], Memi[mw+i], Memi[mwa+i],
			 Memc[fitfile], nfit)
		}

		call acm_geomap (mwg, Memi[mw], Memc[fitfile], nfit, "general",
		    interactive, reject, rms, logfd)

		# Add grid of constraining points.
		if (ngrid > 0) {
		    do i = 0, ncats-1
			call acm_ccmap_cat (Memi[cat+i], Memi[mw+i],
			    Memi[mwa+i], mwg, ngrid)
		}
	    } then
	        call erract (erraction)

	    # Free catalog and MWCS pointers.
	    do i = 0, ncats-1 {
		if (Memi[cat+i] != NULL)
		    call catclose (Memi[cat+i], NO)
		if (Memi[mw+i] != NULL)
		    call mw_close (Memi[mw+i])
		if (Memi[mwa+i] != NULL)
		    call mw_close (Memi[mwa+i])
	    }
	    if (mwg != NULL)
		call mw_close (mwg)
	}

	call imtclose (wcs)
	call imtclose (icats)
	call sfree (sp)
end


procedure acm_gcat1 (catname, catdef, catfilter, mw, mode, cat, logfd)

char	catname[ARB]			#I Catalog name
char	catdef[ARB]			#I Catalog definitions
char	catfilter[ARB]			#I Catalog filter
pointer	mw				#I MWCS
int	mode				#I Access mode
pointer	cat				#U Catalog pointer
int	logfd				#I Log file descriptor

int	i
pointer	ctlw, ctwl, rec

pointer	mw_sctran()
errchk	mw_sctran
errchk	catopen, catrrecs

begin
	# Open the catalog.
	if (cat == NULL) {
	    if (mode == READ_WRITE)
		call catopen (cat, catname, catname, catdef, STRUCTDEF, NULL, 1)
	    else
		call catopen (cat, catname, "", catdef, STRUCTDEF, NULL, 1)
	}

	# Read catalogs and set fields using the WCS.
	call catrrecs (cat, catfilter, -1)
	ctlw = mw_sctran (mw, "logical", "world", 3)
	ctwl = mw_sctran (mw, "world", "logical", 3)
	do i = 1, CAT_NRECS(cat) {
	    rec = CAT_REC(cat,i)
	    if (rec == NULL)
	        next
	    if ((IS_INDEFD(ACM_X(rec))||IS_INDEFD(ACM_Y(rec))) &&
		(IS_INDEFD(ACM_RA(rec))||IS_INDEFD(ACM_DEC(rec))))
		next
	    if (IS_INDEFD(ACM_X(rec))||IS_INDEFD(ACM_Y(rec))) {
		call mw_c2trand (ctwl, ACM_RA(rec)*15D0, ACM_DEC(rec),
		    ACM_X(rec), ACM_Y(rec))
	    } else if (IS_INDEFD(ACM_RA(rec))||IS_INDEFD(ACM_DEC(rec))) {
		call mw_c2trand (ctlw, ACM_X(rec), ACM_Y(rec),
		    ACM_RA(rec), ACM_DEC(rec))
		ACM_RA(rec) = ACM_RA(rec) / 15D0
	    }
	    ACM_PTR(rec) = NULL
	}
	call mw_ctfree (ctlw)
	call mw_ctfree (ctwl)

	if (DEBUG > 0) {
	    do i = 1, CAT_NRECS(cat) {
		rec = CAT_REC(cat,i)
		if (rec == NULL)
		    next
		call printf ("  %.2H %.1h %.2f %.2f\n")
		    call pargd (ACM_RA(rec))
		    call pargd (ACM_DEC(rec))
		    call pargd (ACM_X(rec))
		    call pargd (ACM_Y(rec))
	    }
	    call flush (STDOUT)
	}
end


procedure acm_geomap_data (cat, mw, mwa, fitfile, nfit)

pointer	cat			#I Reference catalogs
pointer	mw			#I WCS
pointer	mwa			#I WCS for world <-> astrometry
char	fitfile[ARB]		#O Filename for fitting data
int	nfit			#U Number of objects in fitting data

int	i, fd
double	dra1, ddec1, dra2, ddec2
pointer	rec, ctlw, ctwa

int	open()
pointer	mw_sctran()
errchk	open

begin
	fd = open (fitfile, APPEND, TEXT_FILE)

	ctlw = mw_sctran (mw, "logical", "world", 3)
	ctwa = mw_sctran (mwa, "world", "physical", 3)
	do i = 1, CAT_NRECS(cat) {
	    rec = CAT_REC(cat,i)
	    call mw_c2trand (ctlw, ACM_X(rec), ACM_Y(rec), dra1, ddec1)
	    call mw_c2trand (ctwa, dra1, ddec1, dra1, ddec1)
	    call mw_c2trand (ctwa, ACM_RA(rec)*15D0, ACM_DEC(rec), dra2, ddec2)
	    call fprintf (fd, "%g %g %g %g\n")
		call pargd (3600. * dra1)
		call pargd (3600. * ddec1)
		call pargd (3600. * dra2)
		call pargd (3600. * ddec2)
	    nfit = nfit + 1
	}
	call mw_ctfree (ctlw)
	call mw_ctfree (ctwa)

	call close (fd)
end


procedure acm_geomap (mw, mwref, fitfile, nfit, fitgeom, interactive,
	reject, rms, logfd)

pointer	mw			#O MWCS mapping
pointer	mwref			#I Reference MWCS for tangent point
char	fitfile[ARB]		#I File containing fitting data
int	nfit			#I Number of objects in fitting data
char	fitgeom[ARB]		#I Fit geometry
bool	interactive		#I Interactive?
real	reject 			#I sigma rejection
real	rms			#I RMS to accept (arcsec)
int	logfd			#I Log file descriptor

int	i, fd
double	results[8], r[2], w[2], cd[2,2]
pointer	sp, db, graphics, cursor, cmd

int	open(), stropen(), strdic(), fscan(), nscan()
pointer	mw_open()
errchk	open

begin
	call smark (sp)
	call salloc (db, SZ_FNAME, TY_CHAR)
	call salloc (graphics, SZ_FNAME, TY_CHAR)
	call salloc (cursor, SZ_FNAME, TY_CHAR)
	call salloc (cmd, SZ_CMD, TY_CHAR)

	# Get interactive graphics parameters.
	call clgstr ("graphics", Memc[graphics], SZ_LINE)
	call clgstr ("cursor", Memc[cursor], SZ_LINE)

	# Temporary database.
	call mktemp ("tmp$iraf", Memc[db], SZ_FNAME)

	# Generate command.
	fd = stropen (Memc[cmd], SZ_CMD, NEW_FILE)
	call fprintf (fd,
	    "geomap input=%s database=%s transforms='' results=''")
	    call pargstr (fitfile)
	    call pargstr (Memc[db])
	call fprintf (fd, " xmin=INDEF xmax=INDEF ymin=INDEF ymax=INDEF")
	call fprintf (fd, " fitgeom=%s func=polynomial")
	    if (nfit > 1)
		call pargstr (fitgeom)
	    else
		call pargstr ("shift")
	call fprintf (fd, " xxo=2 xyo=2 xxt=half yxo=2 yyo=2 yxt=half")
	call fprintf (fd, " reject=%g maxiter=%d calc=double verb-")
	    call pargr (reject)	
	    if (reject > 0.1)
		call pargi (3)	
	    else
		call pargi (0)	
	call fprintf (fd, " inter=%b graphics=%s cursor=\"%s\"")
	    call pargb (interactive)
	    call pargstr (Memc[graphics])
	    call pargstr (Memc[cursor])
	if (Memc[cursor] != EOS)
	    call fprintf (fd, " > dev$null")
	call close (fd)

	# Run GEOMAP.
	call clcmdw (Memc[cmd])

	# Remove fit file.
	call delete (fitfile)

	# Extract linear transformations terms.
	fd = open (Memc[db], READ_ONLY, TEXT_FILE)
	while (fscan (fd) != EOF) {
	    call gargwrd (Memc[cmd], SZ_CMD)
	    if (nscan() != 1)
		next
	    i = strdic (Memc[cmd], Memc[cmd], SZ_CMD,
		"|xshift|yshift|xmag|ymag|xrotation|yrotation|xrms|yrms|")
	    if (i == 0)
		next
	    call gargd (results[i])
	    if (i == 5 || i == 6)
		if (results[i] > 180.)
		    results[i] = results[i] - 360.
	}
	call close (fd)
	call delete (Memc[db])

	# Compute new tangent point.
	call mw_gwtermd (mwref, r, w, cd, 2)
	call sldtps (DEGTORAD(results[1]/3600.), DEGTORAD(results[2]/3600.),
	    DEGTORAD(w[1]), DEGTORAD(w[2]), r[1], r[2])
	r[1] = RADTODEG(r[1])
	r[2] = RADTODEG(r[2])

	# Provide output.
	if (logfd != NULL || interactive) {
	    call fprintf (logfd, "      input number of coordinates = %d\n")
		call pargi (nfit)
	    call fprintf (logfd, "      old tangent point = (%.2H, %.1h)\n")
		call pargd (w[1])
		call pargd (w[2])
	    call fprintf (logfd, "      new tangent point = (%.2H, %.1h)\n")
		call pargd (r[1])
		call pargd (r[2])
	    call fprintf (logfd,
	        "      tangent point shift = (%.2f, %.2f) arcsec\n")
		call pargd (results[1])
		call pargd (results[2])
	    call fprintf (logfd,
	        "      fractional scale change = (%.3f, %.3f)\n")
		call pargd (results[3])
		call pargd (results[4])
	    call fprintf (logfd, "      axis rotation = (%.3f, %.3f) degrees\n")
		call pargd (results[5])
		call pargd (results[6])
	    call fprintf (logfd, "      rms = (%.3f, %.3f) arcsec\n")
		call pargd (results[7])
		call pargd (results[8])
	}

	# Set transformation: uncorrected astrometric -> corrected astrometric.
	r[1] = 0.; r[2] = 0.
	w[1] = results[1] / 3600.; w[2] = results[2] / 3600.
	cd[1,1] = results[3] * cos (DEGTORAD(results[5]))
	cd[2,1] = results[4] * sin (DEGTORAD(results[6]))
	cd[1,2] = -results[3] * sin (DEGTORAD(results[5]))
	cd[2,2] = results[4] * cos (DEGTORAD(results[6]))

	mw = mw_open (NULL, 2)
	call mw_newsystem (mw, "world", 2)
	call mw_swtype (mw, 1, 1, "linear", "")
	call mw_swtype (mw, 2, 1, "linear", "")
	call mw_swtermd (mw, r, w, cd, 2)

	# If the result doesn't satisfy RMS requirement return with an error.
	if (!interactive && rms < max (results[7], results[8])) {
	    call sprintf (Memc[cmd], SZ_CMD,
		"RMS of fit is too large: %.3f > %.3f")
		call pargd (max (results[7], results[8]))
		call pargr (rms)
	    call error (1, Memc[cmd])
	}

	call sfree (sp)
end


procedure acm_ccmap_cat (icat, mw, mwa, mwg, ngrid)

pointer	icat			#I Input catalog
pointer	mw			#I Full uncorrected WCS
pointer	mwa			#I World <-> Astrometry transformation
pointer	mwg			#I Geomap transformation
int	ngrid			#I Number of constraining grid points

int	i, j, k, nxgrid, nygrid
double	crmin1, crmax1, crmin2, crmax2, x, y, r, d
pointer	hdr, ctlw, ctwa, ctaw, ctaa, rec

double	imgetd()
pointer	mw_sctran()
errchk	mw_sctran

begin
	hdr = CAT_IHDR(icat)
	crmin1 = imgetd (hdr, "crmin1")
	crmax1 = imgetd (hdr, "crmax1")
	crmin2 = imgetd (hdr, "crmin2")
	crmax2 = imgetd (hdr, "crmax2")

	r = (crmax2 - crmin2) / (crmax1 - crmin1)
	nxgrid = max (3, nint (sqrt (ngrid / r)))
	nygrid = max (3, nint (sqrt (ngrid * r)))

	# Set WCS to apply GEOMAP transformation.
	ctlw = mw_sctran (mw, "logical", "world", 3)
	ctwa = mw_sctran (mwa, "world", "physical", 3)
	ctaw = mw_sctran (mwa, "physical", "world", 3)
	ctaa = mw_sctran (mwg, "physical", "world", 3)

	# Create output record structure.
	call malloc (rec, CAT_RECLEN(icat), TY_STRUCT)

	k = CAT_NRECS(icat)
	do j = 1, nygrid {
	    y = nint (crmin2 + (j - 1) * (crmax2 - crmin2) / (nygrid - 1.))
	    do i = 1, nxgrid {
	        x = nint (crmin1 + (i - 1) * (crmax1 - crmin1) / (nxgrid - 1.))

		call mw_c2trand (ctlw, x, y, r, d)
		call mw_c2trand (ctwa, r, d, r, d)
		call mw_c2trand (ctaa, r, d, r, d)
		call mw_c2trand (ctaw, r, d, r, d)

		ACM_RA(rec) = r / 15.
		ACM_DEC(rec) = d
		ACM_WX(rec) = r / 15.
		ACM_WY(rec) = d
		ACM_X(rec) = x
		ACM_Y(rec) = y
		ACM_MREF(rec) = -99
		ACM_MAG(rec) = -99
		ACM_FWHM(rec) = -99
		ACM_WA(rec) = -99
		ACM_WB(rec) = -99
		call strcpy ("-", ACM_FLAGS(rec), ARB)
		k = k + 1
		call catwrec (icat, rec, k)
	    }
	}

	call mfree (rec, TY_STRUCT)
	call mw_ctfree (ctaa)
	call mw_ctfree (ctaw)
	call mw_ctfree (ctwa)
	call mw_ctfree (ctlw)
end
