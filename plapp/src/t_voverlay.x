# VOVERLAY -- Overlay a vector to an image.
#
# This is not as efficient as it could be since it evaluates every
# pixel in a bounding box rather than the end points at each line and
# use a vops routine.  This task is a quick work around for a segmentation
# error in mskregions.

#task	voverlay = t_voverlay

include	<imhdr.h>

procedure t_voverlay ()

pointer	input			# Input image
real	val			# Vector value
real	x1, y1, x2, y2		# Vector end-points
real	wid			# Vector width

int	c, c1, c2, l, l1, l2, nc, nl
real	x, y, dx, dy, len, cost, sint
pointer	sp
pointer	im, ptr

real	clgetr()
pointer	immap(), imgl2i(), imgl2r(), impl2i(), impl2r()

errchk	immap

begin
	call smark (sp)
	call salloc (input, SZ_FNAME, TY_CHAR)

	# Get parameters.
	call clgstr ("input", Memc[input], SZ_FNAME)
	val = clgetr ("value")
	x1 = clgetr ("x1")
	y1 = clgetr ("y1")
	x2 = clgetr ("x2")
	y2 = clgetr ("y2")
	wid = clgetr ("width") / 2.

	# Open the mask.
	ptr = immap (Memc[input], READ_WRITE, 0); im = ptr
	if (IM_NDIM(im) != 2) {
	    call imunmap (im)
	    call error (1, "Image must be 2D")
	}
	nc = IM_LEN(im,1); nl = IM_LEN(im,2)

	# Set rotation angle.
	if (y2 < y1) {
	    x = x1; y = y1; x1 = x2; y1 = y2; x2 = x; y2 = y 
	}
	dx = x2 - x1; dy = y2 - y1
	len = sqrt (dx**2 + dy**2)
	cost = dx / len
	sint = dy / len

	# Set bounding box.
	c1 = max (1, nint(min(x1,x2) - wid * sint))
	c2 = min (nc, nint(min(x1,x2) + len * abs(cost) + wid * sint))
	l1 = max (1, nint(y1 - wid * abs(cost)))
	l2 = min (nl, nint(y1 + len * sint + wid * abs(cost)))

	# Overlay vector. Evaluate each pixel in the bounding box for
	# whether it is in the vector.

	switch (IM_PIXTYPE(im)) {
	case TY_SHORT, TY_INT, TY_LONG:
	    do l = l1, l2 {
		ptr = impl2i (im, l)
		call amovi (Memi[imgl2i(im,l)], Memi[ptr], nc)
	        dy = l - y1 
	        do c = c1, c2 {
		    dx = c - x1
		    x = dx * cost + dy * sint
		    if (x < 0 || x > len)
		        next
		    y = -dx * sint + dy * cost
		    if (y < -wid || y > wid)
		        next
		    Memi[ptr+c-1] = val
		}
	    }
	default:
	    do l = l1, l2 {
		ptr = impl2r (im, l)
		call amovr (Memi[imgl2r(im,l)], Memr[ptr], nc)
	        dy = l - y1 
	        do c = c1, c2 {
		    dx = c - x1
		    x = dx * cost + dy * sint
		    if (x < 0 || x > l)
		        next
		    y = -dx * sint + dy * cost
		    if (y < -wid || y > wid)
		        next
		    Memr[ptr+c-1] = val
		}
	    }
	}

	call imunmap (im)
	call sfree (sp)
end
