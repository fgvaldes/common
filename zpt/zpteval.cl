string	imageid
int	ccd, amp
real	c1, c2, c3, skyref, skymin, skymax, sky, m
real	mjd = 57424

#for (ccd=1; ccd<=4; ccd+=1) {
#for (amp=1; amp<=4; amp+=1) {
for (ccd=2; ccd<=2; ccd+=1) {
for (amp=3; amp<=3; amp+=1) {

imageid = "ccd" // ccd // ":" // radix(amp+9,16)
getcal ("ampflat", imageid=imageid, mjd=mjd, result="STDOUT") |
    scan (c1, c2, c3, skyref, skymin, skymax)

for (sky=1000; sky<=20000; sky+=500) {
    x = log10(max(skymin,min(skymax,sky))/skyref)   
    m = c1 + x * (c2 + x * c3)
    #m = x * (c2 + x * c3)
    print (sky, x, m, >> "zpteval.tmp")
}
fields ("zpteval.tmp", "1,3") | graph (point-, wx1=skymin, wx2=skymax,
    wy1=0., wy2=1.5, title=imageid, xlabel="sky", ylabel="dmag")
#fields ("zpteval.tmp", "2,3") | graph (point-, wx1=-0.2, wx2=0.4,
#    wy1=-0.1, wy2=0.1, title=imageid, xlabel="sky", ylabel="dmag")
=gcur
delete ("zpteval.tmp")
}
}
