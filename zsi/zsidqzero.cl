# ZSIDQZERO -- Combine bias images with data quality checking.
#
# When the input list has more than one bias it checks for good ones.
# This rejection of bad biases is different from the rejection that is
# done by zcombine. Here rejection is done based on global statistics,
# whereas in zcombine rejection is done based on a per-pixel basis. In
# principle, a bias frame could be globally bad (e.g., elevated noise
# level) but some pixels of such a frame would not be rejected by zcombine.
# Because possible structure in the bias may affect the global statistics,
# the quality control has to be done in two steps: first, the bias frames
# should be combined and subtracted from the individual ones. Next, the
# statistics of the individual frames should be determined and compared to
# the expected values. If some frames need to be rejected, zcombine needs
# to be run again.

procedure zsidqzero (dataset, ilist, imname)

string	dataset			{prompt="Dataset name"}
file	ilist			{prompt="List of input zero images"}
file	imname			{prompt="Output zero image"}
real	quality			{prompt="Quality"}
int	status			{prompt="Status return"}

struct	*fd

begin
	int	numbias = 10		# Minimum number of biases
	real	meandev = 3		# Maximum mean deviation from 0
	real	noisemin = 0.25		# Minimum noise
	real	noisemax = 4		# Maximum noise

	int	i, nin, nout, nkey
	int	result = 0
	real	rdnoise, gain, dqglmean, dqglsig, exprdnoise, mjd
	real	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, dqovjprb
	string	rulepath, im, sim, parentImages, cast
	file	tmpfile, olist

	# Initialize.
	quality = 1.; stauts = 1

	# If there is only one image set some header info and return it.
	count (ilist) | scan (i)
	if (i == 1) {
	    imcopy ("@"//ilist, imname)
	    quality = 0.
	    return
	}

	# Combine.
	zcombine ("@"//ilist, imname, amps=no, offset="physical",
	    imcmb="DTNSANAM")

#	# If we get here then we have multiple biases to check and combine.
#	tmpfile = mktemp ("tmpdq")
#	olist = mktemp ("tmpdq")
#	nin = 0; nout = 0; parentImages = ''
#	fd = ilist
#	while (fscan (fd, im) != EOF) {
#	    sim = substr (im, strldx("/",im)+1, 1000)
#
#	    # Get required header info and return with an error if missing.
#	    hselect (im, "$rdnoise, $gain, $dqglsig, $dqglmean, yes) |
#	        scan (rdnoise, gain, dqglsig, dqglmean)
#	    if (isindef(rdnoise)||isindef(gain)||isindef(dqglsig)||
#	        isindef(dqglmean)) {
#		fd = ""
#		printf ("WARNING: Missing header information (%s)\n", sim)
#		print (im)
#	        print (rdnoise, gain, dqglsig, dqglmean)
#		quality = -1.
#		status = 0
#		return
#	    }
#
#	    rdnoise = rdnoise/gain
#	    if (dqglsig==0)
#	        dqglmean = INDEF
#	    else
#	        dqglmean = dqglmean/dqglsig
#
#	    # Get the statistics of the residual image.
#	    imarith (im, "-", imname, tmpfile)
#	    imstat (tmpfile, fields="mean,stddev", nclip=1, lsigma=2, usigma=2,
#	        format-) | scan (x1, x2)
#	    imdel (tmpfile)
#
#	    # Convert mean (x1) into units of sigma (x2). In some cases
#	    # x2 can be 0, indicating im and imname are the same. In that
#	    # case, x1 is left unchanged.
#	    if (x2 != 0)
#	        x1 = x1/x2
#	    ;
#
#	    # Calculate the expected noise in the average bias frame
#	    # The pixel values in a subtracted bias (Bsub) are:
#	    # Bsub = B1-sum(Bn)[i=1..n]/n
#	    #      = B1((n-1)/n)-sum(Bn)[i=2..n]/n
#	    # The corresponding noise is:
#	    # sig  = sqrt{sig1**2*((n-1)/n)**2+sum((sign/n)**2)}
#	    #      = sqrt(sig1**2*((n-1)/n)**2+(n-1)*(sig/n)**2)
#	    #      = sig/n*sqrt(i*i-i)
#	    exprdnoise = rdnoise * sqrt (i*i-i) / i;
#
#	    if (verbose>=2) {
#	        # Write data quality data to log file ...
#	        printf ("DQ | image: %s\n", im)
#	        printf ("DQ | rdnoise[ADU]: %f\n", rdnoise)
#	        printf ("DQ | expected noise[ADU]: %f\n", exprdnoise)
#	        printf ("DQ | sigma[ADU]: %f (before zcombine)\n", dqglsig)
#	        printf ("DQ | mean/sigma: %f (before zcombine)\n", dqglmean)
#	        printf ("DQ | sigma[ADU]: %f (after zcombine)\n", x2)
#	    }
#	    ;
#
#	    # Express the noise in units of the expected noise
#	    x2 = x2/exprdnoise
#	    if (verbose>=2) {
#	        printf ("DQ | sigma[exp. sigma]: %f (after zcombine)\n", x2)
#	        printf ("DQ | mean/sigma: %f (after zcombine)\n", x1)
#	    }
#	    ;
#
#	    # ... to the header ...
#	    hedit (im, "dqbiesig", exprdnoise, add+, verify-, show+, update+)
#	    hedit (im, "dqbizmns", x1, add+, verify-, show+, update+)
#	    hedit (im, "dqbizsge", x2, add+, verify-, show+, update+)
#
#	    # ... and to PMAS
#	    printf ("%12.5e\n", exprdnoise) | scan (cast)
#	    setkeyval (class="zeroimage", id=sim, dm=dm,
#		keyword="dqbiesig", value=cast)
#	    printf ("%12.5e\n", x1) | scan (cast)
#	    setkeyval (class="zeroimage", id=sim, dm=dm,
#		keyword="dqbizmns", value=cast)
#	    printf ("%12.5e\n", x2) | scan (cast)
#	    setkeyval (class="zeroimage", id=sim, dm=dm,
#		keyword="dqbizsge", value=cast)
#
#	    # Check if the bias is OK.
#	    if (abs(x1)<meandev && x2>noisemin && x2<noisemax) {
#		printf  ("%s\n", im, >> olist)
#		if (strlen(parentImages)==0) {
#		    parentImages = sim
#		} else {
#		   parentImages = parentImages // ',' // sim
#		}
#		nout = nout+1
#	    } else
#		printf ("WARNING: Rejected this bias frame (%d)\n", im)
#
#	    # Increase the counter
#	    nin = nin+1
#	}
#	fd = ""
#	
#	# Check if there are enough biases.
#	if (i < numbias)
#	    printf ("WARNING: Small number of bias frames (%d)\n", nout)
#
#	# No images left...
#	if (nout==0) {
#	    if (imaccess (imname))
#		imdel (imname)
#	    printf ("WARNING: No bias frames to combine after rejections\n"t)
#	    quality = -1.
#	    status = 2
#	    return
#	}
#
#	# Make a new zero image without the rejected ones.
#	if (nout < nin) {
#	    if (imaccess (imname))
#		imdel (imname)
#	    if (nout>1)
#		zcombine ("@"//olist, imname, amps=no, offset="physical",
#		    imcmb="DTNSANAM")
#	    else
#		imcopy ("@"//olist, imname)
#	}
#
#	# Write DQ to header and PMAS.
#	nin = nin-nout
#	hedit (imname, "dqzenusd", nout, add+, show+)
#	hedit (imname, "dqzenrej", nin, add+, show+)
#	parentImages = "\"" // parentImages // "\""
#
#	# Now check the bias structure.
#	biasstruct (imname)
#	flpr # Force IRAF to update the header of imname
#	hselect (imname, "dqzexslp,dqzeyslp,dqzexsig,dqzeysig,dqzeglme,\
#	    dqzeglsi,dqzexmea,dqzexcef,dqzeymea,dqzeycef", yes) |
#	    scan (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
#	if (nscan()<10)
#	    printf ("WARNING: Could not read all data from biasstruct\n")
#	delete (olist)
end
